package com.payriff.adpmeezanbank.service;

import com.payriff.adpmeezanbank.client.MeezanClient;
import com.payriff.adpmeezanbank.pojo.CreatePaymentDto;
import com.payriff.adpmeezanbank.pojo.FinanceOperationDto;

@org.springframework.stereotype.Service
public class GatewayService {
    private final MeezanClient neezanClient;

    public GatewayService(MeezanClient neezanClient) {
        this.neezanClient = neezanClient;
    }


    public Object createPayment(CreatePaymentDto createPaymentDto, boolean preAuth) {
        return neezanClient.createPayment(createPaymentDto, preAuth);
    }

    public Object getPaymentInfo(FinanceOperationDto financeOperationDto) {
        return neezanClient.getPaymentInfo(financeOperationDto);
    }

    public Object reversePayment(FinanceOperationDto financeOperationDto) {
        return neezanClient.reverse(financeOperationDto);
    }

    public Object refundPayment(FinanceOperationDto financeOperationDto) {
        return neezanClient.refund(financeOperationDto);
    }

    public Object completePayment(FinanceOperationDto financeOperationDto) {
        return neezanClient.complete(financeOperationDto);
    }
}
