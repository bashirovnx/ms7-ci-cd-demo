package com.payriff.adpmeezanbank.controller;


import com.payriff.adpmeezanbank.pojo.CreatePaymentDto;
import com.payriff.adpmeezanbank.pojo.FinanceOperationDto;
import com.payriff.adpmeezanbank.service.GatewayService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController

@RequestMapping("/api/meezan-bank")
public class Controller {
    private final GatewayService gatewayService;


    public Controller(GatewayService gatewayService) {
        this.gatewayService = gatewayService;
    }

    @PostMapping
    public ResponseEntity createPayment(@RequestBody CreatePaymentDto createPaymentDto, @RequestParam(required = false) boolean preAuth) {
        return ResponseEntity.ok(gatewayService.createPayment(createPaymentDto, preAuth));
    }

    @PostMapping("complete")
    public ResponseEntity complete(@RequestBody FinanceOperationDto financeOperationDto) {
        return ResponseEntity.ok(gatewayService.completePayment(financeOperationDto));
    }

    @PostMapping("get-payment")
    public ResponseEntity getPayment(@RequestBody FinanceOperationDto financeOperationDto) {
        return ResponseEntity.ok(gatewayService.getPaymentInfo(financeOperationDto));
    }

    @PostMapping("reverse")
    public ResponseEntity reversePayment(@RequestBody FinanceOperationDto financeOperationDto) {
        return ResponseEntity.ok(gatewayService.reversePayment(financeOperationDto));
    }

    @PostMapping("refund")
    public ResponseEntity refundPayment(@RequestBody FinanceOperationDto financeOperationDto) {
        return ResponseEntity.ok(gatewayService.refundPayment(financeOperationDto));
    }
}
