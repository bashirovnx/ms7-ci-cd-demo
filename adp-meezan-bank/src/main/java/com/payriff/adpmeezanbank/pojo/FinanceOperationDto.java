package com.payriff.adpmeezanbank.pojo;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@EqualsAndHashCode(callSuper = true)
@Data
@Builder
public class FinanceOperationDto extends CredentialDto {
    private BigDecimal amount;
    private String orderId;
}
