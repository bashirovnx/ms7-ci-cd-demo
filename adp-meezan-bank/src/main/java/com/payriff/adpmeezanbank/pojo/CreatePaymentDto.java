package com.payriff.adpmeezanbank.pojo;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.math.BigDecimal;
import java.util.Map;

@Data
@Builder
public class CreatePaymentDto extends CredentialDto {
    private Long orderNumber;
    private BigDecimal amount;
    private String currency;
    private String returnUrl;
    private ThreeDS2Params threeDS2Params = new ThreeDS2Params();
    private String failUrl;


    @Data
    public static class ThreeDS2Params {
        private String deviceChannel = "02";
    }
}
