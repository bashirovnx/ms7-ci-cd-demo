package com.payriff.adpmeezanbank.pojo;

import lombok.Data;

@Data
public class CredentialDto {

    private String username;
    private String password;

}
