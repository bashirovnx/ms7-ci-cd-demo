package com.payriff.adpmeezanbank.client;

import com.payriff.adpmeezanbank.pojo.CreatePaymentDto;
import com.payriff.adpmeezanbank.pojo.FinanceOperationDto;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;

@Service
@RequiredArgsConstructor
public class MeezanClient {
    private final RestTemplate restTemplate;
    @Value("${bank.ecom.username}")
    private String userName;
    @Value("${bank.ecom.password}")
    private String password;
    @Value("${bank.ecom.url}")
    private String BASE_URL;

    private final Environment environment;


    public String createPayment(CreatePaymentDto createPaymentDto) {
        return createPayment(createPaymentDto, Boolean.FALSE);
    }

    public String createPayment(CreatePaymentDto createPaymentDto, boolean preAuth) {
        var data = setCredential(createPaymentDto.getUsername(), createPaymentDto.getPassword());
        var headers = setUpHeaders();
        var entity = new HttpEntity<>(data, headers);
        data.add("orderNumber", createPaymentDto.getOrderNumber().toString());
        data.add("currency", createPaymentDto.getCurrency());
        data.add("amount", createPaymentDto.getAmount().multiply(BigDecimal.valueOf(100)).stripTrailingZeros().toPlainString());
        data.add("returnUrl", createPaymentDto.getReturnUrl());
        data.add("failUrl", createPaymentDto.getFailUrl());
        data.add("threeDS2Params", createPaymentDto.getThreeDS2Params());
        return paymentEnginExecute(preAuth ? GatewayOperations.REGISTER_PRE_AUTH : GatewayOperations.REGISTER, entity);
    }

    public Object complete(FinanceOperationDto financeOperationDto) {
        var data = setCredential(financeOperationDto.getUsername(), financeOperationDto.getPassword());
        var headers = setUpHeaders();
        var entity = new HttpEntity<>(data, headers);
        data.add("orderId", financeOperationDto.getOrderId());
        data.add("amount", financeOperationDto.getAmount().multiply(BigDecimal.valueOf(100)).stripTrailingZeros().toPlainString());
        return paymentEnginExecute(GatewayOperations.COMPLETE, entity);
    }

    public String getPaymentInfo(FinanceOperationDto financeOperationDto) {
        var data = setCredential(financeOperationDto.getUsername(), financeOperationDto.getPassword());
        var headers = setUpHeaders();
        var entity = new HttpEntity<>(data, headers);
        data.add("orderId", financeOperationDto.getOrderId());
        return paymentEnginExecute(GatewayOperations.GET_PAYMENT, entity);
    }

    public String reverse(FinanceOperationDto financeOperationDto) {
        var data = setCredential(financeOperationDto.getUsername(), financeOperationDto.getPassword());
        var headers = setUpHeaders();
        var entity = new HttpEntity<>(data, headers);
        data.add("orderId", financeOperationDto.getOrderId());
        return paymentEnginExecute(GatewayOperations.REVERSE, entity);
    }

    public String refund(FinanceOperationDto financeOperationDto) {
        var data = setCredential(financeOperationDto.getUsername(), financeOperationDto.getPassword());
        var headers = setUpHeaders();
        var entity = new HttpEntity<>(data, headers);
        data.add("orderId", financeOperationDto.getOrderId());
        data.add("amount", financeOperationDto.getAmount().multiply(BigDecimal.valueOf(100)).stripTrailingZeros().toPlainString());
        return paymentEnginExecute(GatewayOperations.REFUND, entity);
    }

    private MultiValueMap<String, Object> setCredential(String reqUsername, String reqPassword) {

        MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();

        if (environment.acceptsProfiles(Profiles.of("prod"))) {
            map.add("userName", reqUsername);
            map.add("password", reqPassword);
        } else {
            map.add("userName", userName);
            map.add("password", password);
        }
        return map;
    }

    private HttpHeaders setUpHeaders() {
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        return headers;
    }

    private String paymentEnginExecute(GatewayOperations operation, HttpEntity entity) {
        var response = restTemplate.exchange(BASE_URL.concat(operation.value), HttpMethod.POST, entity, String.class);
        return response.getBody();
    }


    enum GatewayOperations {
        REGISTER("register.do"),
        GET_PAYMENT("getOrderStatusExtended.do"),
        REGISTER_PRE_AUTH("registerPreAuth.do"),
        COMPLETE("deposit.do"),
        REVERSE("reverse.do"),
        REFUND("refund.do");

        GatewayOperations(String value) {
            this.value = value;
        }

        private final String value;


    }
}
