//package com.payriff.adpmeezanbank.client;
//
//import com.payriff.adpmeezanbank.pojo.CreatePaymentDto;
//import com.payriff.adpmeezanbank.pojo.FinanceOperationDto;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit.jupiter.SpringExtension;
//
//import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT;
//
//@ExtendWith(SpringExtension.class)
//@SpringBootTest(webEnvironment = DEFINED_PORT)
//class MeezanClientTest {
//
//
//    @Autowired
//    private MeezanClient neezanClient;
//    private static final String DUMMY_ORDER_ID = "84d6d8b1-34e8-4a7d-bad2-443b4d1b1f81";
//
//    CreatePaymentDto createPaymentDto;
//
//    @BeforeEach
//    void setup() {
//        createPaymentDto = CreatePaymentDto.builder()
//                .amount(523L)
//                .orderNumber(randomOrderNumber())
//                .currency("586")
//                .build();
//    }
//
//    @Test
//    void testCreatePayment() {
//        String payment = neezanClient.createPayment(createPaymentDto);
//        System.out.println(payment);
//    }
//
//    @Test
//    void testGetPayment() {
//        String payment = neezanClient.getPaymentInfo(DUMMY_ORDER_ID);
//        System.out.println(payment);
//    }
//
//    @Test
//    void testPaymentReverse() {
//        String payment = neezanClient.reverse(DUMMY_ORDER_ID);
//        System.out.println(payment);
//    }
//
//    @Test
//    void testPaymentRefund() {
//        FinanceOperationDto financeOperationDto = FinanceOperationDto.builder().amount(523L).orderId(DUMMY_ORDER_ID).build();
//        String payment = neezanClient.refund(financeOperationDto);
//        System.out.println(payment);
//    }
//
//
//    public Long randomOrderNumber() {
//        long leftLimit = 1L;
//        long rightLimit = 10L;
//        return leftLimit + (long) (Math.random() * (rightLimit - leftLimit));
//    }
//}
//
