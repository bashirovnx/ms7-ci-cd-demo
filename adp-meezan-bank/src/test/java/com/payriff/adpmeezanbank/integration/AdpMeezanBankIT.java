package com.payriff.adpmeezanbank.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.payriff.adpmeezanbank.pojo.CreatePaymentDto;
import com.payriff.adpmeezanbank.pojo.FinanceOperationDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
@ActiveProfiles("local")
public class AdpMeezanBankIT {
//    private static final String BASE_URL = "/api/meezan-bank";
//    private static final String SLASH = "/";
//    private static final String DUMMY_ORDER_ID = "12cb5564-ad73-4b73-9820-3d4bb2f3d92b";
//
//    @Autowired
//    private MockMvc mvc;
//    @Autowired
//    private ObjectMapper objectMapper;
//    CreatePaymentDto createPaymentDto;
//
//    @BeforeEach
//    void setup() {
//        createPaymentDto = CreatePaymentDto.builder()
//                .amount(300L)
//                .orderNumber(randomOrderNumber())
//                .currency("586").build();
//    }
//
//
//    @Test
//    void whenValidRequestThenOrderShouldBeCreated() throws Exception {
//        mvc.perform(post(BASE_URL).accept(MediaType.APPLICATION_JSON)
//                        .content(objectMapper.writeValueAsString(createPaymentDto))
//                        .contentType(MediaType.APPLICATION_JSON))
//                .andDo(MockMvcResultHandlers.print())
//                .andExpect(status().isOk())
//                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$.errorCode").value("0"))
//                .andExpect(jsonPath("$.formUrl").isNotEmpty())
//                .andExpect(jsonPath("$.orderId").isNotEmpty());
//    }
//
//    @Test
//    void whenValidRequestThenPreAuthOrderShouldBeCreated() throws Exception {
//        mvc.perform(post(BASE_URL.concat("?preAuth=true"))
//                        .accept(MediaType.APPLICATION_JSON)
//                        .content(objectMapper.writeValueAsString(createPaymentDto))
//                        .contentType(MediaType.APPLICATION_JSON))
//                .andDo(MockMvcResultHandlers.print())
//                .andExpect(status().isOk())
//                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$.errorCode").value("0"))
//                .andExpect(jsonPath("$.formUrl").isNotEmpty())
//                .andExpect(jsonPath("$.orderId").isNotEmpty());
//    }
//
//    @Test
//    void whenValidRequestThenPreAuthOrderCompleteShouldBeFail() throws Exception {
//        mvc.perform(post(BASE_URL.concat("/complete"))
//                        .content(objectMapper.writeValueAsString(FinanceOperationDto.builder().amount(createPaymentDto.getAmount()).orderId(DUMMY_ORDER_ID).build()))
//                        .accept(MediaType.APPLICATION_JSON)
//                        .contentType(MediaType.APPLICATION_JSON))
//                .andDo(MockMvcResultHandlers.print())
//                .andExpect(status().isOk())
//                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$.errorCode").isNotEmpty())
//                .andExpect(jsonPath("$.errorMessage").isNotEmpty())
//                .andExpect(jsonPath("$.errorMessage").exists());
//    }
//
//
//    @Test
//    void whenValidOrderIdThenGetInfoShouldBeOk() throws Exception {
//        mvc.perform(get(BASE_URL.concat(SLASH).concat(DUMMY_ORDER_ID))
//                        .accept(MediaType.APPLICATION_JSON)
//                        .contentType(MediaType.APPLICATION_JSON))
//                .andDo(MockMvcResultHandlers.print())
//                .andExpect(status().isOk())
//                .andExpect(content()
//                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$.errorCode").value("0"))
//                .andExpect(jsonPath("$.orderNumber").isNotEmpty())
//                .andExpect(jsonPath("$.attributes[0].value").isNotEmpty())
//                .andExpect(jsonPath("$.orderNumber").isNotEmpty());
//    }
//
//
//    @Test
//    void whenPaymentNoPayThenReverseError() throws Exception {
//        mvc.perform(post(BASE_URL.concat("/reverse/").concat(DUMMY_ORDER_ID)).accept(MediaType.APPLICATION_JSON)
//                        .contentType(MediaType.APPLICATION_JSON))
//                .andDo(MockMvcResultHandlers.print())
//                .andExpect(status().isOk())
//                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$.errorCode").value("7"))
//                .andExpect(jsonPath("$.errorMessage").isNotEmpty())
//                .andExpect(jsonPath("$.errorMessage").value("Reversal is impossible for current transaction state"));
//
//    }
//
//    @Test
//    void whenPaymentNoPayThenRefundError() throws Exception {
//        mvc.perform(post(BASE_URL.concat("/refund/"))
//                        .content(objectMapper.writeValueAsString(FinanceOperationDto.builder().amount(createPaymentDto.getAmount()).orderId(DUMMY_ORDER_ID).build()))
//                        .accept(MediaType.APPLICATION_JSON)
//                        .contentType(MediaType.APPLICATION_JSON))
//                .andDo(MockMvcResultHandlers.print())
//                .andExpect(status().isOk())
//                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$.errorCode").isNotEmpty())
//                .andExpect(jsonPath("$.errorMessage").isNotEmpty())
//                .andExpect(jsonPath("$.errorMessage").exists());
//
//    }
//
//    public Long randomOrderNumber() {
//        long leftLimit = 1L;
//        long rightLimit = 10L;
//        return leftLimit + (long) (Math.random() * (rightLimit - leftLimit));
//    }
}
