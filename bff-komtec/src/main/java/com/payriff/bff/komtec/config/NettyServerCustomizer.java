//package com.payriff.bff.komtec.config;
//
//import io.netty.channel.ChannelHandlerContext;
//import io.netty.channel.ChannelInboundHandlerAdapter;
//import io.netty.handler.codec.http.HttpRequest;
//import org.springframework.boot.web.embedded.netty.NettyReactiveWebServerFactory;
//import org.springframework.boot.web.server.WebServerFactoryCustomizer;
//import org.springframework.stereotype.Component;
//import reactor.netty.ConnectionObserver;
//import reactor.netty.NettyPipeline;
//
//import java.net.URLEncoder;
//import java.nio.charset.StandardCharsets;
//import java.util.Arrays;
//
//@Component
//public class NettyServerCustomizer
//        implements WebServerFactoryCustomizer<NettyReactiveWebServerFactory> {
//
//    private final GatewayConfig config;
//
//    public NettyServerCustomizer(GatewayConfig config) {
//        this.config = config;
//    }
//
//    @Override
//    public void customize(NettyReactiveWebServerFactory factory) {
//        factory.addServerCustomizers(httpServer ->
//                httpServer.childObserve((conn, state) -> {
//                    if (state == ConnectionObserver.State.CONNECTED) {
//                        conn.channel()
//                                .pipeline()
//                                .addAfter(NettyPipeline.HttpCodec, "", new QueryHandler());
//                    }
//                }));
//    }
//
//    final class QueryHandler extends ChannelInboundHandlerAdapter {
//
//        @Override
//        public void channelRead(ChannelHandlerContext ctx, Object msg) {
//            if (msg instanceof HttpRequest) {
//                HttpRequest request = (HttpRequest) msg;
//                String url = request.uri();
//
//                System.out.println("Orginal URL: " + url);
//                String encodedUrl = filter(url);
//                System.out.println("Encoded URL: " + encodedUrl);
//                request.setUri(encodedUrl);
//
//            }
//            ctx.fireChannelRead(msg);
//        }
//
//        private String filter(String url) {
//
//            String[] filterable = config.getFilterable();
//            System.out.println("Filterable: " + Arrays.toString(filterable));
//
//            //Some clients use special characters (as |) in url. Gateway use Netty server and encode url here
//            for (String service : filterable) {
//                if (url.contains(service)) {
//                    return encodeSpecialCharacters(url);
//                }
//            }
//            return url;
//        }
//
//        private String encodeSpecialCharacters(String url) {
//
//            String[] characters = config.getAllowedSpecialCharacters();
//            System.out.println("Characters: " + Arrays.toString(characters));
//
//            for (String character : characters) {
//                url = url.replace(character, URLEncoder.encode(character, StandardCharsets.UTF_8));
//            }
//            return url;
//        }
//
//        @Override
//        public boolean isSharable() {
//            return true;
//        }
//    }
//
//}