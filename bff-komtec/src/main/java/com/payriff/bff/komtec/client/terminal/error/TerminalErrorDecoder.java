package com.payriff.bff.komtec.client.terminal.error;

import feign.Response;
import feign.codec.Decoder;
import feign.codec.ErrorDecoder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
@RequiredArgsConstructor
public class TerminalErrorDecoder implements ErrorDecoder {

    private final Decoder decoder;

    private static void httpStatusDecoder(int status) {

        switch (status) {
            case 401:
            case 403:
                throw new TerminalException("AUTHORIZATION_ERROR");
        }
    }

    public Exception decode(String methodKey, Response response) {

        httpStatusDecoder(response.status());

        TerminalResult error;
        try {
            error = (TerminalResult) decoder.decode(response, TerminalResult.class);
        } catch (Exception e) {
            return new TerminalException("INTERNAL_ERROR");
        }

        return new TerminalException(error.getCode(), error.getMessage());
    }

}
