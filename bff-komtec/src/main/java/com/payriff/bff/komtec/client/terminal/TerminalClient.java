package com.payriff.bff.komtec.client.terminal;

import com.payriff.bff.komtec.client.terminal.model.request.InvoiceDetailRequestDto;
import com.payriff.bff.komtec.client.terminal.model.request.TerminalPayRequestDto;
import com.payriff.bff.komtec.client.terminal.model.response.InvoiceResponseDto;
import com.payriff.bff.komtec.client.terminal.model.response.TerminalPayResponseDto;
import com.payriff.bff.komtec.config.FeignConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(
        name = "ms-terminal",
        configuration = FeignConfiguration.class
)
public interface TerminalClient {

    @PostMapping("/api/v1/terminal/invoice-details")
    InvoiceResponseDto getInvoiceDetails(@RequestBody InvoiceDetailRequestDto request);

    @PostMapping("/api/v1/terminal/pay")
    TerminalPayResponseDto pay(@RequestBody TerminalPayRequestDto request);

    @GetMapping("/api/v1/terminal/transaction-info/{rrn}")
    TerminalPayResponseDto getTransactionInfo(@PathVariable("rrn") String rrn);

}
