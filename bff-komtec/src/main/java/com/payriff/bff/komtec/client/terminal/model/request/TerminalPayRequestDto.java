package com.payriff.bff.komtec.client.terminal.model.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TerminalPayRequestDto {

    private BigDecimal amount;
    private String backRrn;
    private boolean partial;
    private String src;

}
