package com.payriff.bff.komtec.service;

import com.payriff.bff.komtec.client.terminal.TerminalClient;
import com.payriff.bff.komtec.client.terminal.error.TerminalException;
import com.payriff.bff.komtec.client.terminal.model.TerminalInvoiceReqType;
import com.payriff.bff.komtec.client.terminal.model.request.InvoiceDetailRequestDto;
import com.payriff.bff.komtec.client.terminal.model.request.TerminalPayRequestDto;
import com.payriff.bff.komtec.client.terminal.model.response.InvoiceResponseDto;
import com.payriff.bff.komtec.client.terminal.model.response.TerminalPayResponseDto;
import com.payriff.bff.komtec.dto.response.KomtecCheckResponseDto;
import com.payriff.bff.komtec.dto.response.KomtecPayResponseDto;
import com.payriff.bff.komtec.error.KomtecException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.Objects;

import static com.payriff.bff.komtec.client.terminal.model.TerminalResult.TerminalStatus.SUCCESS;
import static com.payriff.bff.komtec.dto.KomtecConstant.*;

@Slf4j
@Service
@RequiredArgsConstructor
public class KomtecService {

    private final TerminalClient terminalClient;

    public ResponseEntity<?> routerByCommand(
            String command,
            String account,
            String txnId,
            String sum,
            String txn_date
    ) {

        log.info(String.format("Komtec request: command:[%s], account:[%s], txnId:[%s], sum:[%s], txn_date:[%s]",
                command, account, txnId, sum, txn_date
        ));

        switch (command) {
            case "check":
                return check(account, txnId);
            case "pay":
                return pay(sum, txnId);
            case "status":
                return status(txnId);
            default:
                throw new IllegalArgumentException("Command is undefined: " + command);
        }
    }

    private ResponseEntity<KomtecCheckResponseDto> check(String account, String txnId) {

        InvoiceDetailRequestDto request = checkRequestBuilder(account);

        request.setRrn(txnId);

        InvoiceResponseDto detail;
        try {
            detail = terminalClient.getInvoiceDetails(request);
        } catch (TerminalException ex) {
            log.error("Error on terminal check", ex);
            throw new KomtecException(
                    txnId,
                    ex.getCode(),
                    ex.getMessage()
            );
        }

        KomtecCheckResponseDto body = KomtecCheckResponseDto.builder()
                .addInfo(infoBuilder(detail))
                .osmpTxnId(detail.getBackRrn())
                .build();

        if (SUCCESS.equals(detail.getStatus())) {
            body.setResult(RESULT_SUCCESS.getValue());
            body.setComment(COMMENT_OK.getValue());

            log.info("Success response: " + body);
            return ResponseEntity.ok()
                    .contentType(MediaType.APPLICATION_XML)
                    .body(body);

        } else {
            body.setResult(RESULT_ERROR.getValue());
            body.setComment(COMMENT_NOT_OK.getValue());

            log.info("Error response: " + body);
            return ResponseEntity.badRequest()
                    .contentType(MediaType.APPLICATION_XML)
                    .body(body);
        }
    }

    private String infoBuilder(InvoiceResponseDto detail) {

        LocalDate expireDate = LocalDate.now().plusDays(30L);

        if (!Objects.isNull(detail.getExpiryDate())) {
            expireDate = detail.getExpiryDate().toLocalDate();
        }

        return nvl(detail.getSenderCompany(), "PayRiff")
                .concat("||")
                .concat(nvl(detail.getFullName(), "Fullname"))
                .concat("||")
                .concat(getAmount(detail.getAmount()))
                .concat("||")
                .concat(getMaxAmount(detail.getMaxAmount()))
                .concat("||")
                .concat(nvl(detail.getPhoneNumber(), "+994xxxxxxxxxx"))
                .concat("||")
                .concat(nvl(detail.getEmail(), "info@payriff.com"))
                .concat("||")
                .concat(expireDate.toString())
                .concat("||")
                .concat(nvl(detail.getDescription(), "Default description"));
    }

    private String nvl(String value, String defaultValue) {
        if (!Objects.isNull(value)) {
            return value;
        } else {
            return defaultValue;
        }
    }

    private String getAmount(BigDecimal amount) {

        if (!Objects.isNull(amount)) {
            return String.valueOf(amount.setScale(0, RoundingMode.FLOOR));
        } else {
            return "0";
        }

    }

    private String getMaxAmount(BigDecimal amount) {

        if (!Objects.isNull(amount)) {
            return String.valueOf(amount.setScale(0, RoundingMode.FLOOR));
        } else {
            return "10000";
        }

    }

    private InvoiceDetailRequestDto checkRequestBuilder(String account) {

        String decoded = URLDecoder.decode(account, StandardCharsets.UTF_8);

        String[] req = decoded.split("\\|\\|");

        return InvoiceDetailRequestDto.builder()
                .src(req[0])
                .type(TerminalInvoiceReqType.valueOf(req[1]))
                .value(req[2])
                .build();
    }

    private ResponseEntity<KomtecPayResponseDto> pay(String sum, String txnId) {

        TerminalPayRequestDto payRequest = TerminalPayRequestDto.builder()
                .amount(new BigDecimal(sum))
                .backRrn(txnId)
                .build();

        TerminalPayResponseDto payResponse;
        try {
            payResponse = terminalClient.pay(payRequest);
        } catch (TerminalException ex) {
            log.error("Error on terminal pay", ex);
            throw new KomtecException(
                    txnId,
                    ex.getCode(),
                    ex.getMessage()
            );
        }

        KomtecPayResponseDto response = KomtecPayResponseDto.builder()
                .amount(payResponse.getAmount())
                .prvTxn(txnId)
                .build();

        response.setOsmpTxnId(txnId);

        if (SUCCESS.equals(payResponse.getStatus())) {
            response.setResult(RESULT_SUCCESS.getValue());
            response.setComment(COMMENT_OK.getValue());

            log.info("Success response: " + response);
            return ResponseEntity.ok()
                    .contentType(MediaType.APPLICATION_XML)
                    .body(response);
        } else {
            response.setResult(RESULT_ERROR.getValue());
            response.setComment(COMMENT_NOT_OK.getValue());

            log.info("Error response: " + response);
            return ResponseEntity.badRequest()
                    .contentType(MediaType.APPLICATION_XML)
                    .body(response);
        }
    }

    private ResponseEntity<KomtecPayResponseDto> status(String txnId) {

        TerminalPayResponseDto info;
        try {
            info = terminalClient.getTransactionInfo(txnId);
        } catch (TerminalException ex) {
            log.error("Error on terminal status", ex);
            throw new KomtecException(
                    txnId,
                    ex.getCode(),
                    ex.getMessage()
            );
        }

        KomtecPayResponseDto response = KomtecPayResponseDto.builder()
                .amount(info.getAmount())
                .prvTxn(txnId)
                .build();

        response.setOsmpTxnId(txnId);

        if (SUCCESS.equals(info.getStatus())) {
            response.setResult(RESULT_SUCCESS.getValue());
            response.setComment(COMMENT_OK.getValue());

            log.info("Success response: " + response);
            return ResponseEntity.ok()
                    .contentType(MediaType.APPLICATION_XML)
                    .body(response);
        } else {
            response.setResult(RESULT_ERROR.getValue());
            response.setComment(COMMENT_NOT_OK.getValue());

            log.info("Error response: " + response);
            return ResponseEntity.badRequest()
                    .contentType(MediaType.APPLICATION_XML)
                    .body(response);
        }
    }

}
