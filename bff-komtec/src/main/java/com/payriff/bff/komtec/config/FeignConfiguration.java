package com.payriff.bff.komtec.config;

import com.payriff.bff.komtec.client.terminal.error.TerminalErrorDecoder;
import feign.Logger;
import feign.codec.Decoder;
import feign.codec.ErrorDecoder;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;

@RequiredArgsConstructor
public class FeignConfiguration {

    final Decoder decoder;

    @Bean
    public Logger.Level feignLoggerLevel() {
        return Logger.Level.FULL;
    }

    @Bean
    public ErrorDecoder feignErrorDecoder() {
        return new TerminalErrorDecoder(decoder);
    }

}
