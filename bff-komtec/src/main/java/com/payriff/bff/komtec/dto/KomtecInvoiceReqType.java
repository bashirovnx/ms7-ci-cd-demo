package com.payriff.bff.komtec.dto;

public enum KomtecInvoiceReqType {

    INVOICE_CODE,
    PIN_CODE

}
