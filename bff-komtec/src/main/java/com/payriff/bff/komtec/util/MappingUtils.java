//package com.payriff.bff.komtec.util;
//
//import com.fasterxml.jackson.core.JsonProcessingException;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.payriff.bff.komtec.client.terminal.model.KomtecInvoiceReqType;
//import com.payriff.bff.komtec.client.terminal.model.request.InvoiceDetailRequestDto;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.http.NameValuePair;
//import org.apache.http.client.utils.URLEncodedUtils;
//
//import java.io.UnsupportedEncodingException;
//import java.net.URI;
//import java.net.URLDecoder;
//import java.nio.charset.StandardCharsets;
//import java.util.HashMap;
//import java.util.LinkedHashMap;
//import java.util.List;
//import java.util.Map;
//
//@Slf4j
//public class MappingUtils {
//
//    public static String mappingToBody(URI uri) {
//
//        String decoded = URLDecoder.decode(getAccountValue(uri), StandardCharsets.UTF_8);
//
//        String[] req = decoded.split("\\|\\|");
//
//        InvoiceDetailRequestDto request = InvoiceDetailRequestDto.builder()
//                .src(req[0])
//                .type(KomtecInvoiceReqType.valueOf(req[1]))
//                .value(req[2])
//                .build();
//
//        try {
//            return new ObjectMapper().writeValueAsString(request);
//        } catch (JsonProcessingException e) {
//            log.error("Error on mapping to body", e);
//            return "{}";
//        }
//    }
//
//    private static String getAccountValue(URI url) {
//
//        List<NameValuePair> params = URLEncodedUtils.parse(url, StandardCharsets.UTF_8);
//
//        return params.stream()
//                .filter(key -> key.getName().equalsIgnoreCase("account"))
//                .findFirst()
//                .orElseThrow(() -> new IllegalArgumentException("Account key not found"))
//                .getValue();
//    }
//
//    public static String mappingToURL(String url) {
//        String rewriteURL = "/api/v1";
//
//        Map<String, String> params = new HashMap<>();
//        try {
//            params = splitQuery(url);
//        } catch (UnsupportedEncodingException e) {
//            log.error("Error on splitQuery", e);
//        }
//
//        String command = params.get("command");
//
//        switch (command) {
//            case "check":
//                rewriteURL = rewriteURL.concat("/terminal/invoice-details");
//                break;
//            case "pay":
//                rewriteURL = rewriteURL.concat("/terminal/pay");
//                break;
//            case "status":
//                rewriteURL = rewriteURL.concat("/terminal/transaction-info/".concat(params.get("txn_id")));
//                break;
//            default:
//                throw new IllegalArgumentException("Command is undefined: " + command);
//
//        }
//        log.info("Rewrite url: " + rewriteURL);
//
//        return rewriteURL;
//    }
//
//
//    private static Map<String, String> splitQuery(String query) throws UnsupportedEncodingException {
//        Map<String, String> query_pairs = new LinkedHashMap<String, String>();
//        String[] pairs = query.split("&");
//        for (String pair : pairs) {
//            int idx = pair.indexOf("=");
//            int startIdx = Math.max(pair.indexOf("?") + 1, 0);
//            query_pairs.put(
//                    URLDecoder.decode(pair.substring(startIdx, idx), StandardCharsets.UTF_8),
//                    URLDecoder.decode(pair.substring(idx + 1), StandardCharsets.UTF_8)
//            );
//        }
//        return query_pairs;
//    }
//
//}
