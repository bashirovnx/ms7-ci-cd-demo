package com.payriff.bff.komtec.client.terminal.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.payriff.bff.komtec.client.terminal.model.TerminalResult;
import com.payriff.bff.komtec.client.terminal.model.TerminalTransStatus;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@EqualsAndHashCode(callSuper = false)
public class TerminalPayResponseDto extends TerminalResult {

    private String invoiceCode;
    private BigDecimal amount;
    private TerminalTransStatus transStatus;
    private Date transactionDate;
    private String note;
    private boolean partial;

}
