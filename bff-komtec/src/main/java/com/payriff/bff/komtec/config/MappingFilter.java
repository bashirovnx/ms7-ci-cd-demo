//package com.payriff.bff.komtec.config;
//
//import com.payriff.bff.komtec.util.MappingUtils;
//import lombok.Builder;
//import lombok.Data;
//import lombok.RequiredArgsConstructor;
//import lombok.extern.slf4j.Slf4j;
//import org.reactivestreams.Publisher;
//import org.springframework.cloud.context.config.annotation.RefreshScope;
//import org.springframework.cloud.gateway.filter.GatewayFilter;
//import org.springframework.cloud.gateway.filter.GatewayFilterChain;
//import org.springframework.cloud.gateway.filter.factory.GatewayFilterFactory;
//import org.springframework.cloud.gateway.filter.factory.rewrite.ModifyRequestBodyGatewayFilterFactory;
//import org.springframework.cloud.gateway.filter.factory.rewrite.ModifyResponseBodyGatewayFilterFactory;
//import org.springframework.cloud.gateway.filter.factory.rewrite.RewriteFunction;
//import org.springframework.http.server.reactive.ServerHttpRequest;
//import org.springframework.stereotype.Component;
//import org.springframework.web.server.ServerWebExchange;
//import reactor.core.publisher.Mono;
//
//import java.net.URI;
//
//@RefreshScope
//@Component
//@Slf4j
//@RequiredArgsConstructor
//public class MappingFilter implements GatewayFilterFactory<MappingFilter.Config> {
//
//    private final ModifyRequestBodyGatewayFilterFactory factory;
//
//    @Override
//    public GatewayFilter apply(MappingFilter.Config config) {
//        return (this::filter);
//    }
//
//    @Override
//    public Class<Config> getConfigClass() {
//        return MappingFilter.Config.class;
//    }
//
//    public static class Config {
//    }
//
//    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
//
//        log.info("MappingFilter start");
//
//        ServerHttpRequest request = exchange.getRequest();
//
//        ServerWebExchange mutatedExchange = mappingToPath(exchange, request);
//
//        GatewayFilter modifyBodyFilter = mappingToBody(request);
//
//        return modifyBodyFilter.filter(mutatedExchange, chain);
//
//    }
//
//    private GatewayFilter mappingToBody(ServerHttpRequest request) {
//
//        String body = MappingUtils.mappingToBody(request.getURI());
//
//        log.info("Request body: " + body);
//
//        ModifyRequestBodyGatewayFilterFactory.Config cfg = new ModifyRequestBodyGatewayFilterFactory.Config();
//
//        cfg.setRewriteFunction(String.class, String.class, new RewriteBodyFunction(body));
//
//        return factory.apply(cfg);
//    }
//
//    private ServerWebExchange mappingToPath(ServerWebExchange exchange, ServerHttpRequest request) {
//
//        String path = MappingUtils.mappingToURL(request.getURI().toASCIIString());
//
//        ServerHttpRequest modifiedRequest = request.mutate()
//                .path(path)
//                .uri(URI.create(path))
//                .build();
//
//        return exchange.mutate()
//                .request(modifiedRequest)
//                .build();
//    }
//
//    public static class RewriteBodyFunction implements RewriteFunction<String, String> {
//
//        private final String newBody;
//
//        public RewriteBodyFunction(String newBody) {
//            this.newBody = newBody;
//        }
//
//        @Override
//        public Publisher<String> apply(ServerWebExchange serverWebExchange, String oldBody) {
//            try {
//                return Mono.just(newBody);
//            } catch (Exception e) {
//                log.error("Error on RewriteFunction");
//                return Mono.just(oldBody);
//            }
//        }
//    }
//
//    @Data
//    @Builder
//    public static class InvoiceDetailRequestDto {
//
//        private String src;
//        private KomtecInvoiceReqType type;
//        private String value;
//
//    }
//
//    public enum KomtecInvoiceReqType {
//
//        INVOICE_CODE,
//        PIN_CODE
//
//    }
//
//}