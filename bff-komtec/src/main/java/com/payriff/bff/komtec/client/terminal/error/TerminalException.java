package com.payriff.bff.komtec.client.terminal.error;


import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class TerminalException extends RuntimeException {

    private String id;
    private String code;

    public TerminalException(String message) {
        super(message);
    }

    public TerminalException(String code, String message) {
        super(message);
        this.code = code;
    }

    public TerminalException(String id, String code, String message) {
        super(message);
        this.id = id;
        this.code = code;
    }

}
