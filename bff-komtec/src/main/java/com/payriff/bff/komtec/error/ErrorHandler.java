package com.payriff.bff.komtec.error;

import com.payriff.bff.komtec.dto.KomtecConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
@Slf4j
public class ErrorHandler extends ResponseEntityExceptionHandler {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({KomtecException.class})
    public ResponseEntity<KomtecErrorResult> handleServiceException(KomtecException ex) {

        return ResponseEntity.badRequest()
                .contentType(MediaType.APPLICATION_XML)
                .body(KomtecErrorResult.builder()
                        .osmpTxnId(ex.getId())
                        .result(KomtecConstant.RESULT_ERROR.getValue())
                        .comment("NOT OK")
                        .addInfo(ex.getMessage())
                        .build());
    }

}