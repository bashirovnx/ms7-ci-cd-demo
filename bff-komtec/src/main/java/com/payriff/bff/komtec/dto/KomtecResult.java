package com.payriff.bff.komtec.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class KomtecResult {

    @JsonProperty("osmp_txn_id")
    private String osmpTxnId;

    @JsonProperty("result")
    private String result;

    @JsonProperty("comment")
    private String comment;

    @JsonProperty("addinfo")
    private String addInfo;

}
