package com.payriff.bff.komtec.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties("service")
public class KomtecConfig {

    private Security security;
    private String[] filterable;
    private String[] allowedSpecialCharacters;

    @Data
    public static class Security {

        private External external;
        private Internal internal;

        @Data
        public static class External {
            private String username;
            private String password;
        }

        @Data
        public static class Internal {
            private String source;
            private String authorization;
        }
    }
}
