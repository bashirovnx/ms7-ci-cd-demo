package com.payriff.bff.komtec.client.terminal.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TerminalResult {

    private TerminalStatus status;
    private String code;
    private String message;

    public enum TerminalStatus {
        SUCCESS,
        ERROR
    }

}
