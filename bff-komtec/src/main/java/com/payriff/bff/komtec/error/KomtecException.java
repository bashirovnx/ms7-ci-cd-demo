package com.payriff.bff.komtec.error;


import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class KomtecException extends RuntimeException {

    private String id;
    private String code;

    public KomtecException(String message) {
        super(message);
    }

    public KomtecException(String code, String message) {
        super(message);
        this.code = code;
    }

    public KomtecException(String id, String code, String message) {
        super(message);
        this.id = id;
        this.code = code;
    }
}
