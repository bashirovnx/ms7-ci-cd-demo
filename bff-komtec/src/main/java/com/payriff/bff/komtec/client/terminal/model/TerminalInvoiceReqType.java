package com.payriff.bff.komtec.client.terminal.model;

public enum TerminalInvoiceReqType {

    INVOICE_CODE,
    PIN_CODE,
    CUSTOMER_CODE

}
