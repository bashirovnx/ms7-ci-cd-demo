package com.payriff.bff.komtec.dto;

public enum KomtecTransStatus {

    PENDING,
    ERROR,
    EXPIRED,
    PARTIAL,
    COMPLETE

}
