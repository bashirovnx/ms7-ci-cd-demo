package com.payriff.bff.komtec.error;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonRootName("response")
public class KomtecErrorResult {

    @JsonProperty("osmp_txn_id")
    private String osmpTxnId;
    private String result;
    private String comment;
    @JsonProperty("addinfo")
    private String addInfo;

}