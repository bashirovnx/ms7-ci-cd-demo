package com.payriff.bff.komtec.dto;

public enum KomtecConstant {

    COMMENT_OK("OK"),
    COMMENT_NOT_OK("NOT OK"),
    RESULT_SUCCESS("0"),
    RESULT_ERROR("1");

    String value;

    KomtecConstant(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
