package com.payriff.bff.komtec.client.terminal.model;

public enum TerminalTransStatus {

    PENDING,
    ERROR,
    EXPIRED,
    PARTIAL,
    COMPLETE

}
