package com.payriff.bff.komtec.client.terminal.error;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TerminalResult {

    private TerminalStatus status;
    private String code;
    private String message;

    public enum TerminalStatus {
        SUCCESS,
        ERROR
    }

}