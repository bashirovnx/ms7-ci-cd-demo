package com.payriff.bff.komtec;

import com.payriff.bff.komtec.config.KomtecConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableDiscoveryClient
@EnableConfigurationProperties(value = {
        KomtecConfig.class
})
@EnableFeignClients(basePackages = "com.payriff.bff.komtec")
public class BffKomtecApplication {


    public static void main(String[] args) {

        SpringApplication.run(BffKomtecApplication.class, args);

    }

}
