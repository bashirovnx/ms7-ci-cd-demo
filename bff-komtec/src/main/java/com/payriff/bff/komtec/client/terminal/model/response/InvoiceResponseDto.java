package com.payriff.bff.komtec.client.terminal.model.response;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.payriff.bff.komtec.client.terminal.model.CurrencyType;
import com.payriff.bff.komtec.client.terminal.model.TerminalResult;
import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;


@Data
@Builder
@EqualsAndHashCode(callSuper = false)
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
public class InvoiceResponseDto extends TerminalResult {

    private String senderCompany;

    private String fullName;

    private BigDecimal amount;

    private CurrencyType currencyType;

    private String description;

    private String email;

    private String phoneNumber;

    private BigDecimal maxAmount;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime checkinDate;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime expiryDate;

    private String backRrn;

}
