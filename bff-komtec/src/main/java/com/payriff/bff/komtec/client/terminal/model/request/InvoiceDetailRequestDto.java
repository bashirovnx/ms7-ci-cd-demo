package com.payriff.bff.komtec.client.terminal.model.request;

import com.payriff.bff.komtec.client.terminal.model.TerminalInvoiceReqType;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class InvoiceDetailRequestDto {

    private String rrn;
    private String src;
    private TerminalInvoiceReqType type;
    private String value;

}
