//package com.payriff.bff.komtec.security;
//
//
//import lombok.Data;
//import lombok.RequiredArgsConstructor;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.http.entity.ContentType;
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.cloud.gateway.filter.GatewayFilter;
//import org.springframework.cloud.gateway.filter.GatewayFilterChain;
//import org.springframework.cloud.gateway.filter.factory.GatewayFilterFactory;
//import org.springframework.http.HttpMethod;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.server.reactive.ServerHttpRequest;
//import org.springframework.http.server.reactive.ServerHttpResponse;
//import org.springframework.stereotype.Component;
//import org.springframework.web.server.ServerWebExchange;
//import reactor.core.publisher.Mono;
//
//import java.util.Base64;
//import java.util.Objects;
//
//@Component
//@Slf4j
//@RequiredArgsConstructor
//public class SecurityFilter implements GatewayFilterFactory<SecurityFilter.Config> {
//
//    private final Config config;
//
//    @Override
//    public GatewayFilter apply(Config config) {
//        return (this::filter);
//    }
//
//    @Override
//    public Class<Config> getConfigClass() {
//        return Config.class;
//    }
//
//    @Data
//    @ConfigurationProperties("service.security")
//    public static class Config {
//
//        private External external;
//        private Internal internal;
//
//        @Data
//        public static class External {
//            private String username;
//            private String password;
//        }
//
//        @Data
//        public static class Internal {
//            private String source;
//            private String authorization;
//        }
//
//    }
//
//    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
//
//        log.info("SecurityFilter start");
//
//        ServerHttpRequest request = exchange.getRequest();
//
//        log.info("Headers : " + request.getHeaders());
//
//        if (!request.getHeaders().containsKey("Authorization")) {
//            log.error("Authorization header not found!");
//            return this.onError(exchange, "Authorization header not found", HttpStatus.UNAUTHORIZED);
//        }
//
//        String authorizationHeader = Objects.requireNonNull(request.getHeaders().get("Authorization"))
//                .stream()
//                .findFirst().orElseThrow(() -> {
//                    throw new SecurityException("Authorization header not found!");
//                });
//
//        if (!this.isAuthorizationValid(authorizationHeader)) {
//            log.error("Invalid Authorization header!");
//            return this.onError(exchange, "Invalid Authorization header", HttpStatus.UNAUTHORIZED);
//        }
//
//        ServerWebExchange mutatedExchange = internalAuthentication(exchange, request);
//
//        return chain.filter(mutatedExchange);
//    }
//
//    private ServerWebExchange internalAuthentication(ServerWebExchange exchange, ServerHttpRequest request) {
//        ServerHttpRequest modifiedRequest = request.mutate()
//                .method(HttpMethod.POST)
//                .header("Content-Type", ContentType.APPLICATION_JSON.getMimeType())
//                .header("Authorization", config.getInternal().getAuthorization())
//                .header("X-Client", config.getInternal().getSource())
//                .build();
//
//        return exchange.mutate()
//                .request(modifiedRequest)
//                .build();
//    }
//
//    private boolean isAuthorizationValid(String authHeader) {
//
//        boolean authorized = false;
//
//        if (authHeader != null) {
//
//            String[] authHeaderSplit = authHeader.split("\\s");
//
//            for (int i = 0; i < authHeaderSplit.length; i++) {
//                String token = authHeaderSplit[i];
//                if (token.equalsIgnoreCase("Basic")) {
//
//                    String credentials = new String(Base64.getDecoder().decode(authHeaderSplit[i + 1]));
//                    int index = credentials.indexOf(":");
//                    if (index != -1) {
//                        String username = credentials.substring(0, index).trim();
//                        String password = credentials.substring(index + 1).trim();
//
//                        authorized = username.equals(config.getExternal().getUsername()) &&
//                                password.equals(config.getExternal().getPassword());
//                    }
//                }
//            }
//        }
//        log.info("Authorization: " + authorized);
//
//        return authorized;
//    }
//
//    private Mono<Void> onError(ServerWebExchange exchange, String err, HttpStatus httpStatus) {
//        ServerHttpResponse response = exchange.getResponse();
//        response.setStatusCode(httpStatus);
//
//        return response.setComplete();
//    }
//}