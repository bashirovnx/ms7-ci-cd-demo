package com.payriff.bff.komtec.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.payriff.bff.komtec.dto.KomtecResult;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@EqualsAndHashCode(callSuper = false)
@JsonRootName("response")
public class KomtecPayResponseDto extends KomtecResult {

    private BigDecimal amount;

    @JsonProperty("prv_txn")
    private String prvTxn;
}
