package com.payriff.bff.komtec.config;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class FeignClientInterceptor implements RequestInterceptor {

    private final KomtecConfig config;
    private static final String AUTHORIZATION_HEADER = "Authorization";
    private static final String CLIENT_SOURCE_HEADER = "X-Client";

    @Override
    public void apply(RequestTemplate template) {
        template.header(AUTHORIZATION_HEADER, config.getSecurity().getInternal().getAuthorization());
        template.header(CLIENT_SOURCE_HEADER, config.getSecurity().getInternal().getSource());
    }
}
