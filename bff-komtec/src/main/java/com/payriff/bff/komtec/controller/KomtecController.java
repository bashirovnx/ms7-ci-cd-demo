package com.payriff.bff.komtec.controller;

import com.payriff.bff.komtec.service.KomtecService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static javax.ws.rs.core.MediaType.APPLICATION_XML;

@Slf4j
@RestController
@RequestMapping("api/v1/komtec")
@RequiredArgsConstructor
public class KomtecController {

    private final KomtecService komtecService;

    @GetMapping(produces = APPLICATION_XML)
    public ResponseEntity<?> routerByCommand(
            @RequestParam(value = "command") String command,
            @RequestParam(value = "txn_id") String txnId,
            @RequestParam(value = "account", required = false) String account,
            @RequestParam(value = "sum", required = false) String sum,
            @RequestParam(value = "txn_date", required = false) String txn_date
    ) {
        return komtecService.routerByCommand(command, account, txnId, sum, txn_date);
    }

}
