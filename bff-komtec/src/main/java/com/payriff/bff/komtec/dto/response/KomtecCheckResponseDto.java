package com.payriff.bff.komtec.dto.response;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.*;

@Data
@Builder
@EqualsAndHashCode(callSuper = false)
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
@JsonRootName("response")
public class KomtecCheckResponseDto {

    @JsonProperty("osmp_txn_id")
    private String osmpTxnId;

    @JsonProperty("result")
    private String result;

    @JsonProperty("comment")
    private String comment;

    @JsonProperty("addinfo")
    private String addInfo;
}
