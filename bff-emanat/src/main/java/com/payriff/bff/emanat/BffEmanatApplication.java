package com.payriff.bff.emanat;

import com.payriff.bff.emanat.security.SecurityFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
@EnableConfigurationProperties(value = {SecurityFilter.Config.class})
public class BffEmanatApplication {


    public static void main(String[] args) {

        SpringApplication.run(BffEmanatApplication.class, args);

    }

}
