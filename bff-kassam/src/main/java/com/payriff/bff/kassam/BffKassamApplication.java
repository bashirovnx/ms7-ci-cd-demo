package com.payriff.bff.kassam;

import com.payriff.bff.kassam.security.SecurityFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
@EnableConfigurationProperties(value = {SecurityFilter.Config.class})
public class BffKassamApplication {


    public static void main(String[] args) {

        SpringApplication.run(BffKassamApplication.class, args);

    }

}
