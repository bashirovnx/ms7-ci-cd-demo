//package com.payriff.adapter.lifttaxi.service.impl;
//
//import com.payriff.adapter.lifttaxi.client.AileTaxiClientOld;
//import com.payriff.adapter.lifttaxi.exception.CustomClientException;
//import com.payriff.adapter.lifttaxi.model.request.IncreaseBalanceAileRqModel;
//import com.payriff.adapter.lifttaxi.model.response.CheckAileDriverRsModel;
//import com.payriff.adapter.lifttaxi.model.response.DriverRsModel;
//import com.payriff.adapter.lifttaxi.model.response.IncreaseBalanceAileDriverRsModel;
//import com.payriff.adapter.lifttaxi.service.AileTaxiServiceOld;
//import com.payriff.adapter.lifttaxi.util.ErrorCode;
//import lombok.RequiredArgsConstructor;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.stereotype.Service;
//
//@Service
//@RequiredArgsConstructor
//@Slf4j
//public class AileTaxiServiceImplOld implements AileTaxiServiceOld {
//    private final AileTaxiClientOld aileTaxiClientOld;
//
//    @Value("${service.aile-taxi.api-key}")
//    String apiKey;
//
//    @Override
//    public DriverRsModel<CheckAileDriverRsModel> checkDriver(String driverCode) {
//        var response = aileTaxiClientOld.checkDriver(apiKey, driverCode);
//
//        if (response.getStatus().equals("ERROR")) {
//            throw new CustomClientException(ErrorCode.BAD_REQUEST_ERROR, response.getError().getMessage());
//        }
//
//        log.info("Check driver response: {}", response);
//        return response;
//    }
//
//    @Override
//    public DriverRsModel<IncreaseBalanceAileDriverRsModel> increaseBalance(IncreaseBalanceAileRqModel increaseBalanceModel) {
//        increaseBalanceModel.setApi_key(apiKey);
//        var response = aileTaxiClientOld.increaseBalance(increaseBalanceModel);
//
//        if (response.getStatus().equals("ERROR")) {
//            throw new CustomClientException(ErrorCode.BAD_REQUEST_ERROR, response.getError().getMessage());
//        }
//
//        log.info("Increase balance response: {}", response);
//        return response;
//    }
//}
