package com.payriff.adapter.lifttaxi.service;

import com.payriff.adapter.lifttaxi.model.request.CheckDriverRqModel;
import com.payriff.adapter.lifttaxi.model.request.IncreaseBalanceLiftRqModel;
import com.payriff.adapter.lifttaxi.model.response.CheckDriverLiftRsModel;
import com.payriff.adapter.lifttaxi.model.response.IncreaseBalanceLiftRsModel;

public interface AileTaxiService {

    CheckDriverLiftRsModel checkDriver(CheckDriverRqModel requestBody);

    IncreaseBalanceLiftRsModel increaseBalance(IncreaseBalanceLiftRqModel requestBody);
}
