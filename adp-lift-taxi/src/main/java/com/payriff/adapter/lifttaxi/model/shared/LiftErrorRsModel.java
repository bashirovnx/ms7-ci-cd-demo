package com.payriff.adapter.lifttaxi.model.shared;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LiftErrorRsModel {
    private String entityName;
    private String errorKey;
    private String type;
    private String title;
    private String status;
    private String message;
    private String params;
}
