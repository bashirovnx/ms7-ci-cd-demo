package com.payriff.adapter.lifttaxi.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.payriff.adapter.lifttaxi.exception.CustomClientException;
import com.payriff.adapter.lifttaxi.model.shared.LiftErrorRsModel;
import com.payriff.adapter.lifttaxi.util.ErrorCode;
import feign.Response;
import feign.codec.ErrorDecoder;
import java.io.IOException;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

@RequiredArgsConstructor
@Data
@Log4j2
public class FeignErrorDecoder implements ErrorDecoder {

    private final ErrorDecoder decoder;

    @Override
    public Exception decode(String methodKey, Response response) {
        ObjectMapper mapper = new ObjectMapper();


        try {

            var responseBody = mapper.readValue(response.body().asReader(), LiftErrorRsModel.class);
            switch (response.status()) {
                case 400:
                    throw new CustomClientException(ErrorCode.BAD_REQUEST_ERROR, responseBody.getTitle());
                case 401:
                    throw new CustomClientException(ErrorCode.AUTHORIZATION_ERROR, responseBody.getMessage());
                case 404:
                    throw new CustomClientException(ErrorCode.NOT_FOUND_ERROR, "Driver not found with given tabel number");
                case 500:
                    throw new CustomClientException(ErrorCode.INTERNAL_ERROR, "Client returned Internal Server Error");
                default:
                    return decoder.decode(methodKey, response);
            }

        } catch (IOException e) {
            log.error("Unexpected error happened", e);
            throw new CustomClientException(ErrorCode.INTERNAL_ERROR, "Internal Server Error");
        }


    }
}
