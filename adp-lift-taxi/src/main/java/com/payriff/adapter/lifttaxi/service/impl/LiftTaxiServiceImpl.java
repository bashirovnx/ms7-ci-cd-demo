package com.payriff.adapter.lifttaxi.service.impl;

import com.payriff.adapter.lifttaxi.client.LiftTaxiClient;
import com.payriff.adapter.lifttaxi.model.request.CheckDriverRqModel;
import com.payriff.adapter.lifttaxi.model.request.IncreaseBalanceLiftRqModel;
import com.payriff.adapter.lifttaxi.model.response.CheckDriverLiftRsModel;
import com.payriff.adapter.lifttaxi.model.response.IncreaseBalanceLiftRsModel;
import com.payriff.adapter.lifttaxi.service.LiftTaxiService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class LiftTaxiServiceImpl implements LiftTaxiService {
    private final LiftTaxiClient liftTaxiClient;

    @Override
    public CheckDriverLiftRsModel checkDriver(CheckDriverRqModel requestBody) {
        CheckDriverLiftRsModel response = liftTaxiClient.checkDriver(requestBody);
        log.info("Check driver response: {}", response);
        return response;
    }

    @Override
    public IncreaseBalanceLiftRsModel increaseBalance(IncreaseBalanceLiftRqModel requestBody) {
        IncreaseBalanceLiftRsModel response = liftTaxiClient.increaseBalance(requestBody);
        log.info("Increase driver balance response response: {}", response);
        return response;
    }
}
