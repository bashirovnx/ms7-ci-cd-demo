package com.payriff.adapter.lifttaxi.exception;

import com.payriff.adapter.lifttaxi.util.ErrorCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class CustomClientException extends RuntimeException {

    private ErrorCode error;
    private String message;

}
