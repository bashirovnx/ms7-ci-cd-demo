package com.payriff.adapter.lifttaxi.controller;

import com.payriff.adapter.lifttaxi.model.request.CheckDriverRqModel;
import com.payriff.adapter.lifttaxi.model.request.IncreaseBalanceLiftRqModel;
import com.payriff.adapter.lifttaxi.model.response.CheckDriverLiftRsModel;
import com.payriff.adapter.lifttaxi.model.response.IncreaseBalanceLiftRsModel;
import com.payriff.adapter.lifttaxi.service.LiftTaxiService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/lift-taxi")
@RequiredArgsConstructor
@Slf4j
public class LiftTaxiController {

    private final LiftTaxiService liftTaxiService;

    @PostMapping("/check")
    CheckDriverLiftRsModel checkDriver(@RequestBody CheckDriverRqModel requestBody) {
        log.info("Check driver request: {}", requestBody);
        return liftTaxiService.checkDriver(requestBody);
    }

    @PostMapping("/increase-balance")
    IncreaseBalanceLiftRsModel increaseBalance(@RequestBody IncreaseBalanceLiftRqModel requestBody) {
        log.info("Increase balance request: {}", requestBody);
        return liftTaxiService.increaseBalance(requestBody);
    }
}
