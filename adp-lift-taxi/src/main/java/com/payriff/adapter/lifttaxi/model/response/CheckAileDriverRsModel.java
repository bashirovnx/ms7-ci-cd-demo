package com.payriff.adapter.lifttaxi.model.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Builder
@Data
@NoArgsConstructor
public class CheckAileDriverRsModel {
    private String firstname;
    private String middlename;
    private String lastname;
    private String balance;
}
