//package com.payriff.adapter.lifttaxi.service;
//
//import com.payriff.adapter.lifttaxi.model.request.IncreaseBalanceAileRqModel;
//import com.payriff.adapter.lifttaxi.model.response.CheckAileDriverRsModel;
//import com.payriff.adapter.lifttaxi.model.response.DriverRsModel;
//import com.payriff.adapter.lifttaxi.model.response.IncreaseBalanceAileDriverRsModel;
//
//public interface AileTaxiServiceOld {
//
//    DriverRsModel<CheckAileDriverRsModel> checkDriver(String driverCode);
//
//    DriverRsModel<IncreaseBalanceAileDriverRsModel> increaseBalance(IncreaseBalanceAileRqModel increaseBalanceModel);
//}
