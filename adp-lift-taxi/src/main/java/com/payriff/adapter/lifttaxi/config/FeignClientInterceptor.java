package com.payriff.adapter.lifttaxi.config;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class FeignClientInterceptor implements RequestInterceptor {

    @Value("${service.lift-taxi.security.authorization}")
    private String authorization;
    private static final String AUTHORIZATION_HEADER = "X-Access-token";

    @Override
    public void apply(RequestTemplate requestTemplate) {
        requestTemplate.header(AUTHORIZATION_HEADER, authorization);
    }
}
