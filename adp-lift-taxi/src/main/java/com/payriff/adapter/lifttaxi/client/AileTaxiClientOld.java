//package com.payriff.adapter.lifttaxi.client;
//
//import com.payriff.adapter.lifttaxi.config.FeignConfiguration;
//import com.payriff.adapter.lifttaxi.model.request.IncreaseBalanceAileRqModel;
//import com.payriff.adapter.lifttaxi.model.response.CheckAileDriverRsModel;
//import com.payriff.adapter.lifttaxi.model.response.DriverRsModel;
//import com.payriff.adapter.lifttaxi.model.response.IncreaseBalanceAileDriverRsModel;
//import feign.Headers;
//import org.springframework.cloud.openfeign.FeignClient;
//import org.springframework.http.MediaType;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestParam;
//
//@FeignClient(url = "${service.aile-taxi.url}", name = "adp-aile-taxi",
//        configuration = FeignConfiguration.class)
//public interface AileTaxiClientOld {
//
//    @PostMapping(path = "/balance", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
//    @Headers("Content-Type: application/x-www-form-urlencoded")
//    DriverRsModel<IncreaseBalanceAileDriverRsModel> increaseBalance(@RequestBody IncreaseBalanceAileRqModel data);
//
//    @GetMapping(path = "/balance")
//    DriverRsModel<CheckAileDriverRsModel> checkDriver(
//            @RequestParam(name = "api_key") String apiKey,
//            @RequestParam(name = "driver_code") String driverCode);
//
//
//}
