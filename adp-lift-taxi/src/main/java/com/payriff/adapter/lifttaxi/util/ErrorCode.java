package com.payriff.adapter.lifttaxi.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ErrorCode {

    INTERNAL_ERROR(500),
    AUTHORIZATION_ERROR(401),
    BAD_REQUEST_ERROR(400),
    NOT_FOUND_ERROR(404);

   private final int code;
}
