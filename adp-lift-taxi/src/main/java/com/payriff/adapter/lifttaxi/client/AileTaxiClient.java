package com.payriff.adapter.lifttaxi.client;

import com.payriff.adapter.lifttaxi.config.FeignConfiguration;
import com.payriff.adapter.lifttaxi.model.request.CheckDriverRqModel;
import com.payriff.adapter.lifttaxi.model.request.IncreaseBalanceLiftRqModel;
import com.payriff.adapter.lifttaxi.model.response.CheckDriverLiftRsModel;
import com.payriff.adapter.lifttaxi.model.response.IncreaseBalanceLiftRsModel;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(url = "${service.aile-taxi.url}", name = "adp-aile-taxi",
        configuration = FeignConfiguration.class)
public interface AileTaxiClient {

    @PostMapping(path = "/external/balance")
    IncreaseBalanceLiftRsModel increaseBalance(@RequestBody IncreaseBalanceLiftRqModel requestBody);

    @PostMapping(path = "/external/check")
    CheckDriverLiftRsModel checkDriver(@RequestBody CheckDriverRqModel requestBody);
}
