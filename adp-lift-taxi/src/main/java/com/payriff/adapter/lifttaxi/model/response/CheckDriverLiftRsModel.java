package com.payriff.adapter.lifttaxi.model.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Builder
@Data
@NoArgsConstructor
public class CheckDriverLiftRsModel {
    private String fullName;
}
