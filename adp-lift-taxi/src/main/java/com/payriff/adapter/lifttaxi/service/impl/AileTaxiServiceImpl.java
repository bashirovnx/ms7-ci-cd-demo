package com.payriff.adapter.lifttaxi.service.impl;

import com.payriff.adapter.lifttaxi.client.AileTaxiClient;
import com.payriff.adapter.lifttaxi.client.LiftTaxiClient;
import com.payriff.adapter.lifttaxi.model.request.CheckDriverRqModel;
import com.payriff.adapter.lifttaxi.model.request.IncreaseBalanceLiftRqModel;
import com.payriff.adapter.lifttaxi.model.response.CheckDriverLiftRsModel;
import com.payriff.adapter.lifttaxi.model.response.IncreaseBalanceLiftRsModel;
import com.payriff.adapter.lifttaxi.service.AileTaxiService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class AileTaxiServiceImpl implements AileTaxiService {
    private final AileTaxiClient aileTaxiClient;

    @Override
    public CheckDriverLiftRsModel checkDriver(CheckDriverRqModel requestBody) {
        CheckDriverLiftRsModel response = aileTaxiClient.checkDriver(requestBody);
        log.info("Check driver response: {}", response);
        return response;
    }

    @Override
    public IncreaseBalanceLiftRsModel increaseBalance(IncreaseBalanceLiftRqModel requestBody) {
        IncreaseBalanceLiftRsModel response = aileTaxiClient.increaseBalance(requestBody);
        log.info("Increase driver balance response response: {}", response);
        return response;
    }
}
