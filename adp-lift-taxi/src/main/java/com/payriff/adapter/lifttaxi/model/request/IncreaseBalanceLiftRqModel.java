package com.payriff.adapter.lifttaxi.model.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Builder
@Data
@NoArgsConstructor
public class IncreaseBalanceLiftRqModel {
    private String tabel;
    private double amount;
    private String paymentId;
}
