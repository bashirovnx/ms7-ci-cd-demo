package com.payriff.adapter.lifttaxi.model.shared;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.payriff.adapter.lifttaxi.util.ResultStatus;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ErrorRsModel {

    @JsonInclude(NON_NULL)
    private ResultStatus status;

    @JsonInclude(NON_NULL)
    private String code;

    @JsonInclude(NON_NULL)
    private String kind;

    @JsonInclude(NON_NULL)
    private String message;
}
