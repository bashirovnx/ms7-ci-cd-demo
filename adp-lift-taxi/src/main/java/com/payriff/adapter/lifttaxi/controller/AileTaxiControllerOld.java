//package com.payriff.adapter.lifttaxi.controller;
//
//import com.payriff.adapter.lifttaxi.model.request.IncreaseBalanceAileRqModel;
//import com.payriff.adapter.lifttaxi.model.response.CheckAileDriverRsModel;
//import com.payriff.adapter.lifttaxi.model.response.DriverRsModel;
//import com.payriff.adapter.lifttaxi.model.response.IncreaseBalanceAileDriverRsModel;
//import com.payriff.adapter.lifttaxi.service.AileTaxiServiceOld;
//import lombok.RequiredArgsConstructor;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.validation.annotation.Validated;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//
//@RestController
//@RequestMapping("/api/v1/aile-taxi")
//@RequiredArgsConstructor
//@Validated
//@Slf4j
//public class AileTaxiControllerOld {
//
//    private final AileTaxiServiceOld aileTaxiServiceOld;
//
//    @PostMapping(value = "/increase-balance")
//    DriverRsModel<IncreaseBalanceAileDriverRsModel> increaseBalance(@RequestBody IncreaseBalanceAileRqModel requestBody) {
//        log.info("Increase driver balance with data: {}", requestBody);
//        return aileTaxiServiceOld.increaseBalance(requestBody);
//    }
//
//    @GetMapping("/check")
//    DriverRsModel<CheckAileDriverRsModel> checkDriver(@RequestParam(name = "driver_code") String driverCode) {
//        log.info("Check driver request with driver code: {}", driverCode);
//        return aileTaxiServiceOld.checkDriver(driverCode);
//    }
//}
