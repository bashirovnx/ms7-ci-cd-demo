package com.payriff.adapter.lifttaxi.handler;

import com.payriff.adapter.lifttaxi.exception.CustomClientException;
import com.payriff.adapter.lifttaxi.model.shared.ErrorRsModel;
import com.payriff.adapter.lifttaxi.util.ResultStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
@Slf4j
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(CustomClientException.class)
    public ResponseEntity<ErrorRsModel> handleServiceException(CustomClientException exception) {
        log.error("adp-lift-taxi custom client exception: ", exception);
        return ResponseEntity
                .status(exception.getError().getCode())
                .body(ErrorRsModel.builder()
                        .status(ResultStatus.ERROR)
                        .code(exception.getError().name())
                        .message(exception.getMessage())
                        .build());
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(
            HttpMessageNotReadableException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(ErrorRsModel.builder()
                        .status(ResultStatus.ERROR)
                        .code(HttpStatus.BAD_REQUEST.name())
                        .message(ex.getMessage())
                        .build());
    }

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body,
                                                             HttpHeaders headers,
                                                             HttpStatus status, WebRequest request) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(ErrorRsModel.builder()
                        .status(ResultStatus.ERROR)
                        .code(HttpStatus.BAD_REQUEST.name())
                        .message(ex.getMessage())
                        .build());
    }
}
