package com.payriff.adapter.lifttaxi.config;

import com.payriff.adapter.lifttaxi.handler.FeignErrorDecoder;
import feign.Client;
import feign.Logger;
import feign.codec.ErrorDecoder;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.ssl.TrustStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;

@Slf4j
public class FeignConfiguration {


    private ErrorDecoder decoder;

    public void setDecoder(ErrorDecoder decoder) {
        this.decoder = decoder;
    }

    @Bean
    Logger.Level feignLoggerLevel() {
        return Logger.Level.FULL;
    }

    @Bean
    public ErrorDecoder feignErrorDecoder() {
        return new FeignErrorDecoder(decoder);
    }

    @Bean
    public Client feignClient() {
        return new Client.Default(getSSLSocketFactory(), new NoopHostnameVerifier());
    }

    private SSLSocketFactory getSSLSocketFactory() {
        try {
            TrustStrategy acceptingTrustStrategy = (chain, authType) -> true;

            SSLContext sslContext = SSLContexts.custom()
                    .loadTrustMaterial(null, acceptingTrustStrategy)
                    .build();
            return sslContext.getSocketFactory();
        } catch (Exception exception) {
            log.error("Exception thrown while getting SSL Socket Factory", exception);
        }
        return null;
    }
}
