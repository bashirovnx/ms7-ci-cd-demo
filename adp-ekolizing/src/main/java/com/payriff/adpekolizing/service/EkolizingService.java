package com.payriff.adpekolizing.service;

import com.payriff.adpekolizing.client.model.dto.invoice.InvoiceDetailRequestDto;
import com.payriff.adpekolizing.client.model.dto.invoice.InvoiceResponseDto;
import com.payriff.adpekolizing.client.model.dto.invoice.TerminalPayRequestDto;
import com.payriff.adpekolizing.client.model.dto.invoice.TerminalPayResponseDto;

public interface EkolizingService {
    InvoiceResponseDto checkPayment(InvoiceDetailRequestDto detailRequestDto);

    TerminalPayResponseDto makePayment(TerminalPayRequestDto requestDto);
}
