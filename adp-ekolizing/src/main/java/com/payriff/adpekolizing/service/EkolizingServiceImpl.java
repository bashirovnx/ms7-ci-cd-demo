package com.payriff.adpekolizing.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.payriff.adpekolizing.client.EkolizingClient;
import com.payriff.adpekolizing.client.model.dto.CheckResponseDto;
import com.payriff.adpekolizing.client.model.dto.PayResponseDto;
import com.payriff.adpekolizing.client.model.dto.invoice.*;
import com.payriff.adpekolizing.error.ServiceException;
import com.payriff.adpekolizing.util.EntityUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Objects;

@Slf4j
@Service
@RequiredArgsConstructor
public class EkolizingServiceImpl implements EkolizingService {
    private final EkolizingClient client;
    private final ObjectMapper objectMapper;

    @Value("${service.ekolizing.security.secretKey}")
    private String secretKey;

    @Override
    public InvoiceResponseDto checkPayment(InvoiceDetailRequestDto detailRequestDto) {

        log.info("Check request: " + detailRequestDto);

        String hashMD5 = getHashMD5(detailRequestDto.getValue(), detailRequestDto.getRrn(), null);
        String check = client.check("check", detailRequestDto.getValue(), LocalDateTime.now().toString(),
                        detailRequestDto.getRrn(), hashMD5).stripLeading()
                .stripTrailing().trim().replaceAll("\uFEFF", "");

        log.info("Check response: {}", check);

        CheckResponseDto checkDto = null;
        try {
            checkDto = objectMapper.readValue(check, CheckResponseDto.class);
        } catch (Exception e) {
            log.error("ERROR on ObjectMapper", e);
            throw new ServiceException("INTERNAL_ERROR", e.getMessage());

        }

        InvoiceResponseDto responseDto = InvoiceResponseDto.builder()
                .fullName(checkDto.getAccountInfo().getParam1())
                .description(checkDto.getAccountInfo().getParam2())
                .backRrn(detailRequestDto.getRrn())
                .senderCompany("Ekolizinq")
                .currencyType(CurrencyType.AZN)
                .email("info@payriff.com")
                .expiryDate(LocalDateTime.now().plusDays(1L))
                .maxAmount(new BigDecimal("100000"))
                .phoneNumber("+99451xxxxxxx")
                .build();

        String paramAmount = checkDto.getAccountInfo().getParam3();

        log.info("Amount: " + paramAmount);

        if (!Objects.isNull(paramAmount)) {
            responseDto.setAmount(
                    new BigDecimal(paramAmount.replace(",", "."))
            );
        }

        return responseDto;
    }

    @Override
    public TerminalPayResponseDto makePayment(TerminalPayRequestDto requestDto) {
        log.info("Payment request is: {}", requestDto);


        String hash = getHashMD5(requestDto.getValue(), requestDto.getBackRrn(), requestDto.getAmount().toString());
        log.info("Hash: {}", hash);
        String pay = client.pay("pay", requestDto.getValue(),
                        LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS).toString(),
                        requestDto.getAmount().toString(),
                        requestDto.getBackRrn(),
                        hash).stripLeading()
                .stripTrailing().trim().replaceAll("\uFEFF", "");

        log.info("Pay response: {}", pay);

        PayResponseDto payDto = null;
        try {
            payDto = objectMapper.readValue(pay, PayResponseDto.class);
        } catch (Exception e) {
            log.error("ERROR on ObjectMapper", e);
            throw new ServiceException("INTERNAL_ERROR", e.getMessage());
        }

        if (!StringUtils.equals(payDto.getStatusCode(), "0")) {
            log.error("Error during PAY API!");
            throw new ServiceException(payDto.getStatusCode(), payDto.getStatusDetail());
        }
        return TerminalPayResponseDto.builder()
                .amount(requestDto.getAmount())
                .invoiceCode(requestDto.getValue())
                .build();
    }

    private String getHashMD5(String customerId, String transactionId, String amount) {
        amount = Objects.nonNull(amount) ? amount : "";
        return EntityUtil.hashMD5(customerId + transactionId + amount + secretKey);
    }
}
