package com.payriff.adpekolizing.client.model.dto;

import lombok.*;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@Builder
@ToString
@AllArgsConstructor
public class CheckRequestDto {
    @NotNull
    private String customerId;

    @NotNull
    private String dateTime;

    @NotNull
    private String transactionId;
}
