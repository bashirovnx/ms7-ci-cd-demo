package com.payriff.adpekolizing.client.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class CheckResponseDto {

    @JsonProperty("StatusCode")
    private Integer statusCode;

    @JsonProperty("StatusDetail")
    private String statusDetail;

    @JsonProperty("DateTime")
    private String dateTime;

    @JsonProperty("AccountInfo")
    private AccountInfo accountInfo;

    @Getter
    @Setter
    @ToString
    public static class AccountInfo {
        @JsonProperty("Parameter1")
        private String param1;

        @JsonProperty("Parameter2")
        private String param2;

        @JsonProperty("Parameter3")
        private String param3;
    }
}
