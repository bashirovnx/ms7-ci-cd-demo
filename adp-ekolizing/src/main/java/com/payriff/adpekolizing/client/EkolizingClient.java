package com.payriff.adpekolizing.client;

import com.payriff.adpekolizing.config.FeignConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(url = "${service.ekolizing.url}", name = "adp-ekolizing", configuration = FeignConfiguration.class)
public interface EkolizingClient {

    @GetMapping
    String check(
            @RequestParam(value = "action", defaultValue = "check") String check,
            @RequestParam(value = "customer_id") String customerId,
            @RequestParam(value = "datetime") String dateTime,
            @RequestParam(value = "transaction_id") String transactionId,
            @RequestParam("key") String key
    );

    @GetMapping
    String pay(
            @RequestParam(value = "action", defaultValue = "pay") String check,
            @RequestParam(value = "customer_id") String customerId,
            @RequestParam(value = "datetime") String dateTime,
            @RequestParam(value = "amount") String amount,
            @RequestParam(value = "transaction_id") String transactionId,
            @RequestParam("key") String key
    );
}