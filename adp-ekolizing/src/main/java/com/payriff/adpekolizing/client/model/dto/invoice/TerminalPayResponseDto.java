package com.payriff.adpekolizing.client.model.dto.invoice;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@EqualsAndHashCode(callSuper = false)
public class TerminalPayResponseDto extends TerminalResult {

    private String invoiceCode;
    private BigDecimal amount;
    private String transStatus;
    private Date transactionDate;
    private String note;
    private boolean partial;

}
