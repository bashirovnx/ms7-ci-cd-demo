package com.payriff.adpekolizing.client.model.dto;

import lombok.*;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class PayRequestDto {
    @NotNull
    private String customerId;

    @NotNull
    private String dateTime;

    @NotNull
    private String amount;

    @NotNull
    private String transactionId;
}
