package com.payriff.adpekolizing.client.model.dto.invoice;

public enum TerminalInvoiceReqType {

    INVOICE_CODE,
    PIN_CODE,
    CUSTOMER_CODE

}
