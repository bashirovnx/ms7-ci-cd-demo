package com.payriff.adpekolizing.client.error;

public enum ErrorCode {
    INTERNAL_ERROR,
    AUTHORIZATION_ERROR
}
