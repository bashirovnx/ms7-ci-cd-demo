package com.payriff.adpekolizing.client.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@Builder
@ToString
@AllArgsConstructor
public class PayResponseDto {
    @JsonProperty("StatusCode")
    private String statusCode;

    @JsonProperty("StatusDetail")
    private String statusDetail;

    @JsonProperty("PaymentID")
    private String paymentId;

    @JsonProperty("OrderDate")
    private String orderDate;
}