package com.payriff.adpekolizing.client.model.dto.invoice;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class InvoiceDetailRequestDto {

    private String rrn;
    private String src;
    private TerminalInvoiceReqType type;
    private String value;

}
