package com.payriff.adpekolizing.error;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ServiceException extends RuntimeException {
    private String errorCode;
    private String message;

    public static ServiceException of(String code, String message) {
        throw new ServiceException(code, message);
    }
}
