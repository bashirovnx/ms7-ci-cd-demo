package com.payriff.adpekolizing.error;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ErrorResponseDto {

    private ResultStatus status;
    private String code;
    private String message;

}
