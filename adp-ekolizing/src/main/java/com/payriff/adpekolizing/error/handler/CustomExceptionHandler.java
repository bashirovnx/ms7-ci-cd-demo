package com.payriff.adpekolizing.error.handler;

import com.payriff.adpekolizing.error.ErrorResponseDto;
import com.payriff.adpekolizing.error.ResultStatus;
import com.payriff.adpekolizing.error.ServiceException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Slf4j
@RestControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({ServiceException.class})
    public ResponseEntity<ErrorResponseDto> handleServiceException(ServiceException exception) {

        log.error("adp-ekolizing custom client exception: ", exception);

        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST.value())
                .body(ErrorResponseDto.builder()
                        .status(ResultStatus.ERROR)
                        .code(exception.getErrorCode())
                        .message(exception.getMessage())
                        .build());
    }
}
