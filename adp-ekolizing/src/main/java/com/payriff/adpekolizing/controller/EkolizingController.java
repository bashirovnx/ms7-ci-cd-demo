package com.payriff.adpekolizing.controller;

import com.payriff.adpekolizing.client.model.dto.PayRequestDto;
import com.payriff.adpekolizing.client.model.dto.PayResponseDto;
import com.payriff.adpekolizing.client.model.dto.invoice.InvoiceDetailRequestDto;
import com.payriff.adpekolizing.client.model.dto.invoice.InvoiceResponseDto;
import com.payriff.adpekolizing.client.model.dto.invoice.TerminalPayRequestDto;
import com.payriff.adpekolizing.client.model.dto.invoice.TerminalPayResponseDto;
import com.payriff.adpekolizing.service.EkolizingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping("/api/v1/ekolizing")
@RequiredArgsConstructor
public class EkolizingController {
    private final EkolizingService ekolizingService;

    @PostMapping("/check")
    public ResponseEntity<InvoiceResponseDto> check(@RequestBody @Valid InvoiceDetailRequestDto requestDto) {
        InvoiceResponseDto responseDto = ekolizingService.checkPayment(requestDto);
        return ResponseEntity.ok(responseDto);
    }

    @PostMapping(value = "/pay")
    public ResponseEntity<TerminalPayResponseDto> pay(@RequestBody @Valid TerminalPayRequestDto requestDto) {
        TerminalPayResponseDto payResponseDto = ekolizingService
                .makePayment(requestDto);
        return ResponseEntity.ok(payResponseDto);
    }
}
