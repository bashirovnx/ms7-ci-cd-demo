package com.payriff.adpekolizing.util;

import org.apache.commons.codec.digest.MessageDigestAlgorithms;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;

public class EntityUtil {
    public static String hashMD5(String data) {
        String md5Hash;
        try {
            MessageDigest md = MessageDigest.getInstance(MessageDigestAlgorithms.MD5);
            md.reset();
            md.update(data.getBytes());
            md5Hash = byteToHex(md.digest());
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("StandardResult.ResultMessages.ERROR", e);
        }
        return md5Hash;
    }

    private static String byteToHex(final byte[] hash) {
        Formatter formatter = new Formatter();
        for (byte b : hash) {
            formatter.format("%02x", b);
        }
        String result = formatter.toString();
        formatter.close();
        return result;
    }
}
