package com.payriff.notification.service.sms.service.interfaces;

public interface LocalSmsService {
    boolean sendSmsWithMsm(String phone, String text, boolean mandatory);
    void sendSmsWithLsim(String phone, String text, boolean mandatory);
}
