package com.payriff.notification.service.sms.service.interfaces;

public interface SmsService {
    boolean sendSms(String phone, String text, boolean mandatory);
}
