package com.payriff.notification.service.sms.consumer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.payriff.commonlib.mq.dto.SendSmsRequest;
import com.payriff.notification.service.sms.SmsService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.KafkaListener;

@Configuration
@RequiredArgsConstructor
public class SmsConsumer {
    private final SmsService smsService;
    private final ObjectMapper objectMapper = new ObjectMapper();


    @KafkaListener(topics = "sms")
    public void listenGroupOtp(String message) {
        try {
            SendSmsRequest sendSmsRequest = objectMapper.readValue(message, SendSmsRequest.class);
            smsService.sendSmsWithFallback(sendSmsRequest.getPhone(), sendSmsRequest.getMessage(), sendSmsRequest.isMandatory());
            System.out.println("Success");
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

}
