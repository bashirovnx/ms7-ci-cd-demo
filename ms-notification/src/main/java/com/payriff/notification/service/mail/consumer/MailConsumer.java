package com.payriff.notification.service.mail.consumer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.payriff.commonlib.mq.dto.SendMailRequest;
import com.payriff.notification.service.mail.MailService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.KafkaListener;

@Configuration
@RequiredArgsConstructor
@Slf4j
public class MailConsumer {

    private final MailService mailService;
    private final ObjectMapper objectMapper = new ObjectMapper();


    @KafkaListener(topics = "mail")
    public void listenGroupMail(String message) {

        log.info("**************** Event consumer *************************");

        try {
            SendMailRequest sendSmsRequest = objectMapper.readValue(message, SendMailRequest.class);

            mailService.sendMail(
                    sendSmsRequest.getFrom(),
                    sendSmsRequest.getTo(),
                    sendSmsRequest.getContent(),
                    sendSmsRequest.getSubject(),
                    sendSmsRequest.getCc(),
                    sendSmsRequest.getBcc()
            );
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

}
