package com.payriff.notification.service.sms;


import com.payriff.commonlib.Constants;
import com.payriff.commonlib.dto.LsimResponseDto;
import com.payriff.commonlib.util.CommonUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @author HasanovZK
 */
@Service
@Log4j2
@RequiredArgsConstructor
public class SmsService {

    private final RestTemplate restTemplate;

    public void sendSms(String phone, String text) {
        sendSms(phone, text, true);
    }

    public void sendSmsWithFallback(String phone, String text, boolean mandatory) {
        if (!sendSms(phone, text, mandatory)) {
            sendSmsLSIM(phone, text, mandatory);
        }
    }


    public boolean sendSms(String phone, String text, boolean mandatory) {
        phone = phone.replaceFirst("\\+", "");
        log.info(String.format("TEXT: %s is sent to phone number %s", text, phone));
        String url = Constants.Endpoints.SMS.replace("{NUMBER}", phone).replace("{TEXT}", text);
        log.info(String.format("URL: %s for sending sms", url));
        ResponseEntity<String> response;
        if (mandatory) {
            try {
                response = restTemplate.getForEntity(url, String.class);
            } catch (RestClientException e) {
                return false;
            }
            log.info(String.format("RESPONSE: %s message for mobile phone %s", response, phone));
            Map<String, String> stringMap = splitQuery(response.getBody());
            if (!stringMap.get("errno").equals("100"))
                return false;
            return true;
        } else {
            try {
                response = restTemplate.getForEntity(url, String.class);
                log.info(String.format("RESPONSE: %s message for mobile phone %s", response, phone));
                return true;
            } catch (Exception e) {
                log.warn(e);
                return false;
            }
        }
    }

    public void sendSmsLSIM(String phone, String text, boolean mandatory) {
        phone = phone.replaceFirst("\\+", "");
        String url =
                Constants.Endpoints.SMS_LSIM
                        .replace("{NUMBER}", phone)
                        .replace("{TEXT}", text)
                        .replace(
                                "{KEY}",
                                CommonUtil.hashMD5(
                                        CommonUtil.hashMD5(Constants.StaticValues.LSIM_PASS)
                                                + Constants.StaticValues.LSIM_LOGIN
                                                + text
                                                + phone
                                                + Constants.StaticValues.LSIM_SENDER));
        ResponseEntity<LsimResponseDto> response = null;
        if (mandatory) {
            response = restTemplate.getForEntity(url, LsimResponseDto.class);
            if (Objects.requireNonNull(response.getBody()).getErrorMessage() != null
                    && !response.getBody().getErrorMessage().isEmpty())
                throw new RuntimeException(
                        "Sms service error:" + phone + " " + response.getBody().getErrorMessage());
        } else {
            try {
                restTemplate.getForEntity(url, String.class);
            } catch (Exception e) {
                log.warn(e);
            }
        }
    }

    private static Map<String, String> splitQuery(String url) {
        if (Objects.isNull(url)) throw new RuntimeException("Sms service error");

        Map<String, String> queryPairs = new LinkedHashMap<>();
        String[] pairs = url.split("&");
        for (String pair : pairs) {
            int idx = pair.indexOf('=');
            queryPairs.put(pair.substring(0, idx), pair.substring(idx + 1));
        }
        return queryPairs;
    }
}
