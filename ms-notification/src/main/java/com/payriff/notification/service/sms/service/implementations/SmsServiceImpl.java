package com.payriff.notification.service.sms.service.implementations;

import com.payriff.notification.service.sms.service.interfaces.LocalSmsService;
import com.payriff.notification.service.sms.service.interfaces.SmsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service
public class SmsServiceImpl implements SmsService {

    private final LocalSmsService localSmsService;

    @Override
    public boolean sendSms(String phone, String text, boolean mandatory) {
        return false;
    }
}
