package com.payriff.notification.service.mail;

import com.payriff.notification.dto.request.RequestMailDto;
import com.payriff.notification.domain.NotifyEntity;
import com.payriff.notification.repository.NotifyRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;
import java.io.File;
import java.time.LocalDateTime;
import java.util.Arrays;

@Service
@Slf4j
@RequiredArgsConstructor
public class MailService {

    private final JavaMailSender javaMailSender;
    private final NotifyRepository notifyRepository;

    public void sendMail(RequestMailDto mailDto) {
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper;
        if (mailDto.getCc() == null)
            mailDto.setCc(new String[0]);

        if (mailDto.getBb() == null)
            mailDto.setBb(new String[0]);

        try {
            helper = new MimeMessageHelper(message, true);
            helper.setFrom(mailDto.getFrom(), "PayRiff Payment System");
            helper.setTo(mailDto.getTo());
            helper.setText(mailDto.getText(), true);
            helper.setSubject(mailDto.getSubject());
            helper.setCc(mailDto.getCc());
            helper.setBcc(mailDto.getBb());

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        try {
            javaMailSender.send(message);
            log.info("Email {} was sent.", mailDto);

            saveNotificationEntity(mailDto.getFrom(), mailDto.getTo(), mailDto.getText(), mailDto.getSubject(), mailDto.getBb(), mailDto.getCc(), mailDto.getUserId());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public void sendMail(String from, String to, String text, String subject, String[] cc, String[] bb) {
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper;
        if (cc == null) cc = new String[0];
        if (bb == null) bb = new String[0];

        try {
            helper = new MimeMessageHelper(message, true);
            helper.setFrom(from, "PayRiff Payment System");
            helper.setTo(to);
            helper.setText(text, true);
            helper.setSubject(subject);
            helper.setCc(cc);
            helper.setBcc(bb);

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        try {
            log.info("Sending mail------------>>>>>>>>");
            javaMailSender.send(message);
            log.info("Sending mail Done------------<<<<<<<<<");

            saveNotificationEntity(from, to, text, subject, bb, cc, null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public void sendMail(String from, String to, String text, String subject, String[] cc) {
        sendMail(from, to, text, subject, cc, null);
    }

    public void sendMail(String from, String[] to, String text, String subject, String filename, File attachment, String[] cc) {
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper;
        try {
            helper = new MimeMessageHelper(message, true);
            helper.setFrom(from, "PayRiff Payment System");
            helper.setTo(to);
            helper.setText(text, true);
            helper.setSubject(subject);
            helper.addAttachment(filename, attachment);
            helper.setCc(cc);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        try {
            log.info("Sending mail with attachment");
            javaMailSender.send(message);
            log.info("Sending mail done with attachment");

            saveNotificationEntity(from, Arrays.toString(to), text, subject, null, cc, null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    private void saveNotificationEntity(String from, String to, String text, String subject,
                                        String[] bb, String[] cc, Long userId) {

        NotifyEntity notifyEntity = NotifyEntity.builder()
                .sender(from)
                .receiver(to)
                .text(text)
                .subject(subject)
                .cc(Arrays.toString(cc))
                .bcc(Arrays.toString(bb))
                .userId(userId)
                .createdDate(LocalDateTime.now())
                .build();

        try {
            notifyRepository.save(notifyEntity);
            log.info("Notification {} has been saved.", notifyEntity);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

    }
}
