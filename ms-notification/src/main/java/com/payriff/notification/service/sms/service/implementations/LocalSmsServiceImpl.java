package com.payriff.notification.service.sms.service.implementations;

import com.payriff.commonlib.Constants;
import com.payriff.commonlib.dto.LsimResponseDto;
import com.payriff.commonlib.util.CommonUtil;
import com.payriff.notification.service.sms.service.interfaces.LocalSmsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

@Slf4j
@Service
@RequiredArgsConstructor
public class LocalSmsServiceImpl implements LocalSmsService {
    private final RestTemplate restTemplate;

    @Override
    public boolean sendSmsWithMsm(String phone, String text, boolean mandatory) {
        phone = phone.replaceFirst("\\+", "");
        log.info("Text: {} is sent to phone number {}.", text, phone);

        String url = Constants.Endpoints.SMS.replace("{NUMBER}", phone).replace("{TEXT}", text);
        log.info("Url: {} for sending sms", url);

        ResponseEntity<String> response;
        if (mandatory) {
            try {
                response = restTemplate.getForEntity(url, String.class);
            } catch (RestClientException e) {
                return false;
            }
            log.info("Response: {} message for mobile phone {}", response, phone);

            Map<String, String> stringMap = splitQuery(response.getBody());
            return stringMap.get("errno").equals("100");

        } else {
            try {
                response = restTemplate.getForEntity(url, String.class);
                log.info("ResponseL {} message for mobile phone {}", response, phone);

                return true;
            } catch (Exception e) {
                log.warn(e.toString());
                return false;
            }
        }
    }

    @Override
    public void sendSmsWithLsim(String phone, String text, boolean mandatory) {
        phone = phone.replaceFirst("\\+", "");
        String url =
                Constants.Endpoints.SMS_LSIM
                        .replace("{NUMBER}", phone)
                        .replace("{TEXT}", text)
                        .replace(
                                "{KEY}",
                                CommonUtil.hashMD5(
                                        CommonUtil.hashMD5(Constants.StaticValues.LSIM_PASS)
                                                + Constants.StaticValues.LSIM_LOGIN
                                                + text
                                                + phone
                                                + Constants.StaticValues.LSIM_SENDER));

        ResponseEntity<LsimResponseDto> response;
        if (mandatory) {
            response = restTemplate.getForEntity(url, LsimResponseDto.class);

            if (Objects.requireNonNull(response.getBody()).getErrorMessage() != null
                    && !response.getBody().getErrorMessage().isEmpty())
                throw new RuntimeException(
                        "Sms service error:" + phone + " " + response.getBody().getErrorMessage());
        } else {
            try {
                restTemplate.getForEntity(url, String.class);
            } catch (Exception e) {
                log.warn(e.toString());
            }
        }
    }

    private static Map<String, String> splitQuery(String url) {
        if (Objects.isNull(url)) throw new RuntimeException("Sms service error");

        Map<String, String> queryPairs = new LinkedHashMap<>();
        String[] pairs = url.split("&");
        for (String pair : pairs) {
            int idx = pair.indexOf('=');
            queryPairs.put(pair.substring(0, idx), pair.substring(idx + 1));
        }
        return queryPairs;
    }
}
