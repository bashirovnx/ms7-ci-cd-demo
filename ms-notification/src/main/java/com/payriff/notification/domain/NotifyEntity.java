package com.payriff.notification.domain;

import lombok.*;
import org.hibernate.Hibernate;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "notify_log")
@ToString(onlyExplicitlyIncluded = true)
public class NotifyEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "notify_generator")
  @SequenceGenerator(name = "notify_generator", sequenceName = "notify_seq")
  private Long id;

  private String sender;

  private String receiver;

  private String text;

  private String subject;

  private String cc;

  private String bcc;

  private Long userId;

  @CreatedDate
  @Column(name = "created_date")
  private LocalDateTime createdDate = LocalDateTime.now();

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
    NotifyEntity notify = (NotifyEntity) o;
    return id != null && Objects.equals(id, notify.id);
  }

  @Override
  public int hashCode() {
    return getClass().hashCode();
  }
}
