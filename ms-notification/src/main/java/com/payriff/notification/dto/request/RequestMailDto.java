package com.payriff.notification.dto.request;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
public class RequestMailDto {

    private String from;

    private String to;

    @ToString.Exclude
    private String text;

    private String subject;

    private String[] cc;

    private String[] bb;

    private Long userId;

}
