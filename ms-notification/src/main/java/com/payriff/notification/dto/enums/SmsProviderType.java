package com.payriff.notification.dto.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public enum SmsProviderType {

    LSIM(0),
    MSM(1),
    ITS(2);

    private int provider;

}
