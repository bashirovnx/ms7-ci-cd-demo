package com.payriff.notification.client.payriff.model.enums;

public enum InvoiceStatus {

    PENDING,
    ERROR,
    EXPIRED,
    PARTIAL,
    COMPLETE

}