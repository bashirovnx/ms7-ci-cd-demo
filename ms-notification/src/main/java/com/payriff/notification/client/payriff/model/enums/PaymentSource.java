package com.payriff.notification.client.payriff.model.enums;

public enum PaymentSource {

    NORMAL,
    INVOICE,
    TRANSFER,
    INSTALLMENT,
    QR

}
