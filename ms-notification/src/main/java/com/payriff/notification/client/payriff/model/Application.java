package com.payriff.notification.client.payriff.model;

import com.payriff.notification.client.payriff.model.enums.ApplicationStatus;
import lombok.Data;

@Data
public class Application {

    private Long id;
    private String name;
    private ApplicationStatus status;
    private Long userId;

}
