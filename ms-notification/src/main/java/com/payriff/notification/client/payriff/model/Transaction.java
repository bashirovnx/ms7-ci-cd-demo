package com.payriff.notification.client.payriff.model;

//import com.payriff.terminal.emanat.client.payriff.model.enums.PaymentSource;
//import com.payriff.terminal.emanat.client.payriff.model.enums.PaymentStatus;
//import com.payriff.terminal.emanat.dto.CurrencyType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Transaction {

    private Long id;
    private Date createdDate;
    private String cardBrand;
//    private PaymentStatus paymentStatus;
    private BigDecimal amount;
    private BigDecimal paidAmount;
    private Long applicationId;
//    private CurrencyType currencyType;
    private String description;
//    private PaymentSource paymentSource;

}