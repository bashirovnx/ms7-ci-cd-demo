package com.payriff.notification.client.payriff.model.enums;

public enum InvoiceSource {

    B2B,
    DASHBOARD,
    PMD,
    TERMINAL

}
