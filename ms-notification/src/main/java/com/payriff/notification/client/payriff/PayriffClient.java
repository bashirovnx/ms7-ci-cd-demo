package com.payriff.notification.client.payriff;

import com.payriff.notification.client.payriff.model.Application;
import com.payriff.notification.client.payriff.model.CustomerInvoice;
import com.payriff.notification.client.payriff.model.Transaction;
import com.payriff.notification.config.PayriffFeignConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(
        url = "${service.payriff.url}",
        name = "ms-notification",
        configuration = PayriffFeignConfig.class
)
public interface PayriffClient {

    @GetMapping(path = "/v1/terminal/transaction/{id}")
    Transaction findTransactionById(@RequestHeader("Authorization") String authorization,
                                    @PathVariable("id") Long id);

    @PostMapping(path = "/v1/terminal/transaction/{orderId}")
    Transaction findTansactionByOrderId(@RequestHeader("Authorization") String authorization,
                                        @PathVariable("orderId") Long orderId);

    @GetMapping(path = "/v1/terminal/application/{id}")
    Application findApplicationById(@RequestHeader("Authorization") String authorization,
                                    @PathVariable("id") Long id);

    @GetMapping(path = "/v1/terminal/application/{id}/merchant")
    Application findApplicationByMerchantId(@RequestHeader("Authorization") String authorization,
                                            @PathVariable("id") String id);

    @GetMapping(path = "/v1/terminal/invoice/{id}/id")
    CustomerInvoice findInvoiceById(@RequestHeader("Authorization") String authorization,
                                    @PathVariable("id") Long id);

}
