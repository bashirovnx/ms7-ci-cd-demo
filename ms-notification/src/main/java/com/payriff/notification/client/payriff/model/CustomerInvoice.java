package com.payriff.notification.client.payriff.model;

//import com.payriff.terminal.emanat.client.payriff.model.enums.InvoiceSource;
//import com.payriff.terminal.emanat.client.payriff.model.enums.InvoiceStatus;
//import com.payriff.terminal.emanat.dto.CurrencyType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CustomerInvoice {

    private Long id;
    private String invoiceCode;
    private String senderCompany;
    private String fullName;
    private BigDecimal amount;
//    private CurrencyType currencyType;
    private String description;
    private String email;
    private String phoneNumber;
    private BigDecimal maxAmount;
    private String checkinDate;
    private String expiryDate;
//    private InvoiceStatus invoiceStatus;
//    private InvoiceSource source;
    private Long applicationEntity;
    private Long userEntity;
    private String uuid;
    private String invoiceUuid;
    private LocalDate paymentDay;
    private LocalDate expireDay;

}