package com.payriff.notification.config.interceptor;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;

@Slf4j
public class PayriffRequestInterceptor implements RequestInterceptor {

    private static final String AUTHORIZATION_HEADER = "Authorization";
    @Value("${service.payriff.security.authorization}")
    private String authorization;

    @Override
    public void apply(RequestTemplate template) {
        template.header(AUTHORIZATION_HEADER, authorization);
        log.info("PayriffRequestInterceptor applied");
    }
}