package com.payriff.notification.config;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.web.client.RestTemplate;

@Configuration
@Slf4j
@RequiredArgsConstructor
public class BeansConfiguration {

    private final Environment environment;

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

}
