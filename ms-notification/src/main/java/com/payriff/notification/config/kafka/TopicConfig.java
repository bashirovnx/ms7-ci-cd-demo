package com.payriff.notification.config.kafka;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

@Data
@ConfigurationProperties("spring.kafka")
public class TopicConfig {

    private List<Topic> topics;

    @Data
    public static class Topic {

        private String name;
        private short partitions;
        private short replication;

    }


}