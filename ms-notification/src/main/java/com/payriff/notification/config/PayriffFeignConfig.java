package com.payriff.notification.config;

import com.payriff.notification.client.payriff.error.PayriffErrorDecoder;
import feign.Logger;
import feign.codec.Decoder;
import feign.codec.ErrorDecoder;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class PayriffFeignConfig {

    private static final String AUTHORIZATION_HEADER = "Authorization";
    @Value("${service.payriff.security.authorization}")
    private String authorization;

    @Bean(name = "payriffFeignLoggerLevel")
    Logger.Level feignLoggerLevel() {
        return Logger.Level.FULL;
    }

    @Bean
    public ErrorDecoder feignErrorDecoder() {
        return new PayriffErrorDecoder(new Decoder.Default());
    }

    /*@Bean
    public RequestInterceptor kainatRequestInterceptor() {
        return (RequestTemplate template) -> template.header(AUTHORIZATION_HEADER, authorization);
    }*/

}