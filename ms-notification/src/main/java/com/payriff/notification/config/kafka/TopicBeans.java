package com.payriff.notification.config.kafka;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Configuration
public class TopicBeans implements BeanFactoryAware {


    private final BeanFactory beanFactory;
    private final TopicConfig topicConfig;

    public TopicBeans(TopicConfig topicConfig, BeanFactory beanFactory) {
        this.topicConfig = topicConfig;
        this.beanFactory = beanFactory;
    }

    @PostConstruct
    public void onPostConstruct() {
        setBeanFactory(beanFactory);
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        ConfigurableBeanFactory configurableBeanFactory = (ConfigurableBeanFactory) beanFactory;

        configurableBeanFactory.registerSingleton("bean_12", new Object());
//        int i = 0;
//        for (TopicConfig.Topic topic : topicConfig.getTopics()) {
//
//            NewTopic bean = topicBuilder(topic);
//
//            configurableBeanFactory.registerSingleton("bean_" + i, bean);
//            i++;
//        }
    }

    private NewTopic topicBuilder(TopicConfig.Topic topic) {
        return new NewTopic(
                topic.getName(),
                topic.getPartitions(),
                topic.getReplication()
        );
    }
}