package com.payriff.notification.controller;

import com.payriff.notification.dto.request.RequestMailDto;
import com.payriff.notification.service.mail.MailService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("api/v1/notification/mail")
public class MailController {
    private final MailService mailService;

    @PostMapping
    public ResponseEntity<?> sendEmail(@RequestBody RequestMailDto request) {
        mailService.sendMail(request);
        return ResponseEntity.ok().build();
    }
}
