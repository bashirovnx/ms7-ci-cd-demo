package com.payriff.notification.controller;

import com.payriff.notification.dto.request.RequestMailDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("api/v1/notification/sms")
public class SmsController {

    @PostMapping
    public ResponseEntity<?> sendSms(@RequestBody RequestMailDto request) {
        return ResponseEntity.ok().build();
    }
}
