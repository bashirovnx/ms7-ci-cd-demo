package com.payriff.notification.controller;

import com.alibaba.fastjson.JSON;
import com.payriff.commonlib.mq.dto.SendMailRequest;
import com.payriff.notification.dto.request.RequestMailDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaAdmin;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("api/v1/publish/")
public class PublishController {

    private final KafkaTemplate<String, String> kafkaTemplate;
    private final KafkaAdmin kafkaAdmin;

    @PostMapping

    public ResponseEntity<?> sendEmail(@RequestBody RequestMailDto request) {
        log.info("Publishing event");
        SendMailRequest mailRequest =
                SendMailRequest.builder()
                        .from("no-replay@payriff.com")
                        .to("besirovnurlan@gmail.com")
                        .subject("Test from ms notify")
                        .content("Salam this is test content")
                        .build();

        kafkaTemplate.send("mail4", JSON.toJSONString(mailRequest));

        kafkaAdmin.getConfigurationProperties().entrySet().forEach(x -> {
            log.info("key: " + x.getKey() + " - value" + x.getValue());
        });

        log.info("Published");
        return ResponseEntity.ok().build();
    }

}
