package com.payriff.adapter.provider.dto.response;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class ProviderCheckResponse {

    private String fullName;
    private BigDecimal amount;
    private String description;
    private String email;
    private String phoneNumber;

}
