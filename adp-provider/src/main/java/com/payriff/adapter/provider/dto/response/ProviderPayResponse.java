package com.payriff.adapter.provider.dto.response;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class ProviderPayResponse {

    private String transactionId;

}
