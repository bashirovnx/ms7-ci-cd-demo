package com.payriff.adapter.provider.error;

import com.payriff.adapter.provider.dto.ResultStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
@Slf4j
public class ErrorHandler extends ResponseEntityExceptionHandler {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({ServiceException.class})
    public ResponseEntity<ErrorResponseDto> handleServiceException(ServiceException ex) {

        return ResponseEntity.badRequest()
                .body(ErrorResponseDto.builder()
                        .status(ResultStatus.ERROR)
                        .code(ex.getCode())
                        .message(ex.getMessage())
                        .build());
    }

}
