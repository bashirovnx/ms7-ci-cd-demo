package com.payriff.adapter.provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication(scanBasePackages = "com.payriff")
@EnableFeignClients(basePackages = "com.payriff.adapter.provider.client")
@EnableDiscoveryClient
public class AdpProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(AdpProviderApplication.class, args);
    }

}
