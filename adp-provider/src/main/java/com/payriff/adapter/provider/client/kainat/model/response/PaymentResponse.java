package com.payriff.adapter.provider.client.kainat.model.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PaymentResponse {

    private Long contractId;
    private BigDecimal amount;
    private String operationUid;
    private String updatedAt;

}
