package com.payriff.adapter.provider.mapper;

import com.payriff.adapter.provider.client.kainat.model.request.PaymentRequest;
import com.payriff.adapter.provider.dto.request.PaymentRequestDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface KainatMapper {

    PaymentRequest toPaymentRequest(PaymentRequestDto paymentRequestDto);

}
