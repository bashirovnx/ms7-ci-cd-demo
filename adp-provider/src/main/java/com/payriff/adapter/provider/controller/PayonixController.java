package com.payriff.adapter.provider.controller;

import com.payriff.adapter.provider.dto.request.ProviderCheckRequest;
import com.payriff.adapter.provider.dto.request.ProviderPayRequest;
import com.payriff.adapter.provider.dto.response.ProviderCheckResponse;
import com.payriff.adapter.provider.dto.response.ProviderPayResponse;
import com.payriff.adapter.provider.service.PayonixService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/payonix")
@RequiredArgsConstructor
public class PayonixController {

    private final PayonixService payonixService;

    @PostMapping(path = "/check")
    ResponseEntity<ProviderCheckResponse> check(@RequestBody ProviderCheckRequest request) {
        return payonixService.check(request);
    }

    @PostMapping(path = "/pay")
    ResponseEntity<ProviderPayResponse> pay(@RequestBody ProviderPayRequest request) {
        return payonixService.pay(request);
    }

}