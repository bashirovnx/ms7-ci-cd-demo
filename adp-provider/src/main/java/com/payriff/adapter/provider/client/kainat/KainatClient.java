package com.payriff.adapter.provider.client.kainat;

import com.payriff.adapter.provider.client.kainat.model.Contract;
import com.payriff.adapter.provider.client.kainat.model.request.PaymentRequest;
import com.payriff.adapter.provider.client.kainat.model.response.GeneralResponse;
import com.payriff.adapter.provider.client.kainat.model.response.PaymentResponse;
import com.payriff.adapter.provider.config.FeignConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient(url = "${service.kainat.url}", name = "adp-kainat", configuration = FeignConfiguration.class)
public interface KainatClient {

    @GetMapping(path = "/contracts/{pin}")
    GeneralResponse<List<Contract>> getContracts(@PathVariable("pin") String pin);

    @PostMapping(path = "/make-payment")
    GeneralResponse<PaymentResponse> makePayment(@RequestBody PaymentRequest request);

}
