package com.payriff.adapter.provider.client.payonix.model.response;

import lombok.Data;

@Data
public class CheckResponse {

    private String fullName;

}
