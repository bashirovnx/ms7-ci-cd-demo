package com.payriff.adapter.provider.client.payonix.error;

import com.payriff.adapter.provider.error.ErrorCode;
import feign.Response;
import feign.codec.Decoder;
import feign.codec.ErrorDecoder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
@RequiredArgsConstructor
public class KainatErrorDecoder implements ErrorDecoder {

    private final Decoder decoder;


    @Override
    public Exception decode(String methodKey, Response response) {

        httpStatusDecoder(response.status());

        BaseResponse error;
        try {
            error = (BaseResponse) decoder.decode(response, BaseResponse.class);
        } catch (Exception e) {
            return new KainatClientException(
                    ErrorCode.INTERNAL_ERROR.name(), e.getMessage()
            );
        }

        return new KainatClientException(error.getResponseCode(), error.getMessage());

    }

    private static void httpStatusDecoder(int status) {

        switch (status) {
            case 401:
            case 403:
                throw new KainatClientException(ErrorCode.AUTHORIZATION_ERROR.name(), "Authorization error");
        }

    }

    @Data
    public static class BaseResponse {

        private String responseCode;
        private String message;

    }

}
