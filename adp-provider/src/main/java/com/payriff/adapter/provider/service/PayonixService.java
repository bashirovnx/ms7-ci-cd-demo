package com.payriff.adapter.provider.service;

import com.payriff.adapter.provider.client.kainat.error.KainatClientException;
import com.payriff.adapter.provider.client.payonix.PayonixClient;
import com.payriff.adapter.provider.client.payonix.model.request.PaymentRequest;
import com.payriff.adapter.provider.client.payonix.model.response.CheckResponse;
import com.payriff.adapter.provider.client.payonix.model.response.PaymentResponse;
import com.payriff.adapter.provider.dto.request.ProviderCheckRequest;
import com.payriff.adapter.provider.dto.request.ProviderPayRequest;
import com.payriff.adapter.provider.dto.response.ProviderCheckResponse;
import com.payriff.adapter.provider.dto.response.ProviderPayResponse;
import com.payriff.adapter.provider.error.ServiceException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class PayonixService {

    @Value("${service.payonix.security.authorization}")
    private String authorization;

    private final PayonixClient payonixClient;

    public ResponseEntity<ProviderCheckResponse> check(ProviderCheckRequest request) {

        log.info("Payonix check request : {}", request);

        try {
            CheckResponse response = payonixClient.check(authorization, request.getValue());

            log.info("Payonix check response: " + response);

            return ResponseEntity.ok(
                    ProviderCheckResponse.builder()
                            .fullName(response.getFullName())
                            .build()
            );
        } catch (KainatClientException ex) {
            log.error("Error on find contract by pin", ex);
            throw ServiceException.of(ex.getCode(), ex.getMessage());
        }
    }

    public ResponseEntity<ProviderPayResponse> pay(ProviderPayRequest request) {
        log.info("Payonix pay request: {}", request);
        try {
            PaymentRequest paymentRequest = PaymentRequest.builder()
                    .amount(request.getAmount())
                    .trackingId(request.getRrn().substring(0, 20))
                    .mobile(request.getValue())
                    .build();

            PaymentResponse response = payonixClient.pay(authorization, paymentRequest);

            log.info("Payonix pay response: " + response);

            return ResponseEntity.ok(ProviderPayResponse.builder()
                    .transactionId(response.getTransactionId())
                    .build());
        } catch (KainatClientException ex) {
            throw ServiceException.of(ex.getCode(), ex.getMessage());
        }
    }

}
