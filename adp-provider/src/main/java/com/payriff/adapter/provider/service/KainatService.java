package com.payriff.adapter.provider.service;

import com.payriff.adapter.provider.client.kainat.KainatClient;
import com.payriff.adapter.provider.client.kainat.error.KainatClientException;
import com.payriff.adapter.provider.client.kainat.model.Contract;
import com.payriff.adapter.provider.client.kainat.model.response.GeneralResponse;
import com.payriff.adapter.provider.client.kainat.model.response.PaymentResponse;
import com.payriff.adapter.provider.dto.request.PaymentRequestDto;
import com.payriff.adapter.provider.error.ServiceException;
import com.payriff.adapter.provider.mapper.KainatMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class KainatService {

    private final KainatClient kainatClient;
    private final KainatMapper mapper;

    public GeneralResponse<List<Contract>> getContractByPinCode(String pin) {
        log.info("Find contract by pin : {}", pin);

        try {
            return kainatClient.getContracts(pin);
        } catch (KainatClientException ex) {
            log.error("Error on find contract by pin", ex);
            throw ServiceException.of(ex.getCode(), ex.getMessage());
        }
    }

    public GeneralResponse<PaymentResponse> makePayment(PaymentRequestDto paymentRequest) {
        log.info("Make payment: {}", paymentRequest);
        try {
            return kainatClient.makePayment(mapper.toPaymentRequest(paymentRequest));
        } catch (KainatClientException ex) {
            throw ServiceException.of(ex.getCode(), ex.getMessage());
        }
    }

}
