package com.payriff.adapter.provider.client.payonix;

import com.payriff.adapter.provider.client.payonix.model.request.PaymentRequest;
import com.payriff.adapter.provider.client.payonix.model.response.CheckResponse;
import com.payriff.adapter.provider.client.payonix.model.response.PaymentResponse;
import com.payriff.adapter.provider.config.FeignConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(url = "${service.payonix.url}", name = "adp-payonix", configuration = FeignConfiguration.class)
public interface PayonixClient {

    @GetMapping(path = "/check")
    CheckResponse check(@RequestHeader("X-API-KEY") String authorization,
                        @RequestParam(value = "mobile") String mobile);

    @PostMapping(path = "/pay")
    PaymentResponse pay(@RequestHeader("X-API-KEY") String authorization,
                        @RequestBody PaymentRequest request);

}
