package com.payriff.adapter.provider.error;

public enum ErrorCode {

    INTERNAL_ERROR,
    AUTHORIZATION_ERROR

}
