package com.payriff.adapter.provider.controller;

import com.payriff.adapter.provider.client.kainat.model.Contract;
import com.payriff.adapter.provider.client.kainat.model.response.GeneralResponse;
import com.payriff.adapter.provider.client.kainat.model.response.PaymentResponse;
import com.payriff.adapter.provider.dto.request.PaymentRequestDto;
import com.payriff.adapter.provider.service.KainatService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/kainat")
@RequiredArgsConstructor
public class KainatController {

    private final KainatService kainatService;

    @GetMapping(path = "/contracts/{pin}")
    GeneralResponse<List<Contract>> getContracts(@PathVariable("pin") String pin) {
        return kainatService.getContractByPinCode(pin);
    }

    @PostMapping(path = "/make-payment")
    GeneralResponse<PaymentResponse> makePayment(@RequestBody PaymentRequestDto requestDto) {
        return kainatService.makePayment(requestDto);
    }

}