package com.payriff.adapter.provider.client.kainat.error;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class KainatClientException extends RuntimeException {

    private String code;
    private String message;

}
