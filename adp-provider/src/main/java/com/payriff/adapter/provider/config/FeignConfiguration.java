package com.payriff.adapter.provider.config;

import com.payriff.adapter.provider.client.kainat.error.KainatErrorDecoder;
import feign.Client;
import feign.Logger;
import feign.codec.Decoder;
import feign.codec.ErrorDecoder;
import lombok.RequiredArgsConstructor;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.ssl.TrustStrategy;
import org.springframework.context.annotation.Bean;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;

@RequiredArgsConstructor
public class FeignConfiguration {

    final Decoder decoder;

    @Bean
    Logger.Level feignLoggerLevel() {
        return Logger.Level.FULL;
    }

    @Bean
    public ErrorDecoder feignErrorDecoder() {
        return new KainatErrorDecoder(decoder);
    }

    @Bean
    public Client feignClient() {
        return new Client.Default(getSSLSocketFactory(),
                new NoopHostnameVerifier());
    }

    private SSLSocketFactory getSSLSocketFactory() {
        try {
            TrustStrategy acceptingTrustStrategy = (chain, authType) -> true;

            SSLContext sslContext = SSLContexts.custom()
                    .loadTrustMaterial(null, acceptingTrustStrategy)
                    .build();
            return sslContext.getSocketFactory();
        } catch (Exception exception) {

        }
        return null;
    }
}