package com.digipay.pypys.msdictionary.scheduler;

import com.digipay.pypys.msdictionary.model.enums.RateType;
import com.digipay.pypys.msdictionary.service.CurrencyService;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;

@RequiredArgsConstructor
public class DailyCurrencyRate {

    private final CurrencyService currencyRateService;

    @Scheduled(cron = "0 0 11 * * ?")
    public void checkDailyUsdRate() {
        currencyRateService.fetchDailyUsdRate(RateType.USD_TO_AZN);
    }

}


