package com.digipay.pypys.msdictionary.model.entity;

import com.digipay.pypys.msdictionary.model.enums.CurrencyType;
import com.digipay.pypys.msdictionary.model.enums.RateType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name = "currency_rate")
public class CurrencyRateEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "currency_rate_generator")
    @SequenceGenerator(
            name = "currency_rate_generator",
            sequenceName = "currency_rate_seq",
            allocationSize = 1
    )
    private Long id;

    @Column(name = "local_date")
    private LocalDate localDate;

    @Column(name = "rate_type")
    @ToString.Include
    private RateType rateType;

    @Column(name = "currency_type")
    @ToString.Include
    private CurrencyType currencyType;

    private Double rate;

}
