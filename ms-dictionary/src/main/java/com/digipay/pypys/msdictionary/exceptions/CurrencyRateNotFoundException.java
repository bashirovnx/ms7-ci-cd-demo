package com.digipay.pypys.msdictionary.exceptions;

public class CurrencyRateNotFoundException extends RuntimeException {

    public CurrencyRateNotFoundException(String message) {
        super(message);
    }

}
