package com.digipay.pypys.msdictionary.model.enums;

public enum RateType {

    USD_TO_AZN(0);

    int id;

    RateType(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

}
