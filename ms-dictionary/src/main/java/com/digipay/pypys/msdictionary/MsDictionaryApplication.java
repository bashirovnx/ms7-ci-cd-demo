package com.digipay.pypys.msdictionary;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableRetry
@EnableScheduling
@EnableDiscoveryClient
@EnableCaching
public class MsDictionaryApplication {

    public static void main(String[] args) {
        SpringApplication.run(MsDictionaryApplication.class, args);
    }

}
