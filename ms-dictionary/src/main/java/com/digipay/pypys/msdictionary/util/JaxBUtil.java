package com.digipay.pypys.msdictionary.util;

import lombok.extern.log4j.Log4j;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;

@Log4j
public class JaxBUtil {

    private JaxBUtil() {
    }

    public static <T> T toPOJO(String data, Class<T> tClass) {
        if (data == null) {
            throw new RuntimeException("No response from Gateway");
        }

        JAXBContext instance;
        try {
            instance = JAXBContext.newInstance(tClass);
            Unmarshaller unmarshaller = instance.createUnmarshaller();
            StringReader reader = new StringReader(data);
            return (T) unmarshaller.unmarshal(reader);
        } catch (JAXBException jaxbException) {
            log.error("Error on toPOJO", jaxbException);
            log.error(jaxbException.getMessage());
            throw new RuntimeException("XML cannot covert to POJO");
        }
    }

}
