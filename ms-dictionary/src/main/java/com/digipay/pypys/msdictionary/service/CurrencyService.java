package com.digipay.pypys.msdictionary.service;

import com.digipay.pypys.msdictionary.exceptions.CurrencyRateNotFoundException;
import com.digipay.pypys.msdictionary.model.CbarCurrencyResponse;
import com.digipay.pypys.msdictionary.model.entity.CurrencyRateEntity;
import com.digipay.pypys.msdictionary.model.enums.CurrencyType;
import com.digipay.pypys.msdictionary.model.enums.RateType;
import com.digipay.pypys.msdictionary.model.response.ResponseCurrencyRate;
import com.digipay.pypys.msdictionary.repository.CurrencyRateRepository;
import com.digipay.pypys.msdictionary.util.JaxBUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
@Log4j
public class CurrencyService {

    private final CurrencyRateRepository currencyRateRepository;
    private final RestTemplate restTemplate;

    @Cacheable(cacheNames = "currencyRate#3600", key = "#rateType", sync = true)
    public ResponseCurrencyRate getByRateType(RateType rateType) {

        CurrencyRateEntity entity = currencyRateRepository.findCurrencyRateEntitiesByRateType(rateType)
                .orElseThrow(
                        () -> new CurrencyRateNotFoundException("No rate found by type :" + rateType)
                );

        return ResponseCurrencyRate.builder()
                .rate(entity.getRate())
                .build();
    }

    public void fetchDailyUsdRate(RateType rateType) {

        CurrencyRateEntity rateEntity = currencyRateRepository.findCurrencyRateEntitiesByRateType(rateType)
                .orElseThrow(
                        () -> new CurrencyRateNotFoundException("No rate found by type :" + rateType)
                );

        String currencyInfo = getCurrencyInformation();

        log.info("Fetched curency xml info");

        CbarCurrencyResponse response =
                JaxBUtil.toPOJO(currencyInfo, CbarCurrencyResponse.class);

        for (CbarCurrencyResponse.ValType val : response.getValTypeList()) {
            for (CbarCurrencyResponse.ValType.Valute valute :
                    val.getValuteList()) {
                if (CurrencyType.USD.toString().equals(valute.getCode())) {
                    rateEntity.setRate(Double.valueOf(valute.getValue()));
                    rateEntity.setLocalDate(LocalDate.now());
                    break;
                }
            }
        }
    }

    @Retryable(value = Exception.class)
    public String getCurrencyInformation() {

        LocalDate localDate = LocalDate.now();

        int year = localDate.getYear();
        int month = localDate.getMonthValue();
        int day = localDate.getDayOfMonth();

        String endOfUrl = String.format("%02d.%d.%d.xml", day, month, year);

        String url = String.format("https://cbar.az/currencies/%s", endOfUrl);

        return restTemplate.getForObject(url, String.class);
    }

}
