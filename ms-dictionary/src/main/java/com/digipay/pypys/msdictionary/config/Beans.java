package com.digipay.pypys.msdictionary.config;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class Beans {

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }


//    @Bean
//    public RedisTemplate<String, Object> redisTemplate() {
//        return new RedisTemplate<>();
//    }
//
//    @Bean
//    public RedisCacheConfiguration redisCacheConfiguration(RedisTemplate<String, Object> template) {
//        return RedisCacheConfiguration
//                .defaultCacheConfig()
//                //Set key to string
//                .serializeKeysWith(RedisSerializationContext.SerializationPair.fromSerializer(template.getStringSerializer()))
//                //Set value to automatically transfer to JSON's object
//                .serializeValuesWith(RedisSerializationContext.SerializationPair.fromSerializer(template.getValueSerializer()))
//                //Do not cache null
//                .disableCachingNullValues()
//                //Cache data is saved for 180L second
//                .entryTtl(Duration.ofSeconds(180L));
//    }

//    @Bean
//    public CacheManager cacheManager(RedisTemplate<String, Object> template) {
//        RedisCacheManager redisCacheManager =
//                RedisCacheManager.RedisCacheManagerBuilder
//                        //Redis connection factory
//                        .fromConnectionFactory(template.getRequiredConnectionFactory())
//                        .cacheDefaults(redisCacheConfiguration(template))
//                        //Configure synchronous modification or deletion of put / evict
//                        .transactionAware()
//                        .build();
//
//        return redisCacheManager;
//    }
}


