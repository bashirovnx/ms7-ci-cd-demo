package com.digipay.pypys.msdictionary.model.response;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class ResponseCurrencyRate implements Serializable {
    private Double rate;
}
