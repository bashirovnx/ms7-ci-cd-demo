package com.digipay.pypys.msdictionary.model;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.List;

@XmlRootElement(name = "ValCurs")
@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
public class CbarCurrencyResponse implements Serializable {

    @XmlAttribute(name = "Date")
    private String date;
    @XmlAttribute(name = "Name")
    private String name;
    @XmlAttribute(name = "Description")
    private String description;

    @XmlElement(name = "ValType")
    private List<ValType> valTypeList;

    @XmlAccessorType(XmlAccessType.FIELD)
    @Getter
    @Setter
    public static class ValType {

        @XmlAttribute(name = "Type")
        private String type;

        @XmlElement(name = "Valute")
        private List<Valute> valuteList;

        @XmlAccessorType(XmlAccessType.FIELD)
        @Getter
        @Setter
        public static class Valute {

            @XmlAttribute(name = "Code")
            private String code;
            @XmlElement(name = "Nominal")
            private String nominal;
            @XmlElement(name = "Name")
            private String name;
            @XmlElement(name = "Value")
            private String value;
        }
    }

}
