package com.digipay.pypys.msdictionary.repository;

import com.digipay.pypys.msdictionary.model.entity.CurrencyRateEntity;
import com.digipay.pypys.msdictionary.model.enums.CurrencyType;
import com.digipay.pypys.msdictionary.model.enums.RateType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CurrencyRateRepository extends JpaRepository<CurrencyRateEntity, Long> {

    Optional<CurrencyRateEntity> findCurrencyRateEntitiesByRateType(RateType rateType);

    Optional<CurrencyRateEntity> findCurrencyRateEntitiesByCurrencyType(CurrencyType currencyType);

}
