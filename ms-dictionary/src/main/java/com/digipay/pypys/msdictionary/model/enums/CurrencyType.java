package com.digipay.pypys.msdictionary.model.enums;

public enum CurrencyType {

    USD(0);

    private final int id;

    CurrencyType(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

}
