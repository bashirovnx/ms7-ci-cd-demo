package com.digipay.pypys.msdictionary.controller;


import com.digipay.pypys.msdictionary.model.enums.RateType;
import com.digipay.pypys.msdictionary.model.response.ResponseCurrencyRate;
import com.digipay.pypys.msdictionary.service.CurrencyService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/dictionary")
@RequiredArgsConstructor
public class DictionaryController {

    private final CurrencyService currencyRateService;

    @GetMapping("/currency/{rateType}")
    public ResponseEntity<ResponseCurrencyRate> getCurrencyRate(@PathVariable("rateType") RateType rateType) {
        return ResponseEntity.ok(currencyRateService.getByRateType(rateType));
    }

}
