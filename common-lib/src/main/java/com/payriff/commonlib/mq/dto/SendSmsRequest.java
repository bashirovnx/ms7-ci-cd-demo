package com.payriff.commonlib.mq.dto;

import lombok.Data;

@Data
public class SendSmsRequest {
    private String message;
    private String phone;
    private boolean mandatory = false;

}
