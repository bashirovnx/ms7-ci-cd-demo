package com.payriff.commonlib.mq.dto;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SendMailRequest implements Serializable {
    private String to;
    private String from;
    private String[] cc;
    private String[] bcc;
    private String subject;
    private String content;
    private String attachmentPath;
}
