//package com.payriff.commonlib.util;
//
//import com.digipay.pysys.edd.event.SendSupportEvent;
//import com.digipay.pysys.model.entity.processing.TransactionEntity;
//import lombok.RequiredArgsConstructor;
//import lombok.extern.log4j.Log4j;
//import org.springframework.context.ApplicationEventPublisher;
//import org.springframework.http.HttpEntity;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.HttpMethod;
//import org.springframework.http.ResponseEntity;
//import org.springframework.stereotype.Component;
//import org.springframework.web.client.RestTemplate;
//
//import static com.digipay.pysys.util.CommonUtil.getFilteredStackTrace;
//
//@Component
//@Log4j
//@RequiredArgsConstructor
//public class WalletUtil {
//    private ApplicationEventPublisher applicationEventPublisher;
//
//    public void depositWallet(TransactionEntity transactionEntity) {
//        try {
//            // walletService.depositWallet(transactionEntity);
//
//            final String DEPOSIT_URL = "https://api.payriff.com/api/v1/terminal/wallet/internal/deposit";
//
//            HttpHeaders headers = new HttpHeaders();
//            headers.add(HttpHeaders.AUTHORIZATION, "Basic dGVybWluYWw6dGU3bWluQCE=");
//
//            log.info("Request : " + transactionEntity);
//
//            HttpEntity<TransactionEntity> request = new HttpEntity<>(transactionEntity, headers);
//            ResponseEntity<String> response =
//                    new RestTemplate().exchange(DEPOSIT_URL, HttpMethod.POST, request, String.class);
//
//            log.info("Response : " + response);
//
//        } catch (Exception e) {
//            log.error(
//                    "Fail deposit to wallet amount: " + transactionEntity.getPaidAmount(),
//                    getFilteredStackTrace(e));
//            SendSupportEvent sendSupportEvent =
//                    new SendSupportEvent(
//                            transactionEntity,
//                            "Fail to add paid amount to wallet \n paid amount:"
//                                    .concat(transactionEntity.getPaidAmount().toString())
//                                    + transactionEntity);
//
//            applicationEventPublisher.publishEvent(sendSupportEvent);
//        }
//        log.info("Successful deposit wallet");
//    }
//
//}
