package com.payriff.commonlib.util;

import java.util.Random;
import java.util.UUID;

/**
 * @author HasanovZK
 */
public class GuidUtil {
    static Random rnd = new Random();
    private static final String STATIC_OTP = "123567";

    public static String get() {
        return UUID.randomUUID().toString().replace("-", "").toUpperCase();
    }

    public static String getToLowerCase() {
        return UUID.randomUUID().toString().replace("-", "").toLowerCase();
    }

    public static String getSixDigitOtp() {
        int number = rnd.nextInt(999999);
        return String.format("%06d", number);
    }

}
