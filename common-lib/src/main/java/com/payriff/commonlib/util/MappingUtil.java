package com.payriff.commonlib.util;


import com.payriff.commonlib.persistence.entity.application.CustomerInvoiceEntity;

public class MappingUtil {

  public static CustomerInvoiceEntity toCustomerInvoiceEntity(
      CustomerInvoiceEntity target, CustomerInvoiceEntity source) {
    if (source == null) {
      return target;
    }

    if (source.getMerchantId() != null) {
      target.setMerchantId(source.getMerchantId());
    }
    if (source.getAmount() != null) {
      target.setAmount(source.getAmount());
    }
    if (source.getFullName() != null) {
      target.setFullName(source.getFullName());
    }
    if (source.getEmail() != null) {
      target.setEmail(source.getEmail());
    }
    if (source.getPhoneNumber() != null) {
      target.setPhoneNumber(source.getPhoneNumber());
    }
    if (source.getCustomMessage() != null) {
      target.setCustomMessage(source.getCustomMessage());
    }
    if (source.getExpireDate() != null) {
      target.setExpireDate(source.getExpireDate());
    }
    if (source.getCurrencyType() != null) {
      target.setCurrencyType(source.getCurrencyType());
    }
    if (source.getLanguageType() != null) {
      target.setLanguageType(source.getLanguageType());
    }
    if (source.getPaymentType() != null) {
      target.setPaymentType(source.getPaymentType());
    }
    if (source.getSubscriptionState() != null) {
      target.setSubscriptionState(source.getSubscriptionState());
    }
    if (source.getPaymentDay() != null) {
      target.setPaymentDay(source.getPaymentDay());
    }
    if (source.getExpireDay() != null) {
      target.setExpireDay(source.getExpireDay());
    }
    target.setActive(source.isActive());
    if (source.getDescription() != null) {
      target.setDescription(source.getDescription());
    }
    if (source.getInvoiceStatus() != null) {
      target.setInvoiceStatus(source.getInvoiceStatus());
    }
    return target;
  }
}
