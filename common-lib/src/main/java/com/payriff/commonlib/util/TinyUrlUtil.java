package com.payriff.commonlib.util;

public class TinyUrlUtil {

    private static final String characterMap = "Qa2bWYZXcOd1R3T485efghiAEFxIJKLjklmNnopBCDUqrstGuvwyHzMPSV0769";
    private static final long charBase = characterMap.length();

    public static String covertToCharacter(long num) {
        num += 190_694_328;
        StringBuilder sb = new StringBuilder();

        while (num > 0) {
            sb.append(characterMap.charAt((int) (num % charBase)));
            num /= charBase;
        }

        return sb.reverse().toString();
    }

    public static long covertToNumber(String str) {
        long num = 0;
        for (int i = 0; i < str.length(); i++)
            num += characterMap.indexOf(str.charAt(i)) * Math.pow(charBase, (str.length() - (i + 1)));
        num -= 190_694_328;
        return num;
    }
}
