package com.payriff.commonlib.util;


import com.payriff.commonlib.Constants;
import com.payriff.commonlib.enums.LanguageType;
import org.apache.commons.codec.digest.MessageDigestAlgorithms;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author HasanovZK
 */
public class CommonUtil {

    public static void storeFile(String data, String filename) {
        try {
            File newTextFile = new File(Constants.CERTS_UPLOAD_PATH + filename);
            try (FileWriter fw = new FileWriter(newTextFile)) {
                fw.write(data);
            }
        } catch (IOException iox) {
            throw new RuntimeException("CSR file could not stored");
        }
    }

    public static String encodeSHA1(String data) {
        String sha1;
        try {
            MessageDigest crypt = MessageDigest.getInstance("SHA-1");
            crypt.reset();
            crypt.update(data.getBytes(StandardCharsets.UTF_8));
            sha1 = byteToHex(crypt.digest());
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("StandardResult.ResultMessages.ERROR", e);
        }
        return sha1;
    }

    public static String hashMD5(String data) {
        String md5Hash;
        try {
            MessageDigest md = MessageDigest.getInstance(MessageDigestAlgorithms.MD5);
            md.reset();
            md.update(data.getBytes());
            md5Hash = byteToHex(md.digest());
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("StandardResult.ResultMessages.ERROR", e);
        }
        return md5Hash;
    }

    private static String byteToHex(final byte[] hash) {
        Formatter formatter = new Formatter();
        for (byte b : hash) {
            formatter.format("%02x", b);
        }
        String result = formatter.toString();
        formatter.close();
        return result;
    }

    public static String removeNameSpaces(String xml) {
        return xml.replaceAll("^(xmlmsg=)", "");
    }

    public static String removeNameSpaces2(String xml) {
        return xml.replaceAll("(<\\?[^<]*\\?>)?", "")
                .replaceAll("xmlns.*?(\"|\').*?(\"|\')", "")
                .replaceAll("(<)(\\w+:)(.*?>)", "$1$3")
                .replaceAll("(</)(\\w+:)(.*?>)", "$1$3");
    }


    public static String encodeFileToBase64(File file) {
        try {
            byte[] fileContent = Files.readAllBytes(file.toPath());
            return Base64.getEncoder().encodeToString(fileContent);
        } catch (IOException e) {
            throw new IllegalStateException("could not read file " + file, e);
        }
    }

    public static String encodeFileToBase64(byte[] bytes) {
        if (bytes == null) return null;
        return Base64.getEncoder().encodeToString(bytes);
    }

    public static LocalDate getLocalDate(String date) {
        return LocalDate.parse(date, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
    }

    public static LocalDateTime getLocalDateTime(String date) {
        return LocalDateTime.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
    }


    public static LinkedHashMap<LocalDate, Long> getMonthMapBetweenTwoDates(
            LocalDate beginDate, LocalDate endDate) {
        LinkedHashMap<LocalDate, Long> map = new LinkedHashMap<>();

        final Long ZERO_COUNT = 0L;
        while (beginDate.isBefore(endDate)) {
            map.put(beginDate, ZERO_COUNT);
            beginDate = beginDate.plusMonths(1);
        }
        return map;
    }

    public static <K, V extends Comparable<V>> Map.Entry<K, V> getMapMaxValue(Map<K, V> map) {
        return Collections.max(map.entrySet(), Map.Entry.comparingByValue());
    }

    public static <K, V extends Comparable<V>> Map.Entry<K, V> getMapMinValue(Map<K, V> map) {
        return Collections.min(map.entrySet(), Map.Entry.comparingByValue());
    }

    public static Double getMapAverageValue(Map<LocalDate, Long> map) {
        return map.values().stream().mapToDouble(Long::longValue).average().orElse(0);
    }

    public static String validatePhoneNumber(String phone) {

        String preffix = "+994";

        if (!phone.contains("+")) {
            return preffix.concat(phone.substring(phone.length() - 9, phone.length()));
        } else {
            return phone;
        }
    }

    public static void fileCopy(String source, String destination) {

        File file = new File(source);

        try {
            Files.copy(
                    file.toPath(), (new File(destination)).toPath(), StandardCopyOption.COPY_ATTRIBUTES);
        } catch (IOException e) {
        }
    }

    public static Date subtractDays(Date date, int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, -days);

        return cal.getTime();
    }

    public static String getInvoiceStandartMessage(LanguageType type) {
        String INVOICE_MSJ_AZ =
                "Hormetli {fullName},\n{amount} {currency} mebleginde odenilmemish fakturaniz var. \nInvoys kodu: {invoice_code} \n{paymentUrl}";
        String INVOICE_MSJ_EN =
                "Dear Customer,\nYou have an unpaid invoice {amount} {currency}.\nInvoice code: {invoice_code} \n{paymentUrl}";
        String INVOICE_MSJ_RU =
                "Uvajaemiy Klient,\nU vas neoplachenniy schet {amount} {currency} \nInvoys kodu: {invoice_code} \n{paymentUrl}";
        switch (type) {
            case AZ:
                return INVOICE_MSJ_AZ;
            case EN:
                return INVOICE_MSJ_EN;
            case RU:
                return INVOICE_MSJ_RU;
        }
        return INVOICE_MSJ_AZ;
    }

    public static String getInvoiceCode(Long invoiceId) {
        return String.valueOf(1_000_000L + invoiceId);
    }

    public static boolean isPasswordValid(String password) {

        String regExpn = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&+=])(?=\\S+$).{8,}$";

        CharSequence inputStr = password;

        Pattern pattern = Pattern.compile(regExpn, Pattern.UNICODE_CASE);
        Matcher matcher = pattern.matcher(inputStr);

        return matcher.matches();
    }

    public static String getApproveUrlBySource(String source) {
        final String SOURCE_B2B = "b2b";

        String defaultUrl = "https://dashboard.payriff.com/#/apps/list";
        String b2bUrl = "https://b2b.payriff.com/#/apps/list";

        if (SOURCE_B2B.equalsIgnoreCase(source)) {
            return b2bUrl;
        } else {
            return defaultUrl;
        }
    }

    public static String getDeclineUrlBySource(String source) {
        final String SOURCE_B2B = "b2b";

        String defaultUrl = "https://dashboard.payriff.com/#/dashboard/connect";
        String b2bUrl = "https://b2b.payriff.com/#/apps/list";

        if (SOURCE_B2B.equalsIgnoreCase(source)) {
            return b2bUrl;
        } else {
            return defaultUrl;
        }
    }

    public static String getCancelUrlBySource(String source) {
        final String SOURCE_B2B = "b2b";

        String defaultUrl = "https://dashboard.payriff.com/#/dashboard/connect";
        String b2bUrl = "https://b2b.payriff.com/#/apps/list";

        if (SOURCE_B2B.equalsIgnoreCase(source)) {
            return b2bUrl;
        } else {
            return defaultUrl;
        }
    }


    public static boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }
}
