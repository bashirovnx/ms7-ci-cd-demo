package com.payriff.commonlib.dto.application;

import lombok.*;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class AsanAuthResponseDto {

    private String transactionId;

    private String verificationCode;

    private String firstname;

    private String surname;

    private String organization;

    private String organizationName;

    private String organizationCode;

    private String role;

    private String informationSystemName;

    private String certType;

    private String personalCode;

    private String phoneNumber;

    private String certificateId;

}
