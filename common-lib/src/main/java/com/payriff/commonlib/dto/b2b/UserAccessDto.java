package com.payriff.commonlib.dto.b2b;

import lombok.Data;

/** @author HasanovZK */
@Data
public class UserAccessDto {
   private Long userId;
  private Long appId;
  private Long roleId;
}
