package com.payriff.commonlib.dto.application;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ResponseCompanyDto {

    private Long id;

    private String name;

    private String website;

    private String voen;

    private String address;

    private String description;

    private String fin;

    private String companyCategory;

}
