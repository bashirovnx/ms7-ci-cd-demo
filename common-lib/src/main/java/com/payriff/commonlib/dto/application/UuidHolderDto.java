package com.payriff.commonlib.dto.application;

import lombok.Data;

@Data
public class UuidHolderDto {
  String uuid;
}
