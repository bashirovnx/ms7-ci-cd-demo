package com.payriff.commonlib.dto.application;

import lombok.Data;

/**
 * @author HasanovZK
 */
@Data
public class BankAccountDto {
    private String serviceUser;
    private String address;
    private String postalCode;
    private String voen;
    private String bankName;
    private String accountNumber;
    private String iban;
    private String code;
    private String bankVoen;
    private String swift;
    private String phone;
}
