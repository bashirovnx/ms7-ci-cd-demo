package com.payriff.commonlib.dto.application;

/**
 * @author HasanovZK
 */
public class ContactDto {
    private String name;
    private String lastname;
    private String mail;
    private String phone;
    private String message;
}
