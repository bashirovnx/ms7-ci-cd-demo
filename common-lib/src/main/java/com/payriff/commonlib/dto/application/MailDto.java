package com.payriff.commonlib.dto.application;

import lombok.Data;

/**
 * @author HasanovZK
 */
@Data
public class MailDto {
    private String email;
}
