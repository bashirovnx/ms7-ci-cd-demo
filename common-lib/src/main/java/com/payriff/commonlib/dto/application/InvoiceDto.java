package com.payriff.commonlib.dto.application;

import com.payriff.commonlib.enums.CurrencyType;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Builder
@EqualsAndHashCode(callSuper = false)
public class InvoiceDto  {

    private String senderCompany;

    private String fullName;

    private BigDecimal amount;

    private CurrencyType currencyType;

    private String description;

    private String email;

    private String phoneNumber;

    private BigDecimal maxAmount;


    private LocalDateTime checkinDate;

    private LocalDateTime expiryDate;

    private String backRrn;

}
