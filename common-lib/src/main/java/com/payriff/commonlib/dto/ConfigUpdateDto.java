package com.payriff.commonlib.dto;

import lombok.Data;

import java.util.Set;

/** @author HasanovZK */
@Data
public class ConfigUpdateDto {
  private long configId;
  private long appId;
  private boolean directPay;
  private String defaultSmsTemplate;
  private String defaultMailTemplate;
  private String applicationMail;
  private Set<Long> availableCurrencies;
}
