package com.payriff.commonlib.dto.application;

import com.payriff.commonlib.enums.LanguageType;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author HasanovZK
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class GetOrderInformationDto {
    private LanguageType languageType;
    private long orderId;
    private String sessionId;
}
