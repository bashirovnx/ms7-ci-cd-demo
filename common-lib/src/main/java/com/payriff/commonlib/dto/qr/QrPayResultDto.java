package com.payriff.commonlib.dto.qr;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author HasanovZK
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class QrPayResultDto {
    private String paymentUrl;
}
