package com.payriff.commonlib.dto.application;

/**
 * @author HasanovZK
 */

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author HasanovZK
 */
@Data
public class WithdrawDto {
    private BigDecimal amount;
    private Long appId;
}
