package com.payriff.commonlib.dto.application;

import com.payriff.commonlib.enums.CurrencyType;
import com.payriff.commonlib.enums.InvoiceStatus;
import com.payriff.commonlib.enums.LanguageType;
import com.payriff.commonlib.enums.PaymentType;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class CustomerInvoiceResponseDto {
  private Long id;
  private String merchantId;
  private BigDecimal amount;
  private String fullName;
  private String email;
  private String phoneNumber;
  private String customMessage;
  private LocalDateTime expireDate;
  private CurrencyType currencyType;
  private LanguageType languageType;
  private PaymentType paymentType;
  private boolean active = true;
  private String description;
  private String approveURL;
  private String cancelURL;
  private String declineURL;
  private String uuid;
  private String invoiceUuid;
  private String invoiceCode;
  private InvoiceStatus invoiceStatus;
  private LocalDateTime createdDate;
  private String paymentUrl;
  private boolean sendSms = true;
}
