package com.payriff.commonlib.dto.application;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class VerificationResponseDto {

    private String verificationCode;

}
