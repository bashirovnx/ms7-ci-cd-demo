package com.payriff.commonlib.dto;

import lombok.Data;

/** @author Kanan */
@Data
public class GetSubUsersResponseDto {

  private Long id;
  private Long ownerId;
  private String ownerFullname;
  private Long subUserId;
  private String subFullname;
  private String merchant;
  private String appName;
  private String roleName;
}
