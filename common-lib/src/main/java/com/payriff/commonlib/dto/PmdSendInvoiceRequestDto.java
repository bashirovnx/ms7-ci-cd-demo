package com.payriff.commonlib.dto;

import lombok.Data;

/**
 * @author Kanan
 */
@Data
public class PmdSendInvoiceRequestDto {
    private Long id;
    private boolean viaSms;
    private boolean viaEmail;
}
