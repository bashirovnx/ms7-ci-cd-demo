package com.payriff.commonlib.dto.admin;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @author HasanovZK
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TransferDto {
    private Long appId;
    private String appName;
    private BigDecimal totalAmount;
    private BigDecimal totalPaymentAmount;

}

//@Data
//class TransferDtoItem {
//    private Long id;
//    private Long appId;
//    private String merchantId;
//    private BigDecimal amount;
//    private BigDecimal commissionRate;
//    private String rrn;
//}
