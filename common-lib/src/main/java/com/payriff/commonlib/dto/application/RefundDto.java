package com.payriff.commonlib.dto.application;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author HasanovZK
 */
@Data
public class RefundDto {

    private Long transactionId;
    private BigDecimal refundAmount;
}
