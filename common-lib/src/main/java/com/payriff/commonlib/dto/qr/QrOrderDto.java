package com.payriff.commonlib.dto.qr;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author HasanovZK
 */
@Data
public class QrOrderDto {
    private BigDecimal amount;
    private String fullName;
    private String phoneNumber;
    private String qrCode;
}
