package com.payriff.commonlib.dto.application;

import lombok.Data;

@Data
public class ConfigUpdateDto {
    private long configId;
    private long appId;
    private boolean directPay;
}
