package com.payriff.commonlib.dto.application;

import com.payriff.commonlib.enums.EmanatInvoiceReqType;
import lombok.Data;

@Data
public class InvoiceDetailRequestDto {

    private String src;
    private String clientRrn;
    private EmanatInvoiceReqType type;
    private String value;

}
