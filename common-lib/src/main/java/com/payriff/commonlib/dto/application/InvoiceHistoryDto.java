package com.payriff.commonlib.dto.application;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
@Builder
public class InvoiceHistoryDto {

    LocalDate date;
    List<String> message;

}
