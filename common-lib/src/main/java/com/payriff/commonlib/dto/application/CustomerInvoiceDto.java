package com.payriff.commonlib.dto.application;

import com.payriff.commonlib.enums.CurrencyType;
import com.payriff.commonlib.enums.InvoiceSource;
import com.payriff.commonlib.enums.InvoiceStatus;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Builder
@EqualsAndHashCode(callSuper = false)
public class CustomerInvoiceDto {

  private Long id;
  private String senderCompany;
  private String fullName;
  private BigDecimal amount;
  private CurrencyType currencyType;
  private String description;
  private String email;
  private String phoneNumber;
  private BigDecimal maxAmount;
  private String checkinDate;
  private String expiryDate;
  private InvoiceStatus invoiceStatus;
  private InvoiceSource source;
  private Long applicationEntity;
  private Long userEntity;
  private String uuid;
  private String invoiceUuid;
  private LocalDate paymentDay;
  private LocalDate expireDay;

}
