package com.payriff.commonlib.dto.application;

import com.payriff.commonlib.enums.AccountType;
import lombok.Data;

/**
 * @author HasanovZK
 */
@Data
public class ConnectionDto {

    private ApplicationDto applicationDto;
    private CompanyDto companyDto;
    private AccountType accountType;
    private Long companyId;
    private String hash;

}
