package com.payriff.commonlib.dto.application;

import lombok.Data;

/**
 * @author HasanovZK
 */
@Data
public class MerchantCategoryDto {
    private Long id;
    private String name;
}
