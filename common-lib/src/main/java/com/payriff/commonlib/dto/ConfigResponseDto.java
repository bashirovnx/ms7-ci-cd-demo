package com.payriff.commonlib.dto;


import com.payriff.commonlib.enums.ApplicationStatus;
import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;

@Data
@ToString(onlyExplicitlyIncluded = true)
public class ConfigResponseDto {
  @ToString.Include private Long id;
  @ToString.Include private Boolean active;
  @ToString.Include private String defaultSmsTemplate;
  @ToString.Include private String defaultMailTemplate;
  @ToString.Include private String applicationMail;
  @ToString.Include private Boolean directPay;
  @ToString.Include private BigDecimal amountLimitForInternationalPhone;
  @ToString.Include private Long appId;
  @ToString.Include private ApplicationStatus applicationStatus;
  private String logoPhoto;
}
