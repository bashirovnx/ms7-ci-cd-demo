package com.payriff.commonlib.dto.application;

import com.payriff.commonlib.enums.UserType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/** @author HasanovZK */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UserUpdateDto {

  private Long id;


  private String email;

  private String fullName;

  private String phone;

  private UserType userType;

}
