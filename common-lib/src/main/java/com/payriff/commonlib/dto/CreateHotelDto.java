package com.payriff.commonlib.dto;

import lombok.Data;

/**
 * @author Kanan
 */
@Data
public class CreateHotelDto {
    private String name;
}
