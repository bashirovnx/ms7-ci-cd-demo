package com.payriff.commonlib.dto.application;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class EmanatPayRequestDto {

    private BigDecimal amount;

    private String backRrn;

    private boolean partial;

}
