package com.payriff.commonlib.dto;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author HasanovZK
 */
@Data
@Builder
public class CompleteOrderRequestDto {
    private Long transactionId;
    private BigDecimal amount;

}
