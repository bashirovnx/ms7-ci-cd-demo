package com.payriff.commonlib.dto.application;

import com.payriff.commonlib.enums.ApplicationStatus;
import com.payriff.commonlib.enums.GatewayType;
import lombok.Builder;
import lombok.Data;

/**
 * @author HasanovZK
 */
@Data
@Builder
public class ApplicationDto {

    private Long id;

    private String name;
    private String description;
    private String url;
    private GatewayType gatewayType;
    private ApplicationStatus status;
    private Long userId;

}
