package com.payriff.commonlib.dto.application;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/** @author HasanovZK */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class WalletResponseDto {
  private Long id;
  private String name;
  private BigDecimal balance;
  private Long appId;
  private boolean directPay;
  private Long configId;
  private boolean withdrawal;
}
