package com.payriff.commonlib.dto.application;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author HasanovZK
 */
@Data
public class GatewayRefundDto {
    private Long orderId;
    private BigDecimal refundAmount;
    private String sessionId;
}
