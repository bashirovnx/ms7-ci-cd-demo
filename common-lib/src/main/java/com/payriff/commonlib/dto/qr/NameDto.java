package com.payriff.commonlib.dto.qr;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author HasanovZK
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class NameDto {
    private String name;
}
