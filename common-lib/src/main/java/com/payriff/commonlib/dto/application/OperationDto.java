package com.payriff.commonlib.dto.application;

import com.payriff.commonlib.enums.LanguageType;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author HasanovZK
 */
@Data
public class OperationDto {
    private String orderId;

    private String sessionId;

    private LanguageType language;
    private String description;
    private BigDecimal amount;
}
