package com.payriff.commonlib.dto.security;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author HasanovZK
 */
@Getter
@Setter
@ToString
public class LoginDto {
    String username;
    String password;
}
