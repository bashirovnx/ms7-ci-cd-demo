package com.payriff.commonlib.dto.application;

import com.payriff.commonlib.enums.AccountType;
import com.payriff.commonlib.enums.UserType;
import lombok.Data;

/**
 * @author HasanovZK
 */
@Data
public class UserRegisterDto {

    private String email;
    private String password;
    private String fullName;
    private String mobilePhone;
    private AccountType accountType; //cant see in sing up
    private String fin; //cant see in sing up
    private UserType userType;
}
