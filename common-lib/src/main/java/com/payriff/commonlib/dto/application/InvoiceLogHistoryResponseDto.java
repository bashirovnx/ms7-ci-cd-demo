package com.payriff.commonlib.dto.application;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InvoiceLogHistoryResponseDto {

    private LocalDate date;
    private List<Content> contents;

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Content {

        private LocalDate date;
        private LocalTime time;
        private String message;
        private String createdBy;
        private String lastModifiedBy;
        private Date lastModifiedDate;
        private String completedBy;
    }
}
