package com.payriff.commonlib.dto;

import lombok.Data;

/**
 * @author HasanovZK
 */
@Data
public class LsimResponseDto {

    private String successMessage;
    private String errorMessage;
    private int obj;//balance
    private int errorCode;
}
