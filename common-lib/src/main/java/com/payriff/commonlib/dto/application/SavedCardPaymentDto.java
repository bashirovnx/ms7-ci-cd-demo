package com.payriff.commonlib.dto.application;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author HasanovZK
 */
@Data
public class SavedCardPaymentDto {
    private String description;
    private BigDecimal amount;
    private String cardUuid;
    private String orderId;
    private String sessionId;
}
