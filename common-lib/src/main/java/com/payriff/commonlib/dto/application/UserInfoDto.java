package com.payriff.commonlib.dto.application;

import com.payriff.commonlib.enums.UserType;
import lombok.Getter;
import lombok.Setter;

import javax.management.relation.Role;
import java.util.Collection;

/** @author HasanovZK */
@Setter
@Getter
public class UserInfoDto {
  private Long id;
  private String username;
  private String email;
  private String fullName;
  private String mobilePhone;
  private UserType userType;
  private String fin;
  private boolean active;
  private String accessToken;
  private boolean attachedCard;
  private Collection<Role> roles;

}
