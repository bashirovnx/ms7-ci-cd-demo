package com.payriff.commonlib.dto.admin;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author HasanovZK
 */
@Data
public class TariffDao {
    private BigDecimal commissionRate;
    private BigDecimal payRiffCommission;
}
