package com.payriff.commonlib.dto.application;

import lombok.Data;

/**
 * @author HasanovZK
 */
@Data
public class StandardGatewayRequest<T> {
    private String merchant;
    private String encryptionToken;
    private T body;
}
