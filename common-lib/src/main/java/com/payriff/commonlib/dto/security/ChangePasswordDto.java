package com.payriff.commonlib.dto.security;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author HasanovZK
 */
@Getter
@Setter
@ToString
public class ChangePasswordDto {

    private Long userId;

    private String oldPassword;
    private String newPassword;
    private String confirmPassword;

}
