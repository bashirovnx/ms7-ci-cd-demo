package com.payriff.commonlib.dto;

import com.payriff.commonlib.enums.StatusType;
import lombok.Data;

/**
 * @author Kanan
 */
@Data
public class HotelDto {
    private Long id;
    private String name;
    private StatusType status;
}
