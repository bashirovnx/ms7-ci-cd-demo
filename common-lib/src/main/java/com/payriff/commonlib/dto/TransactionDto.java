package com.payriff.commonlib.dto;

import com.payriff.commonlib.enums.CurrencyType;
import com.payriff.commonlib.enums.PaymentSource;
import com.payriff.commonlib.enums.PaymentStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TransactionDto {

  private Long applicationId;
  private BigDecimal amount;
  private BigDecimal paidAmount;
  private CurrencyType currencyType;
  private String description;
  private PaymentSource paymentSource;
  private PaymentStatus paymentStatus;

}
