package com.payriff.commonlib.dto.application;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ViewContractRequestDto {

    private String accountType;
    private String fin;
    private String address;
    private String fullName;

}
