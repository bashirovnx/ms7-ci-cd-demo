package com.payriff.commonlib.dto.application;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author HasanovZK
 */

@Data
public class PaymentResultDto {
    private String date;
    private String version;
    private Long orderID;
    private String sessionId;
    private String transactionType;
    private String RRN;
    private String PAN;
    private BigDecimal purchaseAmount;
    private String currency;
    private String tranDateTime;
    private String responseCode;
    private String responseDescription;
    private String brand;
    private String orderStatus;
    private String approvalCode;
    private String acqFee;
    private String orderDescription;
    private String approvalCodeScr;
    private BigDecimal purchaseAmountScr;
    private String currencyScr;
    private String orderStatusScr;
    private String CardHolderName;
    private CardRegistration cardRegistration;

    @Data
    public static class CardRegistration {
        private String MaskedPAN;
        private String CardUID;
        private String Brand;
    }
}


