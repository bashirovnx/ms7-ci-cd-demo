package com.payriff.commonlib.dto.application;

import lombok.Data;
import lombok.ToString;

/**
 * @author HasanovZK
 */
@Data
@ToString(callSuper = true)
public class GetOrderStatusDto {
    private String language;
    private String orderId;
    private String sessionId;
}
