package com.payriff.commonlib.dto.application;

import lombok.Data;

/**
 * @author HasanovZK
 */
@Data
public class CompanyDto {

    private String name;

    private String website;


    private String voen;

    private String address;

    private String description;

    private Long companyCategory;

    private String finCode; //create an annotation

}
