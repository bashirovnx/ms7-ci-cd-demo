package com.payriff.commonlib.dto.application;

import com.payriff.commonlib.enums.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

/** @author HasanovZK */
@Getter
@Setter
@ToString
public class InvoiceTemplatePublicDto {
  private String merchantId;
  private BigDecimal amount;
  private String fullName;
  private Long bookingId;
  private String email;
  private String customMessage;
  private LocalDateTime expireDate;
  // should be removed
  private String phoneNumber;
  private CurrencyType currencyType;
  private LanguageType languageType;
  private PaymentType paymentType;
  private SubscriptionState subscriptionState;
  private LocalDate paymentDay = LocalDate.now();
  private LocalTime paymentTime = LocalTime.now();
  private LocalDate expireDay;
  private WeekDay weekDay;
  private Integer dayOfMonth;
  private String description;
  private String approveURL;
  private String cancelURL;
  private String declineURL;
  private String uuid;
  private InstallmentProductType installmentProductType;
  private int installmentPeriod;
  private boolean directPay;
  private boolean sendSms = true;
}
