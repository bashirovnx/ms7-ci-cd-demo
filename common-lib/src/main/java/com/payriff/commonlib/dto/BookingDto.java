package com.payriff.commonlib.dto;


import com.payriff.commonlib.enums.CurrencyType;
import com.payriff.commonlib.enums.LanguageType;
import com.payriff.commonlib.enums.PreauthCompleteType;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

/** @author Kanan */
@Data
public class BookingDto {
  private Long id;
  private String merchant;
  // customer details
  private String firstName;
  private String lastName;
  private String email;
  private String phoneNumber;
  // payment details
  private Long bookingId;
  private BigDecimal amount;
  private CurrencyType currency;
  private String description;
  private String previewUrl;

  // payment settings
  private LanguageType language;
  private String hotelName;
  private LocalDateTime linkExpirationDate;
  private LocalDateTime checkInDate;
  private boolean directPay;
  private PreauthCompleteType completeType;
  private LocalDateTime completeDate;

  private String createdBy;
  private Date createdDate;
  private String lastModifiedBy;
  private Date lastModifiedDate;

  private String uuid;
  private String paymentUrl;
  private String customMessage;
  private String fileName;
  private String merchantLogo;

  private String currencyRateUsd;
}
