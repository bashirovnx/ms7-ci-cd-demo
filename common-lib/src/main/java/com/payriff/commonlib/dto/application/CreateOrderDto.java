package com.payriff.commonlib.dto.application;

import com.payriff.commonlib.enums.CurrencyType;
import com.payriff.commonlib.enums.InstallmentProductType;
import com.payriff.commonlib.enums.LanguageType;
import com.payriff.commonlib.enums.PaymentSource;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

/**
 * @author HasanovZK
 */
@Setter
@Getter
@ToString(callSuper = true)
public class CreateOrderDto {
    private BigDecimal amount;
    private LanguageType language = LanguageType.EN;
    private CurrencyType currencyType = CurrencyType.AZN;
    private String description;
    private String approveURL;
    private String cancelURL;
    private String declineURL;
    private String cardUuid;
    private InstallmentProductType installmentProductType;
    private Integer installmentPeriod;
    private String uuid;
    private String baseUrl;
    private PaymentSource paymentSource;
    private Boolean directPay;
    private String senderCardUID;
    private Boolean cardStorage = Boolean.TRUE;
}
