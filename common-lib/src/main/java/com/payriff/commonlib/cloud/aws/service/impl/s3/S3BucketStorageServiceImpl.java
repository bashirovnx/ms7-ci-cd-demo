package com.payriff.commonlib.cloud.aws.service.impl.s3;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.util.IOUtils;
import com.payriff.commonlib.cloud.aws.service.interfaces.s3.S3BucketStorageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Slf4j
@Service
@RequiredArgsConstructor
public class S3BucketStorageServiceImpl implements S3BucketStorageService {

    private final AmazonS3 amazonS3Client;

    @Value("${cloud.aws.bucket.name}")
    private String bucketName;

    @Override
    public byte[] downloadFile(String keyName) {
        GetObjectRequest getObjectRequest = new GetObjectRequest(bucketName, keyName);

        S3Object s3Object = amazonS3Client.getObject(getObjectRequest);
        log.info("S3 object {} have been obtained", s3Object);

        S3ObjectInputStream objectContent = s3Object.getObjectContent();

        try {
            log.info("File {} has been downloaded", s3Object.getKey());
            return IOUtils.toByteArray(objectContent);
        } catch (IOException e) {
            log.error("Cannot download file {}", s3Object.getKey());
            throw new IllegalArgumentException();
        }
    }
}
