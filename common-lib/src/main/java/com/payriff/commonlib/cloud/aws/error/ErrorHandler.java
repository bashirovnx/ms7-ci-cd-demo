//package com.payriff.commonlib.cloud.aws.error;
//
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.ExceptionHandler;
//import org.springframework.web.bind.annotation.ResponseStatus;
//import org.springframework.web.bind.annotation.RestControllerAdvice;
//import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
//
//@RestControllerAdvice
//@Slf4j
//public class ErrorHandler extends ResponseEntityExceptionHandler {
//
//    @ResponseStatus(HttpStatus.BAD_REQUEST)
//    @ExceptionHandler({AwsException.class})
//    public ResponseEntity<AwsResult> handleServiceException(AwsException ex) {
//
//        return ResponseEntity.badRequest()
//                .body(AwsResult.builder()
//                        .status(AwsResult.AwsStatus.ERROR)
//                        .code(ex.getCode().getCode())
//                        .message(ex.getMessage())
//                        .build());
//    }
//
//}