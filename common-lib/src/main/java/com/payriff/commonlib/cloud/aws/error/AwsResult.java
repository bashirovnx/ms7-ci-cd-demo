package com.payriff.commonlib.cloud.aws.error;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AwsResult {

    private AwsStatus status;
    private String code;
    private String message;

    public enum AwsStatus {
        SUCCESS,
        ERROR
    }

}