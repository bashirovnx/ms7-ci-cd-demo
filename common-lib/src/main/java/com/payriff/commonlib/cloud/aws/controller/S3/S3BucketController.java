//package com.payriff.commonlib.cloud.aws.controller.S3;
//
//import com.payriff.commonlib.cloud.aws.service.interfaces.s3.S3BucketStorageService;
//import lombok.RequiredArgsConstructor;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//
//import static org.apache.http.HttpHeaders.CONTENT_TYPE;
//import static org.springframework.http.HttpHeaders.CONTENT_DISPOSITION;
//import static org.springframework.http.MediaType.APPLICATION_OCTET_STREAM_VALUE;
//
//@Slf4j
//@RestController
//@RequestMapping("api/v1/storage")
//@RequiredArgsConstructor
//public class S3BucketController {
//    private final S3BucketStorageService storageService;
//
//    @GetMapping("/download")
//    public ResponseEntity<byte[]> downloadFile(
//            @RequestParam("file") final String keyName
//    ) {
//        byte[] file = storageService.downloadFile(keyName);
//
//        return ResponseEntity.ok()
//                .contentLength(file.length)
//                .header(CONTENT_TYPE, APPLICATION_OCTET_STREAM_VALUE)
//                .header(CONTENT_DISPOSITION, "attachment; filename=\"" + keyName + "\"")
//                .body(file);
//    }
//}
