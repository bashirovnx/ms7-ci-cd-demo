package com.payriff.commonlib.cloud.aws.error;


import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class AwsException extends RuntimeException {

    private AwsExceptionCode code;

    public AwsException(AwsExceptionCode code, String message) {
        super(message);
        this.code = code;
    }

    public enum AwsExceptionCode {

        INVOICE_NOT_FOUND("01001"),
        INVOICE_ALREDAY_COMPLETED("01002"),
        INVOICE_EXPIRED("01003"),

        INVOICE_TYPE_INCORRECT("02001"),
        INVOICE_IS_NOT_PENDING("02002"),
        TRANSACTION_NOT_FOUND("02003"),
        PENDING_TRANSACTION_NOT_FOUND("02004"),
        CARD_NOT_FOUND_FOR_TRANSFER("02005"),
        TRANSACTION_NOT_CREATED("02006");

        String code;

        AwsExceptionCode(String code) {
            this.code = code;
        }

        public String getCode() {
            return code;
        }

    }

}
