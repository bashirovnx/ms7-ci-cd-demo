package com.payriff.commonlib.cloud.aws.service.interfaces.s3;

public interface S3BucketStorageService {

    byte[] downloadFile(String keyName);
}
