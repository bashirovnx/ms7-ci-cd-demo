package com.payriff.commonlib.enums;

/**
 * @author HasanovZK
 */
public enum LanguageType {
    AZ,
    EN,
    RU
}
