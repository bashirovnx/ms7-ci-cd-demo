package com.payriff.commonlib.enums;

/**
 * @author HasanovZK
 */
public enum InstallmentProductType {
    BIRKART,
    ALLBALI,
    BOLKART,
}
