package com.payriff.commonlib.enums;

public enum InvoiceSource {
    B2B,
    DASHBOARD,
    PMD,
    TERMINAL
}
