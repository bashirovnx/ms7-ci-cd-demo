package com.payriff.commonlib.enums;

import java.time.Month;
import java.util.Arrays;

public enum MonthType {

    January(1),
    February(2),
    March(3),
    April(4),
    May(5),
    June(6),
    July(7),
    August(8),
    September(9),
    October(10),
    November(11),
    December(12);

    private int value;

    MonthType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static MonthType getMonthType(int month) {
        return Arrays.stream(MonthType.values())
                     .filter(monthType -> monthType.getValue() == month)
                     .findFirst()
                     .orElseThrow(IllegalArgumentException::new);
    }

    public static String of(int month) {
        return Arrays.stream(MonthType.values())
                     .filter(monthType -> monthType.getValue() == month)
                     .findFirst()
                     .orElseThrow(IllegalArgumentException::new)
                     .name();
    }

    public static String of(Month month) {
        return Arrays.stream(MonthType.values())
                     .filter(monthType -> monthType.getValue() == month.getValue())
                     .findFirst()
                     .orElseThrow(IllegalArgumentException::new)
                     .name();
    }

}
