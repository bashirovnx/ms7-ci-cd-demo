package com.payriff.commonlib.enums;

/** @author HasanovZK */
public enum PaymentSource {
  NORMAL,
  INVOICE,
  TRANSFER,
  INSTALLMENT,
  QR
}
