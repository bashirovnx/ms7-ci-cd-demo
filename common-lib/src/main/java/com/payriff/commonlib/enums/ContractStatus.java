package com.payriff.commonlib.enums;

public enum ContractStatus {

    SIGNED,
    UNSIGNED

}
