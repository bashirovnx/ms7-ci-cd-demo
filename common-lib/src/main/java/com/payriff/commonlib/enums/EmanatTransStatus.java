package com.payriff.commonlib.enums;

public enum EmanatTransStatus {

    PENDING,
    ERROR,
    EXPIRED,
    PARTIAL,
    COMPLETE

}
