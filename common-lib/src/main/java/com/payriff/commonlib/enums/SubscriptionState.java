package com.payriff.commonlib.enums;

/**
 * @author HasanovZK
 */
public enum SubscriptionState {
    ACTIVE,
    PAUSE,
}
