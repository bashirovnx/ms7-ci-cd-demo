package com.payriff.commonlib.enums;

public enum InvoiceStatus {

    PENDING,
    ERROR,
    EXPIRED,
    PARTIAL,
    COMPLETE

}
