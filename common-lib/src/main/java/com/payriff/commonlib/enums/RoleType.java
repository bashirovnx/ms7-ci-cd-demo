package com.payriff.commonlib.enums;

public enum RoleType {
  B2B,
  PMD,
  STANDARD
}
