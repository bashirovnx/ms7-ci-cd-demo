package com.payriff.commonlib.enums;

/**
 * @author HasanovZK
 */
public enum PaymentType {
    ONETIME,
    DAILY,
    WEEKLY,
    MONTHLY,
    ANNUALLY
}
