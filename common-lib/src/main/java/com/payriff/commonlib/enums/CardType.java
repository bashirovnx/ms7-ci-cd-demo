package com.payriff.commonlib.enums;

/**
 * @author HasanovZK
 */
public enum CardType {
    KAPITALBANK,
    IBAR,
    OTHER,
    UNKNOWN;

    public static CardType of(String maskedPan) {
        if (maskedPan == null)
            return CardType.UNKNOWN;
        if (isKBCard(maskedPan))
            return CardType.KAPITALBANK;
        return CardType.OTHER;
    }

    public static boolean isKBCard(String card) {

        String[] kapitalCards = {"415402", "415402", "41697377", "416974",
                                 "416975", "417211", "417358", "427276",
                                 "510307", "510880", "522121", "523915",
                                 "52391515", "535115", "53511501", "540408",
                                 "544459", "545184", "676867"};

        for (String valid : kapitalCards) {
            if (card.startsWith(valid))
                return true;
        }

        return false;
    }

}
