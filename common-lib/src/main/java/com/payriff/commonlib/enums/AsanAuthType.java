package com.payriff.commonlib.enums;

public enum AsanAuthType {

    ERROR,
    INPROCESS,
    EXPIRED,
    COMPLETE

}
