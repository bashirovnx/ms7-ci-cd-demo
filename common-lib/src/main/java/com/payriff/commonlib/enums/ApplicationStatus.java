package com.payriff.commonlib.enums;

import java.util.Arrays;

/**
 * @author HasanovZK
 */
public enum ApplicationStatus {
    TEST(0),
    REVIEW(1),
    PROD(2),
    REJECT(3),
    UNKNOWN(4);
    int id;

    ApplicationStatus(int id) {
        this.id = id;
    }

    public static ApplicationStatus of(int id) {
        return Arrays.stream(ApplicationStatus.values())
                     .filter(status -> status.id == id)
                     .findFirst()
                     .orElse(ApplicationStatus.UNKNOWN);
    }

    public int getId() {
        return id;
    }
}
