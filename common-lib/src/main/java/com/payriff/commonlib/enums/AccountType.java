package com.payriff.commonlib.enums;

/** @author HasanovZK */
public enum AccountType {
  INDIVIDUAL,
  BUSINESS,
  OWNERSHIP;

  public static AccountType getAccountType(String input) {
    String personal = "personal", business = "business";

    if (personal.equalsIgnoreCase(input)) {
      return AccountType.INDIVIDUAL;
    } else if (business.equalsIgnoreCase(input)) {
      return AccountType.BUSINESS;
    } else {
      throw new IllegalArgumentException("Invalid account type = " + input);
    }
  }
}
