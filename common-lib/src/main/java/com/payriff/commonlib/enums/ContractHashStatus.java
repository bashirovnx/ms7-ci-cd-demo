package com.payriff.commonlib.enums;

public enum ContractHashStatus {

    INPROCESS,
    COMPLETE

}
