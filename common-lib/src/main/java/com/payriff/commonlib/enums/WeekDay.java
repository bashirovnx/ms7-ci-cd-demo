package com.payriff.commonlib.enums;

import java.util.Arrays;

public enum WeekDay {

    Monday(1),
    Tuesday(2),
    Wednesday(3),
    Thursday(4),
    Friday(5),
    Saturday(6),
    Sunday(7);

    private int value;

    WeekDay(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static WeekDay getWeekday(int week) {
        return Arrays.stream(WeekDay.values())
                     .filter(weekDay -> weekDay.getValue() == week)
                     .findFirst()
                     .orElseThrow(IllegalArgumentException::new);
    }

    public static String of(int week) {
        return Arrays.stream(WeekDay.values())
                     .filter(weekDay -> weekDay.getValue() == week)
                     .findFirst()
                     .orElseThrow(IllegalArgumentException::new)
                     .name();
    }

}
