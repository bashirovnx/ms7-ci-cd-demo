package com.payriff.commonlib.enums;

/**
 * @author Kanan
 */
public enum StatusType {
    ACTIVE, INACTIVE
}
