package com.payriff.commonlib.enums;

public enum CommonStatus {

    CREATED,
    PENDING,
    ERROR,
    EXPIRED,
    COMPLETE

}
