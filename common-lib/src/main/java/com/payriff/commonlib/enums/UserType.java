package com.payriff.commonlib.enums;

public enum UserType {
  PMD_USER,
  DASHBOARD_USER
}
