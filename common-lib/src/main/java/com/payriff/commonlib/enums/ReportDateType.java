package com.payriff.commonlib.enums;

import java.util.Arrays;

public enum ReportDateType {

    TODAY("today"),
    WEEK("week"),
    MONTH("month");

    private String value;

    ReportDateType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static ReportDateType of(String value) {
        return Arrays.stream(ReportDateType.values())
                .filter(x -> x.getValue().equals(value))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }

}
