package com.payriff.commonlib.enums;

/**
 * @author HasanovZK
 */
public enum GatewayType {
    KAPITAL_BANK,
    AZERI_CARD,
    MILLI_CARD
}
