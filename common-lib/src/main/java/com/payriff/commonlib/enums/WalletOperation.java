package com.payriff.commonlib.enums;

/**
 * @author HasanovZK
 */
public enum WalletOperation {
    IN,
    OUT
}
