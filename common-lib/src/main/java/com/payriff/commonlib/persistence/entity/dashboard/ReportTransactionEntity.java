package com.payriff.commonlib.persistence.entity.dashboard;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.time.LocalDate;

@Data
@Builder
@ToString
public class ReportTransactionEntity {

    private LocalDate date;
    private String count;
    private String totalAmount;

}
