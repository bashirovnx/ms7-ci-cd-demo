package com.payriff.commonlib.persistence.entity.meta;

import com.payriff.commonlib.enums.PaymentStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @author HasanovZK
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransactionMetaDataEntity {
    private int count;
    private BigDecimal sum=BigDecimal.ONE;
    private PaymentStatus paymentStatus;

}
