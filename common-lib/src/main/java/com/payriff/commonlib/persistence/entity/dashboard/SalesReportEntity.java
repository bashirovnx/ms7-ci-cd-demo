package com.payriff.commonlib.persistence.entity.dashboard;

import com.payriff.commonlib.enums.ReportDateType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SalesReportEntity {

    private ReportDateType type;
    private BigDecimal count;

}
