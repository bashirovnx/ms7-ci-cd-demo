package com.payriff.commonlib.persistence.entity;

import lombok.Data;

import javax.persistence.Embeddable;

/**
 * @author Kanan
 */
@Embeddable
@Data
public class CustomerEntity {
    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;
}
