package com.payriff.commonlib.persistence.entity.security;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * @author HasanovZK
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "convert_check")
public class ConvertCheckEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "converter_generator")
    @SequenceGenerator(name = "converter_generator", sequenceName = "converter_seq", allocationSize = 1)
    private long id;
    private Long orderId;
    private String uuid;
    private boolean active;
}
