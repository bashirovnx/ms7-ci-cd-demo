package com.payriff.commonlib.persistence.entity.processing;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * @author HasanovZK
 */
@Entity
@Setter
@Getter
@EqualsAndHashCode(callSuper = false)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@NoArgsConstructor
@Table(name = "company_category")
public class CompanyCategoryEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "company_category_generator")
    @SequenceGenerator(name = "company_category_generator", sequenceName = "company_category_seq", allocationSize = 1,
            initialValue = 1090099)
    private Long id;
    private String name;
    private boolean active;

}
