package com.payriff.commonlib.persistence.repository;

import com.payriff.commonlib.persistence.entity.application.UserAppContractEntity;
import com.payriff.commonlib.persistence.entity.application.UserEntity;
import com.payriff.commonlib.persistence.entity.processing.ApplicationEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserAppContractRepository extends JpaRepository<UserAppContractEntity, Long> {

    Optional<UserAppContractEntity> findByUserEntityAndApplicationEntityAndAccountTypeAndStatus(
            UserEntity user, ApplicationEntity application, String accountType, String status);

    Optional<UserAppContractEntity> findByUserEntityAndAccountTypeAndStatus(
            UserEntity user, String accountType, String status);

    void deleteByUserEntityAndAccountTypeAndStatus(UserEntity userEntity, String account, String status);

    void deleteByUserEntityAndApplicationEntityAndAccountTypeAndStatus(UserEntity user, ApplicationEntity application,
                                                                       String account, String status);

}
