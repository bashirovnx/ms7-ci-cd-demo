package com.payriff.commonlib.persistence.repository;

import com.payriff.commonlib.persistence.entity.HotelEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Kanan
 */
@Repository
public interface HotelRepository extends JpaRepository<HotelEntity,Long> {
}
