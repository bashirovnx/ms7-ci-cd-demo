package com.payriff.commonlib.persistence.entity.dashboard;

import com.payriff.commonlib.enums.PaymentStatus;
import com.payriff.commonlib.persistence.entity.application.UserEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "statistics")
public class StatisticsEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "statistics_generator")
    @SequenceGenerator(name = "statistics_generator", sequenceName = "statistics_seq", allocationSize = 1)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private UserEntity userEntity;

    @Column(name = "payment_status")
    private PaymentStatus paymentStatus;

    @Column(name = "transaction_count")
    private Long transactionCount;

    @Column(name = "statistic_date")
    private LocalDate statisticDate;

    @Column(name = "update_date")
    private LocalDate updateDate;

    @Column(name = "is_min")
    private Boolean isMin;

    @Column(name = "is_max")
    private Boolean isMax;

}
