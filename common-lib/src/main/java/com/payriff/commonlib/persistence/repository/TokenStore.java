package com.payriff.commonlib.persistence.repository;

import com.payriff.commonlib.persistence.entity.security.TokenEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/** @author HasanovZK */
@Repository
public interface TokenStore extends JpaRepository<TokenEntity, Long> {
  @Procedure(procedureName = "check_token")
  boolean checkToken(@Param("val") String token);

  @Transactional
  @Modifying
  @Query(value = "UPDATE token set active=false where user_id= :userId", nativeQuery = true)
  void deactivateOldTokens(@Param("userId") Long userId);

  Optional<List<TokenEntity>> findByAccessToken(String token);
}
