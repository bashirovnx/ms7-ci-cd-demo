package com.payriff.commonlib.persistence.repository;

import com.payriff.commonlib.persistence.entity.processing.MerchantEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author HasanovZK
 */
@Repository
public interface MerchantRepository extends JpaRepository<MerchantEntity, Long> {
}
