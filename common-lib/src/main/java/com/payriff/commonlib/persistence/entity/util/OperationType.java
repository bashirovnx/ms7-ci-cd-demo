package com.payriff.commonlib.persistence.entity.util;

import java.util.Arrays;

/**
 * @author HasanovZK
 */
public enum OperationType {
    REGISTRATION(0),
    LOGIN(1),
    FORGET_PASSWORD(2),
    CREATE_APP(3),
    DELETE_APP(4),
    SUBMIT_PROD(5),
    REFUND(6),
    UPDATE_APP_KEY(7),
    APPROVE_EMAIL(8),
    CHANGE_PHONE_NUMBER(9),
    WITHDRAW(10);
    int id;

    OperationType(int id) {
        this.id = id;
    }

    public static OperationType of(int id) {
        return Arrays.stream(OperationType.values())
                     .filter(status -> status.id == id)
                     .findFirst()
                     .orElse(null);
    }
}
