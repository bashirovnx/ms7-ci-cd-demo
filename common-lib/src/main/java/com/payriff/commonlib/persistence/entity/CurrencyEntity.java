package com.payriff.commonlib.persistence.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.payriff.commonlib.enums.StatusType;
import com.payriff.commonlib.persistence.entity.application.GlobalConfigEntity;
import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "currency")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CurrencyEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "currency")
    @SequenceGenerator(name = "currency", sequenceName = "currency_seq", allocationSize = 1)
    @Column(name = "id", nullable = false)
    private Long id;

    public CurrencyEntity(Long id) {
        this.id = id;
    }

    @Enumerated(EnumType.STRING)
    private StatusType status;

    private String name;
    private Integer code;
    private String shortName;

    @ManyToMany(mappedBy = "availableCurrencies")
    @JsonIgnore
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Set<GlobalConfigEntity> configs;
}
