package com.payriff.commonlib.persistence.entity.application;

import com.payriff.commonlib.persistence.entity.processing.TransactionEntity;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "invoice_trans_info")
@ToString
public class InvoiceTransInfoEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "invoice_trans_info_generator")
  @SequenceGenerator(
      name = "invoice_trans_info_generator",
      sequenceName = "invoice_trans_info_seq",
      allocationSize = 1)
  private Long id;

  @Column(name = "create_date")
  private LocalDateTime createDate;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "invoice_id", referencedColumnName = "id")
  private CustomerInvoiceEntity invoiceEntity;

  @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
  @JoinColumn(name = "transaction_id", referencedColumnName = "id")
  private TransactionEntity transactionEntity;

  public InvoiceTransInfoEntity(boolean nullSafe) {
    if (nullSafe) {
      CustomerInvoiceEntity customerInvoiceEntity = new CustomerInvoiceEntity();
      customerInvoiceEntity.setFullName("");
      this.invoiceEntity = customerInvoiceEntity;
    }
  }
}
