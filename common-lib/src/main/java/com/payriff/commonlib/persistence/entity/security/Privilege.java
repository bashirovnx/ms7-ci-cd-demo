package com.payriff.commonlib.persistence.entity.security;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.payriff.commonlib.persistence.entity.application.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Collection;

/** @author HasanovZK */
@Entity
@Data
@NoArgsConstructor
public class Privilege extends Auditable<String> {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "privilege_generator")
  @SequenceGenerator(
      name = "privilege_generator",
      sequenceName = "privilege_seq",
      allocationSize = 1)
  private Long id;

  private String name;

  public Privilege(String name) {
    this.name = name;
  }

  @ManyToMany(mappedBy = "privileges", cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.DETACH})
  @JsonIgnore
  private Collection<Role> roles = new java.util.ArrayList<>();
}
