package com.payriff.commonlib.persistence.entity.dashboard;

import com.payriff.commonlib.enums.PaymentStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MonthlyReportEntity {

    private Long tranCount;
    private LocalDate tranDate;
    private PaymentStatus paymentStatus;

}
