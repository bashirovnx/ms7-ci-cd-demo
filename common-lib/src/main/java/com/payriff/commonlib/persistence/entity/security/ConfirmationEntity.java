package com.payriff.commonlib.persistence.entity.security;


import com.payriff.commonlib.persistence.entity.application.Auditable;
import com.payriff.commonlib.persistence.entity.application.UserEntity;
import com.payriff.commonlib.persistence.entity.util.OperationType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * @author HasanovZK
 */
@Entity
@Setter
@Getter
@Table(name = "confirmation")
public class ConfirmationEntity extends Auditable<String> {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "forget_generator")
    @SequenceGenerator(name = "forget_generator", sequenceName = "forget_seq", allocationSize = 1)
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private UserEntity userEntity;
    private String guid;
    private boolean active;
    private OperationType operationType;
}
