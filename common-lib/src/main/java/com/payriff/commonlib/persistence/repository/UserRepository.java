package com.payriff.commonlib.persistence.repository;

import com.payriff.commonlib.enums.UserType;
import com.payriff.commonlib.persistence.entity.application.UserEntity;
import com.payriff.commonlib.persistence.entity.processing.ApplicationEntity;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/** @author HasanovZK */
@Repository
public interface UserRepository
    extends JpaRepository<UserEntity, Long>, JpaSpecificationExecutor<UserEntity> {
  Optional<UserEntity> findUserByEmail(String mail);

  Optional<UserEntity> findUserByEmailAndActiveIsTrue(String mail);

  Optional<List<UserEntity>> findAllByUserTypeAndActiveTrue(UserType userType, Sort sort);

  Optional<UserEntity> findUserByUsername(String username);

  Optional<UserEntity> findUserByUsernameAndActiveIsTrue(String username);

  Optional<UserEntity> findByApplicationEntitiesContains(ApplicationEntity applicationEntity);
}
