package com.payriff.commonlib.persistence.repository;

import com.payriff.commonlib.persistence.entity.security.ConfirmationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * @author HasanovZK
 */
@Repository
public interface ConfirmationEntityRepository extends JpaRepository<ConfirmationEntity, Long> {
    @Query(value = "select * from confirmation where active=true and guid= :guid", nativeQuery = true)
    Optional<ConfirmationEntity> getIsGuidActive(@Param("guid") String guid);

    @Transactional
    @Modifying
    @Query(value = "UPDATE confirmation set active=false where user_id= :userId and id=:id", nativeQuery = true)
    void deactivatePasswordForget(@Param("userId") Long userId, @Param("id") Long id);
}
