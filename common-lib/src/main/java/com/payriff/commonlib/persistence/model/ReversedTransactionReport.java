package com.payriff.commonlib.persistence.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;


@Data
@AllArgsConstructor
public class ReversedTransactionReport {

    private BigDecimal reversedAmountSum;
    private Long reversedCount;
}
