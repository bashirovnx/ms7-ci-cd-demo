package com.payriff.commonlib.persistence.entity;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.payriff.commonlib.enums.CurrencyType;
import com.payriff.commonlib.enums.LanguageType;
import com.payriff.commonlib.enums.PreauthCompleteType;
import com.payriff.commonlib.persistence.entity.application.Auditable;
import com.payriff.commonlib.persistence.entity.application.InvoiceTemplateEntity;
import com.payriff.commonlib.persistence.entity.application.UserEntity;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author Kanan
 */
@Entity
@Table(name = "bookings")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@Data
public class BookingEntity extends Auditable<String> {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "bookings_generator")
    @SequenceGenerator(name = "bookings_generator", sequenceName = "bookings_seq")
    private Long id;

    //customer details
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "firstName", column = @Column(name = "customer_first_name")),
            @AttributeOverride(name = "lastName", column = @Column(name = "customer_last_name")),
            @AttributeOverride(name = "email", column = @Column(name = "customer_email")),
            @AttributeOverride(name = "phoneNumber",
                    column = @Column(name = "customer_phone_number"))
    })
    private CustomerEntity customer;

    //payment details
    @Column(nullable = false)
    private Long bookingId;

    @Column(nullable = false)
    private BigDecimal amount;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private CurrencyType currency;

    private String description;
    private String customMessage;

    //upload your file

    //payment settings
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private LanguageType language;

    private LocalDateTime linkExpirationDate;

    private LocalDateTime checkInDate;

//    @ManyToOne(fetch = FetchType.EAGER)
//    @JoinColumn(nullable = false)
//    private HotelEntity hotel;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false)
    private UserEntity user;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn
    private InvoiceTemplateEntity invoice;

    //merchant
    private String merchant;

    // @Lob >> r=emember that i am not using it anymore to avoid the exception on the browser
    @Column(length = 16000000) // This should generate a medium blob
    @Basic(fetch = FetchType.LAZY) // I've read this is default, but anyway..
    private byte[] data;

    private String fileName;

    private String uuid;

    private String previewUrl;

    @Column(name = "direct_pay", columnDefinition = "false")
    private Boolean directPay;

    @Column(name = "complete_type")
    @Enumerated(EnumType.STRING)
    private PreauthCompleteType completeType;

    @Column(name = "complete_date")
    private LocalDateTime completeDate;

}
