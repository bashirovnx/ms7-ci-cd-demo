package com.payriff.commonlib.persistence.entity.application;

import com.payriff.commonlib.enums.CardType;
import com.payriff.commonlib.enums.GatewayType;
import com.payriff.commonlib.enums.InstallmentProductType;
import com.payriff.commonlib.enums.PaymentSource;
import com.payriff.commonlib.persistence.entity.MerchantCategoryEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * @author HasanovZK
 */
@Entity
@Setter
@Getter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Table(name = "tariff")
public class TariffEntity extends Auditable<String> {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tariff_generator")
    @SequenceGenerator(name = "tariff_generator", sequenceName = "tariff_seq", allocationSize = 1)
    private Long id;

    private BigDecimal percent;
    private BigDecimal foreignPercent;
    private int installmentPeriod;
    @Enumerated(EnumType.STRING)
    private InstallmentProductType productType;
    @Enumerated(EnumType.STRING)
    private GatewayType gatewayType;
    @Enumerated(EnumType.STRING)
    private PaymentSource paymentSource;
    @Enumerated(EnumType.STRING)
    private CardType cardType;
    private String merchant;
    private int version;
    private boolean active;
    private BigDecimal kbFee;
    private BigDecimal payRiffFee;
    @ManyToOne
    @JoinColumn(name = "merch_cat_id", referencedColumnName = "id")
    private MerchantCategoryEntity merchantCategory;
}
