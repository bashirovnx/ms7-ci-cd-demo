package com.payriff.commonlib.persistence.entity.application;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * @author Kanan
 */
@Entity
@Setter
@Getter
@Table(name = "user_notp")
@Where(clause = "active = true")
public class UserNotpEntity extends Auditable<String> {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_notp_generator")
    @SequenceGenerator(name = "user_notp_generator", sequenceName = "user_notp_seq", allocationSize = 1)
    private Long id;

    @OneToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private UserEntity user;

    @Column(nullable = false)
    private String otp;

    private boolean active = true;
}
