package com.payriff.commonlib.persistence.entity.processing;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author HasanovZK
 */
@Data
@AllArgsConstructor
public class Certification {
    private String certBody;
    private String certPrivateKey;
    private String certPublicKey;
}
