package com.payriff.commonlib.persistence.entity;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.payriff.commonlib.enums.StatusType;
import com.payriff.commonlib.persistence.entity.application.Auditable;
import lombok.Data;

import javax.persistence.*;

/**
 * @author Kanan
 */
@Entity
@Table(name = "hotels")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@Data
public class HotelEntity extends Auditable<String> {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "hotels_generator")
    @SequenceGenerator(name = "hotels_generator", sequenceName = "hotels_seq")
    private Long id;

    @Column(nullable = false, unique = true)
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private StatusType status = StatusType.ACTIVE;

//    @OneToMany(mappedBy = "hotel")
//    private Set<BookingEntity> bookings;
}
