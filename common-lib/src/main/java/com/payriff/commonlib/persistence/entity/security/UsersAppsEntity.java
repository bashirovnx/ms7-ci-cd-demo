package com.payriff.commonlib.persistence.entity.security;


import com.payriff.commonlib.enums.StatusType;
import com.payriff.commonlib.persistence.entity.application.Auditable;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;

/**
 * @author HasanovZK
 */
@Entity
@Data
@Table(name = "users_apps")
@ToString
public class UsersAppsEntity extends Auditable<String> {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "users_apps_generator")
    @SequenceGenerator(
            name = "users_apps_generator",
            sequenceName = "users_apps_seq",
            allocationSize = 1)
    private Long id;

    private Long ownerId;
    private Long subUserId;
    private Long appId;
    private String roleName;
    private Long roleId;

    @Enumerated(EnumType.STRING)
    // @Column(nullable = false)
    private StatusType status = StatusType.ACTIVE;
}
