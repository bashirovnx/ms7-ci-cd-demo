package com.payriff.commonlib.persistence.entity.util;

/**
 * @author HasanovZK
 */
public enum OperationState {
    SUCCESS,
    FAIL
}