package com.payriff.commonlib.persistence.repository;

import com.payriff.commonlib.persistence.entity.application.ContractFeeEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContractFeeRepository extends JpaRepository<ContractFeeEntity, Long> {
}
