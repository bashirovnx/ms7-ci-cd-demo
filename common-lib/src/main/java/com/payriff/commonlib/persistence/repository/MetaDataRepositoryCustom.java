package com.payriff.commonlib.persistence.repository;

import com.payriff.commonlib.enums.ApplicationStatus;
import com.payriff.commonlib.enums.PaymentStatus;
import com.payriff.commonlib.persistence.entity.InvoiceHistoryEntity;
import com.payriff.commonlib.persistence.entity.application.AppAndIncomeEntity;
import com.payriff.commonlib.persistence.entity.application.MailOrderMetaEntity;
import com.payriff.commonlib.persistence.entity.meta.TransactionMetaDataEntity;
import lombok.extern.log4j.Log4j;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * @author HasanovZK
 */
@Repository
@Slf4j
public class MetaDataRepositoryCustom {

    private final JdbcTemplate jdbcTemplate;

    public MetaDataRepositoryCustom(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public List<AppAndIncomeEntity> findAllByApp(Long userId) {
        return jdbcTemplate.query("select app.name, sum(t.amount) as total\n" +
                                  "                   from transaction t,\n" +
                                  "                        application app\n" +
                                  "                   where t.user_id=" + userId + " and\n" +
                                  "                         t.payment_source<>2 and\n" +
                                  "                         app.id=t.app_id and\n" +
                                  "                         t.amount is not null and\n" +
                                  "                         app.active=true and\n" +
                                  "                         app.status=" + ApplicationStatus.PROD.getId() + " and\n" +
                                  "                         t.payment_status=" + PaymentStatus.APPROVED.getId() + "\n" +
                                  "                   group by app.name",
                                  (rs, rowNum) -> new AppAndIncomeEntity(rs.getString("name"),
                                                                         rs.getBigDecimal("total")));
    }

    public List<MailOrderMetaEntity> getMailOrderMeta(Long userId, Long appId, Date fromDate, Date toDate) {

        String pattern = "MM.dd.yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String from = simpleDateFormat.format(fromDate);
        String to = simpleDateFormat.format(toDate);
        String sql = "select count(*), payment_type " +
                     "from mail_order " +
                     "where active=true and user_id = {userId} and created_date between '{from}' and '{to}' " +
                     "group by payment_type";

        if (Objects.nonNull(appId)) {
            sql = "select count(*), payment_type " +
                  "from mail_order " +
                  "where active=true and app_id = {appId} and user_id = {userId} and created_date between '{from}' and '{to}'  " +
                  "group by payment_type";
            sql = sql.replace("{appId}", String.valueOf(appId));
        }

        sql = sql.replace("{userId}", String.valueOf(userId))
                 .replace("{from}", from)
                 .replace("{to}", to);
        return jdbcTemplate.query(sql,
                                  (rs, rowNum) -> new MailOrderMetaEntity(rs.getInt("count"),
                                                                          rs.getString("payment_type")));
    }

    public void metaSales(Long id, Long appId) {

    }

    public List<TransactionMetaDataEntity> getTransactionMeta(Long userId, Long appId, Date fromDate, Date toDate) {

        String pattern = "MM.dd.yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String from = simpleDateFormat.format(fromDate);
        String to = simpleDateFormat.format(toDate);

        String sql
                = "SELECT count(*), sum(amount), payment_status FROM transaction where payment_status not in (0, 8, 9) and active=true and application_status=2 and user_id = {userId} and created_date between '{from}' and '{to}'  group by payment_status";

        if (Objects.nonNull(appId)) {
            sql
                    = "SELECT count(*), sum(amount), payment_status FROM transaction where payment_status not in (0, 8, 9) and active=true and application_status=2 and app_id = {appId} and user_id = {userId} and created_date between '{from}' and '{to}'  group by payment_status";
            sql = sql.replace("{appId}", String.valueOf(appId));
        }
        sql = sql.replace("{userId}", String.valueOf(userId))
                 .replace("{from}", from)
                 .replace("{to}", to);


        return jdbcTemplate.query(sql,
                                  (rs, rowNum) -> new TransactionMetaDataEntity(rs.getInt("count"),
                                                                                rs.getBigDecimal("sum"),
                                                                                PaymentStatus.of(rs.getInt(
                                                                                        "payment_status"))));
    }

    public List<InvoiceHistoryEntity> getInvoiceHistoryById(Long userId) {

      String sql = "SELECT mo.id as template_id,\n"
              + "          to_char(mo.created_date, 'yyyy-mm-dd HH24:MM:SS') as template_date,\n"
              + "          ci.id as invoice_id,\n"
              + "      ci.invoice_code,\n"
              + "      to_char(ci.created_date, 'yyyy-mm-dd HH24:MM:SS') as invoice_date,\n"
              + "      tr.id as transaction_id,\n"
              + "      tr.amount,\n"
              + "      tr.payment_status,\n"
              + "     to_char(tr.created_date, 'yyyy-mm-dd HH24:MM:SS') as transaction_date\n"
              + "     FROM public.mail_order  mo\n"
              + "FULL JOIN public.customer_invoice ci \n"
              + "       ON ci.invoice_template_id = mo.id\n"
              + "FULL JOIN public.invoice_trans_info  iti \n"
              + "       ON iti.invoice_id = ci.id\n"
              + "FULL JOIN public.transaction tr  \n"
              + "       on tr.id = iti.transaction_id\n"
              + "    WHERE mo.id = ?";

    return jdbcTemplate.query(sql, new Object[]{userId}, (rs, rowNum) -> InvoiceHistoryEntity.builder()
            .invoiceId(rs.getString("invoice_id"))
            .invoiceCode(rs.getString("invoice_code"))
            .invoiceCreatedDate(rs.getString("invoice_date"))
            .templateId(rs.getString("template_id"))
            .templateCreatedDate(rs.getString("template_date"))
            .transactionId(rs.getString("transaction_id"))
            .amount(rs.getString("amount"))
            .paymentStatus(rs.getString("payment_status"))
            .transactionDate(rs.getString("transaction_date"))
            .build());
    }

}


