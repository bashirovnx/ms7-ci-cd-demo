package com.payriff.commonlib.persistence.repository;

import com.payriff.commonlib.persistence.entity.application.UserEntity;
import com.payriff.commonlib.persistence.entity.dashboard.RecentActivityEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface RecentActivityRepository extends JpaRepository<RecentActivityEntity, Long> {

//    Page<RecentActivityEntity> getAllByUserEntityAndActionDateGreaterThanEqualOrderByActionDateDesc(
//            UserEntity userEntity, LocalDateTime dateTime, Pageable pageable);

    List<RecentActivityEntity> getAllByUserEntityAndActionDateGreaterThanEqualOrderByActionDateDesc(
            UserEntity userEntity, LocalDateTime dateTime);

}
