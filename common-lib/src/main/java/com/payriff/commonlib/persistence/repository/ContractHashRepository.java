package com.payriff.commonlib.persistence.repository;

import com.payriff.commonlib.persistence.entity.application.ContractHashEntity;
import com.payriff.commonlib.persistence.entity.application.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ContractHashRepository extends JpaRepository<ContractHashEntity, Long> {

    Optional<ContractHashEntity> findByUserEntityAndHashAndStatus(UserEntity userEntity, String hash, String status);

    void deleteByUserEntityAndStatus(UserEntity userEntity, String status);

}
