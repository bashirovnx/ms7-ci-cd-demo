package com.payriff.commonlib.persistence.entity.security;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.payriff.commonlib.persistence.entity.application.Auditable;
import com.payriff.commonlib.persistence.entity.application.UserEntity;
import com.payriff.commonlib.persistence.entity.util.OperationState;
import com.payriff.commonlib.persistence.entity.util.OperationType;
import lombok.*;

import javax.persistence.*;

/**
 * @author HasanovZK
 */
@Entity
@Setter
@Getter
@EqualsAndHashCode(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@Table(name = "otp_attempt")
public class OtpAttemptEntity extends Auditable<String> {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "otpAttempt_generator")
    @SequenceGenerator(name = "otpAttempt_generator", sequenceName = "otpAttempt_seq", allocationSize = 1)
    private Long id;
    private String phoneNumber;
    private String ip;
    private String userAgent;
    private boolean active;
    @Enumerated(EnumType.ORDINAL)
    private OperationState operationState;
    @Enumerated(EnumType.ORDINAL)
    private OperationType operationType;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    @JsonIgnore
    private UserEntity userEntity;

}
