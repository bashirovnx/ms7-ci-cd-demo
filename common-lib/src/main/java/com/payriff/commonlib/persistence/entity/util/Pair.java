package com.payriff.commonlib.persistence.entity.util;

import lombok.Data;

/**
 * @author HasanovZK
 */
@Data
public class Pair {
    private String name;
    private String value;
}
