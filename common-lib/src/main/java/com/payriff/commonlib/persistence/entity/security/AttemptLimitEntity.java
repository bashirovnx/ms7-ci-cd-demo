package com.payriff.commonlib.persistence.entity.security;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.payriff.commonlib.persistence.entity.application.Auditable;
import com.payriff.commonlib.persistence.entity.application.UserEntity;
import com.payriff.commonlib.persistence.entity.util.OperationState;
import com.payriff.commonlib.persistence.entity.util.OperationType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * @author HasanovZK
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "attempt_limit")
public class AttemptLimitEntity extends Auditable<String> {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "attempt_generator")
    @SequenceGenerator(name = "attempt_generator", sequenceName = "attempt_seq", allocationSize = 1)
    private Long id;
    @Enumerated(EnumType.STRING)
    private OperationType operationType;
    @Enumerated(EnumType.STRING)
    private OperationState operationState;
    private boolean active;
    private String ip;
    private String phoneNumber;
    private String userAgent;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    @JsonIgnore
    private UserEntity userEntity;

}
