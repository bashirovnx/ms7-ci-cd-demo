package com.payriff.commonlib.persistence.repository;

import com.payriff.commonlib.persistence.entity.application.BankAccountEntity;
import com.payriff.commonlib.persistence.entity.application.CompanyEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author HasanovZK
 */
@Repository
public interface BankAccountRepository extends JpaRepository<BankAccountEntity, Long> {
    Optional<BankAccountEntity> findByCompanyEntityAndActiveTrue(CompanyEntity companyEntity);

    @Query(value = "SELECT count(*) \n" +
                   "  FROM bank_account b  \n" +
                   "  JOIN company c       \n" +
                   "    ON c.id = b.company_id \n" +
                   " WHERE c.user_id = :userId",
           nativeQuery = true)
    int countBankAccountEntityByUserId(@Param("userId") Long userId);

    Optional<BankAccountEntity> findByCompanyEntityAndActive(CompanyEntity entity, boolean status);
}
