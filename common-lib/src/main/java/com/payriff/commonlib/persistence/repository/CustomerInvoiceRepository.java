package com.payriff.commonlib.persistence.repository;


import com.payriff.commonlib.enums.InvoiceStatus;
import com.payriff.commonlib.persistence.entity.application.CustomerInvoiceEntity;
import com.payriff.commonlib.persistence.entity.application.InvoiceTemplateEntity;
import com.payriff.commonlib.persistence.entity.application.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/** @author HasanovZK */
@Repository
public interface CustomerInvoiceRepository extends JpaRepository<CustomerInvoiceEntity, Long> {

  Optional<CustomerInvoiceEntity> findByInvoiceUuidAndActive(String invoiceUuid, boolean active);

  List<CustomerInvoiceEntity> findByInvoiceUuidAndActiveTrue(String invoiceUuid);
  List<CustomerInvoiceEntity> findByInvoiceUuid(String invoiceUuid);


  Optional<List<CustomerInvoiceEntity>> findByUuidAndActive(String invoiceUuid, boolean active);
  Optional<List<CustomerInvoiceEntity>> findByInvoiceUuidAndActiveTrueAndUserEntity(String invoiceUuid, UserEntity userEntity);

  List<CustomerInvoiceEntity> findByInvoiceCodeAndActiveAndInvoiceStatus(
      String invoiceCode, boolean active, InvoiceStatus invoiceStatus);

  List<CustomerInvoiceEntity> findByInvoiceCodeAndActive(String invoiceCode, boolean active);

  List<CustomerInvoiceEntity> findByInvoiceTemplateEntity(InvoiceTemplateEntity invoiceTemplateEntity);

}
