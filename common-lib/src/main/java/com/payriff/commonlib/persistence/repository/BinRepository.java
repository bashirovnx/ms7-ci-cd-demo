package com.payriff.commonlib.persistence.repository;


import com.payriff.commonlib.enums.CardType;
import com.payriff.commonlib.enums.GatewayType;
import com.payriff.commonlib.persistence.entity.application.BinEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author HasanovZK
 */
@Repository
public interface BinRepository extends JpaRepository<BinEntity, String> {

    List<BinEntity> findAllByActiveTrueAndGatewayTypeAndCardType(GatewayType gatewayType, CardType cardType);
}
