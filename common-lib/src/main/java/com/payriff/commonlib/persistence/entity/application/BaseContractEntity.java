package com.payriff.commonlib.persistence.entity.application;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "application_contract")
public class BaseContractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "application_contract_generator")
    @SequenceGenerator(name = "application_contract_generator", sequenceName = "application_contract_seq", allocationSize = 1)
    private Long id;

    private String textHtml;

    @Column(columnDefinition="TEXT")
    private String contract;

    private LocalDate updateDate;

    @Column(name = "account_type")
    private String accountType;

}
