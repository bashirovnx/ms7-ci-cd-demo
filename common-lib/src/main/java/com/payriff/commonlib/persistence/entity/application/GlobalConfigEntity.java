package com.payriff.commonlib.persistence.entity.application;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.payriff.commonlib.persistence.entity.CurrencyEntity;
import com.payriff.commonlib.persistence.entity.processing.ApplicationEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Set;

/** @author HasanovZK */
@Data
@Entity
@DynamicUpdate
@Table(name = "global_config")
@NoArgsConstructor
@ToString(onlyExplicitlyIncluded = true)
@EqualsAndHashCode(onlyExplicitlyIncluded = true, doNotUseGetters = true, callSuper = false)
public class GlobalConfigEntity {

  public GlobalConfigEntity(UserEntity userEntity) {
    this.userEntity = userEntity;
  }

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "global_config_gen")
  @SequenceGenerator(
      name = "global_config_gen",
      sequenceName = "global_conf_seq",
      allocationSize = 1)
  @ToString.Include
  @EqualsAndHashCode.Include
  private Long id;

  @ToString.Include @EqualsAndHashCode.Include private Boolean active = true;
  @ToString.Include private String defaultSmsTemplate;

  @ToString.Include
  @Column(columnDefinition = "TEXT")
  private String defaultMailTemplate;

  @ToString.Include private String applicationMail;

  @ColumnDefault("false")
  @ToString.Include
  @EqualsAndHashCode.Include
  private Boolean directPay = false; // ?

  @ColumnDefault("50")
  @ToString.Include
  @EqualsAndHashCode.Include
  private BigDecimal amountLimitForInternationalPhone;

  @OneToOne(mappedBy = "globalConfigEntity", cascade = CascadeType.ALL)
  @JoinColumn(name = "app_id")
  @JsonIgnore
  private ApplicationEntity applicationEntity;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "user_id")
  @JsonIgnore
  @EqualsAndHashCode.Exclude
  private UserEntity userEntity;

  @ManyToMany(fetch = FetchType.EAGER)
  @JoinTable(
      name = "currency_config",
      joinColumns = {@JoinColumn(name = "config_id")},
      inverseJoinColumns = {@JoinColumn(name = "currency_id")})
  private Set<CurrencyEntity> availableCurrencies;

  private byte[] logoPhoto;

  // @Lob >> r=emember that i am not using it anymore to avoid the exception on the browser
  @Column(length = 16000000) // This should generate a medium blob
  @Basic(fetch = FetchType.LAZY) // I've read this is default, but anyway...
  public byte[] getData() {
    return logoPhoto;
  }

  public void setData(byte[] logoPhoto) {
    this.logoPhoto = logoPhoto;
  }
}
