package com.payriff.commonlib.persistence.repository;

import com.payriff.commonlib.enums.ApplicationStatus;
import com.payriff.commonlib.enums.PaymentSource;
import com.payriff.commonlib.enums.PaymentStatus;
import com.payriff.commonlib.persistence.entity.application.UserEntity;
import com.payriff.commonlib.persistence.entity.processing.ApplicationEntity;
import com.payriff.commonlib.persistence.entity.processing.TransactionEntity;
import com.payriff.commonlib.persistence.model.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/** @author HasanovZK */
@Repository
public interface TransactionRepository
    extends JpaRepository<TransactionEntity, Long>, JpaSpecificationExecutor<TransactionEntity> {
  Optional<TransactionEntity> findByOrderId(Long orderId);

  Optional<TransactionEntity> findByIdAndPaymentStatusAndActiveTrue(
      Long id, PaymentStatus paymentStatus);

  Optional<List<TransactionEntity>> findByApplicationEntityAndPaymentStatus(
          ApplicationEntity applicationEntity, PaymentStatus paymentStatus);

  Optional<List<TransactionEntity>> findByUuid(String uuid);

  Optional<List<TransactionEntity>> findAllByUserEntityAndActive(
          UserEntity userEntity, boolean active);

  Optional<List<TransactionEntity>> findAllByUserEntityAndActiveAndPaymentStatusIsNot(
      UserEntity userEntity, boolean active, PaymentStatus paymentStatus);

  Optional<TransactionEntity> findByIdAndPaymentStatusAndActiveAndUserEntity(
      Long id, PaymentStatus paymentStatus, boolean active, UserEntity userEntity);

  Optional<TransactionEntity> findByIdAndPaymentStatusAndActive(
      Long id, PaymentStatus paymentStatus, boolean active);

  Optional<TransactionEntity> findByOrderIdAndSessionIdAndPaymentStatusAndActive(
      Long orderId, String sessionId, PaymentStatus paymentStatus, boolean active);

  Optional<TransactionEntity> findByOrderIdAndSessionIdAndApplicationEntityAndActiveTrue(
      Long orderId, String sessionId, ApplicationEntity applicationEntity);

  Optional<TransactionEntity>
      findByPaymentStatusAndActiveAndDescriptionAndUserEntityAndApplicationEntity(
          PaymentStatus paymentStatus,
          boolean active,
          String description,
          UserEntity userEntity,
          ApplicationEntity applicationEntity);

  Page<TransactionEntity> findAllByUserEntityAndApplicationEntityAndActiveAndPaymentStatusIn(
      UserEntity userEntity,
      ApplicationEntity applicationEntity,
      boolean active,
      List<PaymentStatus> paymentStatuses,
      Pageable pageable);

  Page<TransactionEntity>
      findAllByUserEntityAndApplicationEntityAndActiveAndPaymentStatusInAndLastModifiedDateBetweenAndApplicationStatusOrderByIdDesc(
          UserEntity userEntity,
          ApplicationEntity applicationEntity,
          boolean active,
          List<PaymentStatus> paymentStatuses,
          Pageable pageable,
          Date from,
          Date to,
          ApplicationStatus applicationStatus);

  Page<TransactionEntity>
      findAllByApplicationEntityAndActiveAndPaymentStatusInAndLastModifiedDateBetweenAndApplicationStatusOrderByIdDesc(
          ApplicationEntity applicationEntity,
          boolean active,
          List<PaymentStatus> paymentStatuses,
          Pageable pageable,
          Date from,
          Date to,
          ApplicationStatus applicationStatus);

  Page<TransactionEntity> findAllByUserEntityAndPaymentStatusInAndActive(
      UserEntity userEntity,
      List<PaymentStatus> paymentStatuses,
      boolean active,
      Pageable pageable);

  Page<TransactionEntity>
      findAllByUserEntityAndPaymentStatusInAndActiveAndCreatedDateBetweenOrderByIdDesc(
          UserEntity userEntity,
          List<PaymentStatus> paymentStatuses,
          boolean active,
          Pageable pageable,
          Date from,
          Date to);

  Page<TransactionEntity>
      findAllByUserEntityIsInAndPaymentStatusInAndActiveAndCreatedDateBetweenOrderByIdDesc(
          Set<UserEntity> userEntity,
          List<PaymentStatus> paymentStatuses,
          boolean active,
          Pageable pageable,
          Date from,
          Date to);

  Page<TransactionEntity>
      findAllByUserEntityAndPaymentSourceAndPaymentStatusInAndActiveAndCreatedDateBetweenOrderByIdDesc(
          UserEntity userEntity,
          PaymentSource paymentSource,
          List<PaymentStatus> paymentStatuses,
          boolean active,
          Pageable pageable,
          Date from,
          Date to);

  @Query(
      value =
          "select * \n"
              + "from transaction t\n"
              + "where t.application_status = 2\n"
              + "  and t.payment_status = 1\n"
              + "  and t.payment_source != 2\n"
              + "  and t.created_date::::date = (NOW() - INTERVAL '2 DAY')::::date\n"
              + "  and t.active = true",
      nativeQuery = true)
  List<TransactionEntity> getTransactionOn14DayCompleted();

  @Query(
      value =
          "select *\n"
              + "from transaction t\n"
              + "where t.application_status = 2\n"
              + "  and t.payment_status = 0\n"
              + "  and t.payment_source != 2\n"
              + "  and t.created_date > now()::::timestamp - INTERVAL '30 minutes'\n"
              + "  and t.created_date < now()::::timestamp - INTERVAL '10 minutes'\n"
              + "  and t.active = true",
      nativeQuery = true)
  List<TransactionEntity> getTransactionsToCheck();

  @Query( "SELECT DISTINCT new com.digipay.pysys.edd.reporting.model.ReportingInformation(" +
          "t.applicationEntity.id, t.userEntity.id, a.name, a.walletEntity.id) " +
          "FROM TransactionEntity AS t JOIN ApplicationEntity AS a ON a.id=t.applicationEntity.id" +
          " WHERE t.createdDate BETWEEN :from AND :to  AND t.applicationStatus=" +
          "com.digipay.pysys.model.enums.ApplicationStatus.PROD AND t.paymentStatus=" +
          "com.digipay.pysys.model.enums.PaymentStatus.APPROVED"
  )
  List<ReportingInformation> getAppListWithSuccessfulTransactions(
          Date from, Date to
  );


  @Query( "SELECT new com.digipay.pysys.edd.reporting.model.SuccessfulOnUsTransactionReport(" +
          "sum(t.amount), sum(t.paidAmount), count(t.amount)) " +
          "FROM TransactionEntity AS t WHERE t.createdDate BETWEEN :from AND :to  AND t.applicationEntity.id = :appId" +
          " AND t.cardType=com.digipay.pysys.model.enums.CardType.KAPITALBANK"
  )
  List<SuccessfulOnUsTransactionReport> getSuccessfulOnUsTransactions(
          Date from, Date to, Long appId
  );


  @Query( "SELECT new com.digipay.pysys.edd.reporting.model.SuccessfulOffUsTransactionReport(" +
          "sum(t.amount), sum(t.paidAmount), count(t.amount)) " +
          "FROM TransactionEntity AS t WHERE t.createdDate BETWEEN :from AND :to  AND t.applicationEntity.id = :appId" +
          " AND t.cardType IN (com.digipay.pysys.model.enums.CardType.IBAR," +
          "com.digipay.pysys.model.enums.CardType.OTHER," +
          "com.digipay.pysys.model.enums.CardType.UNKNOWN)"
  )
  List<SuccessfulOffUsTransactionReport> getSuccessfulOffUsTransactions(
          Date from, Date to, Long appId
  );

  @Query( "SELECT new com.digipay.pysys.edd.reporting.model.ReversedTransactionReport(" +
          "sum(t.amount), count(t.amount)) " +
          "FROM TransactionEntity AS t WHERE t.createdDate BETWEEN :from AND :to  " +
          "AND t.applicationEntity.id = :appId AND t.paymentStatus = " +
          "com.digipay.pysys.model.enums.PaymentStatus.REVERSE"
  )
  List<ReversedTransactionReport> getReversedTransactions(
          Date from, Date to, Long appId
  );

  @Query( "SELECT new com.digipay.pysys.edd.reporting.model.RefundedTransactionReport(" +
          "sum(rl.refundAmount), count(t.amount)) " +
          "FROM TransactionEntity AS t JOIN RefundableLimitEntity rl ON t.id=rl.transactionEntity.id " +
          "WHERE t.createdDate BETWEEN :from AND :to  " +
          "AND t.applicationEntity.id = :appId AND t.paymentStatus IN ("+
          "com.digipay.pysys.model.enums.PaymentStatus.PARTIAL_REFUND," +
          "com.digipay.pysys.model.enums.PaymentStatus.REFUNDED)"
  )
  List<RefundedTransactionReport> getRefundedTransactions(
          Date from, Date to, Long appId
  );


  Optional<TransactionEntity> findByIdAndActiveTrue(long id);
}
