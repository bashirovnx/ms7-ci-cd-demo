package com.payriff.commonlib.persistence.entity.application;

import com.payriff.commonlib.enums.CommonStatus;
import com.payriff.commonlib.persistence.entity.processing.TransactionEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@DynamicUpdate
@Table(name = "scheduled_transaction")
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ScheduledTransactionEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "scheduled_transaction_generator")
    @SequenceGenerator(
            name = "scheduled_transaction_generator",
            sequenceName = "scheduled_transaction_seq",
            allocationSize = 1
    )
    Long id;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "transaction_id")
    private TransactionEntity transactionId;

    private LocalDateTime scheduledTime;

    private LocalDateTime executedTime;

    @Enumerated(EnumType.STRING)
    private CommonStatus status;

    private Integer attempt;

    @CreatedDate
    @Column(name = "created_date")
    private LocalDateTime createdDate;

}
