package com.payriff.commonlib.persistence.repository;

import com.payriff.commonlib.persistence.entity.processing.AccountToCardEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountToCardEntityRepository extends JpaRepository<AccountToCardEntity, Long> {
}