package com.payriff.commonlib.persistence.entity.application;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.payriff.commonlib.persistence.entity.processing.ApplicationEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * @author HasanovZK
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "attached_cards")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class AttachedCardEntity extends Auditable<String> {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "card_generator")
    @SequenceGenerator(name = "card_generator", sequenceName = "card_seq", allocationSize = 1)
    private Long id;
    private String maskedPan;
    private String cardUid;
    private String brand;
    private boolean active;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    @JsonIgnore
    private UserEntity userEntity;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "app_id")
    @JsonIgnore
    private ApplicationEntity applicationEntity;

    @Override
    public String toString() {
        return "AttachedCardEntity{" +
               "maskedPan='" + maskedPan + '\'' +
               ", cardUid='" + cardUid + '\'' +
               ", brand='" + brand + '\'' +
               ", active=" + active +
               '}';
    }
}
