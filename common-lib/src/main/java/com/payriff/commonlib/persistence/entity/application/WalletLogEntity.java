package com.payriff.commonlib.persistence.entity.application;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "wallet_log")
@ToString
public class WalletLogEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "wallet_log_generator")
    @SequenceGenerator(
            name = "wallet_log_generator",
            sequenceName = "wallet_log_seq",
            allocationSize = 1)
    private Long id;

    private BigDecimal amount;

    @Column(name = "current_balance")
    private BigDecimal currentBalance;

    @Column(name = "updated_balance")
    private BigDecimal updatedBalance;

    @Column(name = "wallet_id")
    private Long walletId;

    @Column(name = "wallet_history_id")
    private Long walletHistoryId;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "transaction_id")
    private Long transactionId;

    @Column(name = "create_date")
    private LocalDateTime createDate;
}
