package com.payriff.commonlib.persistence.repository;

import com.payriff.commonlib.persistence.entity.application.AsanDataHashEntity;
import com.payriff.commonlib.persistence.entity.application.UserEntity;
import com.payriff.commonlib.persistence.entity.processing.ApplicationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AsanDataHashRepository extends JpaRepository<AsanDataHashEntity, Long> {

    Optional<AsanDataHashEntity> findByUserEntityAndApplicationEntityAndStatus(UserEntity user,
                                                                               ApplicationEntity applicationEntity,
                                                                               String status);

    Optional<AsanDataHashEntity> findByUserEntityAndTransactionIdAndStatus(UserEntity user,
                                                                           String transactionId,
                                                                           String status);

    void deleteByUserEntityAndApplicationEntityAndStatus(UserEntity user,
                                                         ApplicationEntity applicationEntity,
                                                         String status);

}
