package com.payriff.commonlib.persistence.repository;

import com.payriff.commonlib.persistence.entity.processing.RefundableLimitEntity;
import com.payriff.commonlib.persistence.entity.processing.TransactionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Kanan
 */
@Repository
public interface RefundableLimitRepository extends JpaRepository<RefundableLimitEntity, Long> {

    List<RefundableLimitEntity> findAllByTransactionEntity(TransactionEntity transaction);

    int countAllByTransactionEntity(TransactionEntity transaction);
}
