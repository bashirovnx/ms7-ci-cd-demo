package com.payriff.commonlib.persistence.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;


@Data
@AllArgsConstructor
public class SuccessfulOffUsTransactionReport {

    private BigDecimal offUsAmountSum;
    private BigDecimal offUsPaidAmountSum;
    private Long offUsTransactionCount;
}
