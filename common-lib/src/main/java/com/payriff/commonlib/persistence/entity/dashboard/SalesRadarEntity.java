package com.payriff.commonlib.persistence.entity.dashboard;

import com.payriff.commonlib.enums.PaymentStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SalesRadarEntity {

    private Long tranCount;
    private String name;
    private PaymentStatus paymentStatus;

}
