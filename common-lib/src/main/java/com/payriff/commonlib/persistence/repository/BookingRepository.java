package com.payriff.commonlib.persistence.repository;


import com.payriff.commonlib.persistence.entity.BookingEntity;
import com.payriff.commonlib.persistence.entity.application.InvoiceTemplateEntity;
import com.payriff.commonlib.persistence.entity.application.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

/**
 * @author Kanan
 */
@Repository
public interface BookingRepository extends JpaRepository<BookingEntity, Long> {
    Set<BookingEntity> findAllByUserOrderByCreatedDateDesc(UserEntity user);

    Optional<BookingEntity> findByUuid(String uuid);

    Optional<BookingEntity> findByInvoice(InvoiceTemplateEntity invoice);
}
