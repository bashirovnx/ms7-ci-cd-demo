package com.payriff.commonlib.persistence.repository;

import com.payriff.commonlib.persistence.entity.application.AttachedCardEntity;
import com.payriff.commonlib.persistence.entity.application.UserEntity;
import com.payriff.commonlib.persistence.entity.processing.ApplicationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author HasanovZK
 */
@Repository
public interface AttachedCardRepository extends JpaRepository<AttachedCardEntity, Long> {
    Optional<List<AttachedCardEntity>> findAllByUserEntityAndActive(UserEntity userEntity, boolean active);

    Optional<List<AttachedCardEntity>> findAllByApplicationEntityAndActive(ApplicationEntity applicationEntity,
                                                                           boolean active);
}
