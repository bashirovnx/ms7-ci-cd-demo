package com.payriff.commonlib.persistence.entity.security;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.payriff.commonlib.persistence.entity.application.Auditable;
import com.payriff.commonlib.persistence.entity.application.UserEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * @author HasanovZK
 */
@Setter
@Getter
@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@Table(name = "token")
public class TokenEntity extends Auditable<String> {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "token_generator")
    @SequenceGenerator(name = "token_generator", sequenceName = "token_seq", allocationSize = 1)
    private Long id;
    private String accessToken;
    private String refreshToken;
    private boolean active;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    @JsonIgnore
    private UserEntity userEntity;
}
