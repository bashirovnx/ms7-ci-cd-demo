package com.payriff.commonlib.persistence.entity.application;

import com.payriff.commonlib.persistence.entity.processing.ApplicationEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "asan_data_hash")
public class AsanDataHashEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "asan_data_hash_generator")
    @SequenceGenerator(name = "asan_data_hash_generator", sequenceName = "asan_data_hash_seq", allocationSize = 1)
    private Long id;

    private String phone;

    @Column(name = "asan_user_id")
    private String asanUserId;

    @Column(name = "pin_code")
    private String pinCode;

    @Column(name = "tax_id")
    private String taxId;

    @Column(name = "transaction_id")
    private String transactionId;

    private String status;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private UserEntity userEntity;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "application_id")
    private ApplicationEntity applicationEntity;

    @Column(name = "create_date")
    private LocalDateTime createDate;

}
