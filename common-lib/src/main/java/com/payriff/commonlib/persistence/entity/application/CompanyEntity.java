package com.payriff.commonlib.persistence.entity.application;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.payriff.commonlib.persistence.entity.MerchantCategoryEntity;
import com.payriff.commonlib.persistence.entity.processing.ApplicationEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

/** @author HasanovZK */
@Entity
@Setter
@Getter
// @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Table(name = "company")
public class CompanyEntity extends Auditable<String> {
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "company_generator")
  @SequenceGenerator(name = "company_generator", sequenceName = "company_seq", allocationSize = 1)
  private Long id;

  private String name;
  private String website;
  private String voen;
  private String address;
  private String description;
  private String fin;
  private boolean active = true;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "user_id")
  @JsonIgnore
  @EqualsAndHashCode.Exclude
  @ToString.Exclude
  private UserEntity userEntity;

  @JsonProperty("companyCategory")
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "merchant_id")
  // @JsonIgnore
  private MerchantCategoryEntity merchantCategory;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "companyEntity")
  @JsonIgnore
  @EqualsAndHashCode.Exclude
  @ToString.Exclude
  private Set<ApplicationEntity> applicationEntities;

  @OneToOne(fetch = FetchType.EAGER, mappedBy = "companyEntity", cascade = CascadeType.MERGE)
  @JsonIgnore
  @EqualsAndHashCode.Exclude
  @ToString.Exclude
  private BankAccountEntity bankAccountEntity;
}
