package com.payriff.commonlib.persistence.entity.application;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.payriff.commonlib.enums.UserType;
import com.payriff.commonlib.persistence.entity.BookingEntity;
import com.payriff.commonlib.persistence.entity.processing.ApplicationEntity;
import com.payriff.commonlib.persistence.entity.processing.TransactionEntity;
import com.payriff.commonlib.persistence.entity.security.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.util.*;

/** @author HasanovZK */
@Entity
@Setter
@Getter
@Table(name = "users")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class UserEntity extends Auditable<String> {
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "users_generator")
  @SequenceGenerator(name = "users_generator", sequenceName = "users_seq", allocationSize = 1)
  private Long id;

  @Column(unique = true)
  private String username;

  @Column(unique = true)
  private String email;

  private String password;
  private String fullName;
  @Nullable private String mobilePhone;
  private boolean active = true;
  private boolean accountConfirm;

  @ColumnDefault("false")
  private boolean mailConfirm;

  @Nullable private Boolean accountLocked;

  @ColumnDefault("false")
  private boolean attachedCard = false;

  @Enumerated(EnumType.STRING)
  private UserType userType;

  private String fin;

  @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH})
  @LazyCollection(LazyCollectionOption.FALSE)
  @JoinTable(
      name = "users_roles",
      joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
      inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
  private Set<Role> roles = new LinkedHashSet<>();

  @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH})
  @JoinTable(
      name = "users_privilege",
      joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
      inverseJoinColumns = @JoinColumn(name = "privilege_id", referencedColumnName = "id"))
  @JsonIgnore
  private Collection<Privilege> privileges = new ArrayList<>();

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "userEntity")
  private List<TokenEntity> tokenData;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "userEntity")
  private List<ConfirmationEntity> confirmationEntities;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "userEntity")
  private List<TransactionEntity> transactionEntities;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "userEntity")
  private Set<ApplicationEntity> applicationEntities;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "userEntity")
  @JsonIgnore
  private List<OtpSmsEntity> otpSmsEntities;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "userEntity")
  @JsonIgnore
  private List<OtpAttemptEntity> otpAttemptEntities;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "userEntity")
  @JsonIgnore
  private List<AttemptLimitEntity> attemptLimitEntities;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "userEntity")
  @JsonIgnore
  private List<InvoiceTemplateEntity> mailOrderEntities;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "userEntity")
  @JsonIgnore
  private List<CustomerInvoiceEntity> customerInvoiceEntities;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "userEntity")
  @JsonIgnore
  private List<CompanyEntity> companyEntities;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "userEntity")
  @JsonIgnore
  private List<AttachedCardEntity> attachedCardEntities;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "userEntity")
  @JsonIgnore
  private List<WalletEntity> walletEntities;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "userEntity")
  @JsonIgnore
  private List<GlobalConfigEntity> globalConfigEntities;

  @OneToOne(mappedBy = "user")
  private UserNotpEntity userNotpEntity;

  @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
  @JsonIgnore
  private Set<BookingEntity> bookings;
  //
  //  public Collection<Privilege> getPrivileges() {
  //    return this.roles.stream()
  //        .map(Role::getPrivileges)
  //        .flatMap(Collection::stream)
  //        .collect(Collectors.toList());
  //  }
}
