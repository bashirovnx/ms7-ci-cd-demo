package com.payriff.commonlib.persistence.repository;

import com.payriff.commonlib.persistence.entity.application.AsanAuthLogEntity;
import com.payriff.commonlib.persistence.entity.application.UserEntity;
import com.payriff.commonlib.persistence.entity.processing.ApplicationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AsanAuthLogRepository extends JpaRepository<AsanAuthLogEntity, Long> {

    Optional<AsanAuthLogEntity> findByUserEntityAndTransactionIdAndStatus(UserEntity user, String transID, String status);

    Optional<List<AsanAuthLogEntity>> findByUserEntityAndApplicationEntityAndStatus(UserEntity user,
                                                                                     ApplicationEntity application,
                                                                                     String status);

    void deleteByUserEntityAndStatus(UserEntity userEntity, String status);

}
