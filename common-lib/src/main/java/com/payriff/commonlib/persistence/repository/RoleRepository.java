package com.payriff.commonlib.persistence.repository;

import com.payriff.commonlib.enums.RoleType;
import com.payriff.commonlib.persistence.entity.security.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/** @author HasanovZK */
@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
  Role findByName(String name);

  List<Role> findAllByRoleType(RoleType roleType);
  Set<Role> findAllByRoleTypeAndId(RoleType roleType,Long id);
}
