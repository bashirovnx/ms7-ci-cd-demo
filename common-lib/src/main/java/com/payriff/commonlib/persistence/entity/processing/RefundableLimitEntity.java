package com.payriff.commonlib.persistence.entity.processing;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author Kanan
 */
@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = false)
@Table(name = "Refundable_Limit")
public class RefundableLimitEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "refundable_limit_generator")
    @SequenceGenerator(name = "refundable_limit_generator", sequenceName = "refundable_limit_seq", allocationSize = 1)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "transaction_id")
    @JsonIgnore
    private TransactionEntity transactionEntity;

    private BigDecimal refundAmount;

    private BigDecimal restOfAmount;

    private LocalDateTime createdDate;

    @PrePersist
    void createdAt() {
        this.createdDate = LocalDateTime.now();
    }

}
