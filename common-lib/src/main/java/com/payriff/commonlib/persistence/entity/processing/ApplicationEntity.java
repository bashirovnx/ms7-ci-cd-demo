package com.payriff.commonlib.persistence.entity.processing;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.payriff.commonlib.enums.AccountType;
import com.payriff.commonlib.enums.ApplicationStatus;
import com.payriff.commonlib.enums.GatewayType;
import com.payriff.commonlib.persistence.entity.application.*;
import com.payriff.commonlib.persistence.entity.security.OtpHashEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Transient;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

/** @author HasanovZK */
@Entity
@Setter
@Getter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
// @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "application")
@NamedEntityGraph(
    name = ApplicationEntity.APP_USERS_GRAPH,
    attributeNodes = {
      @NamedAttributeNode("userEntity"),
    })
public class ApplicationEntity extends Auditable<String> {
  public static final String APP_USERS_GRAPH = "apps-with-users";

  public ApplicationEntity(Long id) {
    this.id = id;
  }

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "app_generator")
  @SequenceGenerator(name = "app_generator", sequenceName = "app_seq", allocationSize = 1)
  private Long id;

  private String name = "Default";
  private String merchantId = "E1000020";
  private String kbMerchantId;
  private String appKey ;
  private String description;
  private String url;
  private ApplicationStatus status = ApplicationStatus.TEST;

  @Enumerated(EnumType.STRING)
  private AccountType accountType;

  @Enumerated(EnumType.STRING)
  private GatewayType gatewayType;

  private boolean active;
  @Transient private String qrCode;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "applicationEntity")
  @JsonIgnore
  private List<TransactionEntity> transactionEntities;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "user_id")
  @JsonIgnore
  private UserEntity userEntity;

  @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  @JoinColumn(name = "company_id")
  private CompanyEntity companyEntity;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "applicationEntity")
  @JsonIgnore
  private List<OtpHashEntity> otpHashEntities;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "applicationEntity")
  @JsonIgnore
  private List<InvoiceTemplateEntity> mailOrderEntities;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "applicationEntity")
  @JsonIgnore
  private List<CustomerInvoiceEntity> customerInvoiceEntities;

  @OneToMany(fetch = FetchType.EAGER, mappedBy = "applicationEntity")
  private List<AttachedCardEntity> attachedCardEntities;

  @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
  @JoinColumn(name = "wallet_id", referencedColumnName = "id")
  private WalletEntity walletEntity;

  @OneToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "config_id", referencedColumnName = "id")
  //  @JsonIgnore
  private GlobalConfigEntity globalConfigEntity = new GlobalConfigEntity();

  @Override
  public String toString() {
    return "ApplicationEntity{"
        + "id="
        + id
        + ", name='"
        + name
        + '\''
        + ", merchantId='"
        + merchantId
        + '\''
        + ", kbMerchantId='"
        + kbMerchantId
        + '\''
        + ", appKey='"
        + appKey
        + '\''
        + ", description='"
        + description
        + '\''
        + ", url='"
        + url
        + '\''
        + ", status="
        + status
        + ", accountType="
        + accountType
        + ", gatewayType="
        + gatewayType
        + ", active="
        + active
        + ", qrCode='"
        + qrCode
        + '\''
        + '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    if (!super.equals(o)) return false;
    ApplicationEntity that = (ApplicationEntity) o;
    return active == that.active
        && id.equals(that.id)
        && name.equals(that.name)
        && merchantId.equals(that.merchantId)
        && appKey.equals(that.appKey)
        && Objects.equals(description, that.description)
        && Objects.equals(url, that.url)
        && status == that.status
        && accountType == that.accountType
        && gatewayType == that.gatewayType;
  }

  @Override
  public int hashCode() {
    return Objects.hash(
        super.hashCode(),
        id,
        name,
        merchantId,
        appKey,
        description,
        url,
        status,
        accountType,
        gatewayType,
        active);
  }
}
