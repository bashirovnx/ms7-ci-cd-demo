package com.payriff.commonlib.persistence.repository;

import com.payriff.commonlib.enums.EmanatTransStatus;
import com.payriff.commonlib.persistence.entity.application.EmanatTransInfoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EmanatTransInfoRepository extends JpaRepository<EmanatTransInfoEntity, Long> {

    Optional<EmanatTransInfoEntity> findByBackRrnAndStatus(String rrn, EmanatTransStatus status);

    List<EmanatTransInfoEntity> findByBackRrn(String rrn);

    Optional<EmanatTransInfoEntity> findByInvoiceCodeAndStatus(String invoiceCode, EmanatTransStatus status);

}
