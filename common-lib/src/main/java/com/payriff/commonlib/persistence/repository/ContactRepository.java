package com.payriff.commonlib.persistence.repository;

import com.payriff.commonlib.persistence.entity.application.ContactEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author HasanovZK
 */
public interface ContactRepository extends JpaRepository<ContactEntity, Long> {
}
