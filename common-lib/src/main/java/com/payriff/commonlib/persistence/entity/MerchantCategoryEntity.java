package com.payriff.commonlib.persistence.entity;

import com.payriff.commonlib.persistence.entity.application.Auditable;
import lombok.Data;

import javax.persistence.*;

/** @author HasanovZK */
@Data
@Entity
@Table(name = "merchant_category")
public class MerchantCategoryEntity extends Auditable<String> {
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "merchant_generator_cat")
  @SequenceGenerator(
      name = "merchant_generator_cat",
      sequenceName = "merch_cat_seq",
      allocationSize = 1)
  private Long id;

  private String name;
  private boolean active = true;
  private String kbMerchant;
}
