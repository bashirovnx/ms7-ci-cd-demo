package com.payriff.commonlib.persistence.repository;


import com.payriff.commonlib.persistence.entity.processing.CompanyCategoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CompanyCategoryRepository extends JpaRepository<CompanyCategoryEntity, Long> {

    Optional<List<CompanyCategoryEntity>> findAllByActiveTrue();

}
