package com.payriff.commonlib.persistence.repository;

import com.payriff.commonlib.enums.PaymentStatus;
import com.payriff.commonlib.persistence.entity.application.UserEntity;
import com.payriff.commonlib.persistence.entity.dashboard.StatisticsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface StatisticsRepository extends JpaRepository<StatisticsEntity, Long> {

    Optional<List<StatisticsEntity>> findByUserEntity(UserEntity userEntity);

    @Transactional
    @Modifying
    void deleteStatisticsEntityByUserEntityAndPaymentStatus(UserEntity userEntity, PaymentStatus paymentStatus);

}
