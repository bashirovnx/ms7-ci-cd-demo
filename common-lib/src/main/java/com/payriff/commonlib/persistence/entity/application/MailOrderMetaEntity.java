package com.payriff.commonlib.persistence.entity.application;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author HasanovZK
 */
@Data
@AllArgsConstructor
public class MailOrderMetaEntity {
    private int count;
    private String paymentType;
}
