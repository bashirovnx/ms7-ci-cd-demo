package com.payriff.commonlib.persistence.repository;

import com.payriff.commonlib.persistence.entity.application.UserEntity;
import com.payriff.commonlib.persistence.entity.security.OtpSmsEntity;
import com.payriff.commonlib.persistence.entity.util.OperationType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Optional;

/**
 * @author HasanovZK
 */
@Repository
public interface OtpSmsRepository extends JpaRepository<OtpSmsEntity, Long> {
    @Transactional
    @Modifying
    @Query(value = "UPDATE otp_sms set active=false where user_id= :userId", nativeQuery = true)
    void deactivateOldOtp(@Param("userId") Long userId);

    Optional<OtpSmsEntity> findByOperationTypeAndExpireDateAfterAndCodeAndActive(OperationType operationType,
                                                                                 LocalDateTime localDateTime,
                                                                                 String code, boolean active);

    Optional<OtpSmsEntity> findByUserEntityAndOperationTypeAndExpireDateAfterAndActiveAndCode(UserEntity entity,
                                                                                              OperationType operationType,
                                                                                              LocalDateTime localDateTime,
                                                                                              boolean active,
                                                                                              String code);
}
