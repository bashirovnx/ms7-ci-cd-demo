package com.payriff.commonlib.persistence.repository;

import com.payriff.commonlib.persistence.entity.security.ConvertCheckEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * @author HasanovZK
 */
@Repository
public interface ConvertCheckRepository extends JpaRepository<ConvertCheckEntity, Long> {
    Optional<ConvertCheckEntity> findByUuidAndActive(String uuid, boolean active);

    @Transactional
    @Modifying
    @Query(value = "UPDATE convert_check set active=false where uuid= :uuid", nativeQuery = true)
    void deactivateOld(@Param("uuid") String uuid);
}
