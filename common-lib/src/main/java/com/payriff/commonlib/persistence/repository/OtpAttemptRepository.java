package com.payriff.commonlib.persistence.repository;

/**
 * @author HasanovZK
 */

import com.payriff.commonlib.persistence.entity.security.OtpAttemptEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Repository
public interface OtpAttemptRepository extends JpaRepository<OtpAttemptEntity, Long> {

    @Query(value = "select count(*) from otp_attempt where " +
                   "user_id= :userId and " +
                   "ip=:ip and active=true and " +
                   "operation_state=1 and phone_number=:phoneNumber and " +
                   "created_date >= :time",
           nativeQuery = true)
    Integer countAllBy(@Param("userId") Long userId,
                   @Param("ip") String ip,
                   @Param("phoneNumber") String phoneNumber, @Param("time") LocalDateTime time);

    @Query(value = "select count(*) from otp_attempt where " +
                   "user_id= :userId and " +
                   "ip=:ip and " +
                   "operation_state=0 and phone_number=:phoneNumber and user_agent=:userAgent and " +
                   "created_date >= :time ",
           nativeQuery = true)
    int countAllByUserAgent(@Param("userId") Long userId,
                            @Param("ip") String ip,
                            @Param("phoneNumber") String phoneNumber, @Param("time") LocalDateTime time,
                            @Param("userAgent") String userAgent);

    @Query(value = "select count(*) from otp_attempt where " +
                   "user_id= :userId and " +
                   "operation_type=:operationType and " +
                   "ip=:ip and " +
                   "operation_state=0 and phone_number=:phoneNumber and user_agent=:userAgent and " +
                   "created_date >= :time ",
           nativeQuery = true)
    int countAllByUserAgentAndOperationType(@Param("userId") Long userId,
                                            @Param("ip") String ip,
                                            @Param("phoneNumber") String phoneNumber, @Param("time") LocalDateTime time,
                                            @Param("userAgent") String userAgent,
                                            @Param("operationType") String operationType);

    @Transactional
    @Modifying
    @Query(value = "UPDATE otp_attempt set active=false where user_id= :userId and ip=:ip", nativeQuery = true)
    void deactivateOldAttempts(@Param("userId") Long userId, @Param("ip") String ip);
}
