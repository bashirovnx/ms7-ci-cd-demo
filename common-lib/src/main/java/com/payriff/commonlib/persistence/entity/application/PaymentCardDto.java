package com.payriff.commonlib.persistence.entity.application;

import lombok.Data;

/**
 * @author HasanovZK
 */
@Data
public class PaymentCardDto {

    private String merchant;
    private String source;

}
