package com.payriff.commonlib.persistence.repository;

import com.payriff.commonlib.persistence.entity.processing.TransactionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * @author HasanovZK
 */
@Repository
public interface MetaDataRepository extends JpaRepository<TransactionEntity, Long> {

    @Query(value = "SELECT * FROM transaction where (payment_status=1 or " +
                   " payment_status=1  or payment_status=3 or payment_status=4) and user_id=:userId",
           nativeQuery = true)
    Optional<List<TransactionEntity>> getAllTransaction(@Param("userId") Long userId);

    @Query(value = "SELECT * FROM transaction where (payment_status=1 or " +
                   " payment_status=1  or payment_status=3 or payment_status=4) and " +
                   "user_id=:userId and " +
                   "app_id=:appId",
           nativeQuery = true)
    Optional<List<TransactionEntity>> getAllTransactionByApp(@Param("userId") Long userId, @Param("appId") Long appId);

    @Query(value = "SELECT * FROM transaction where (payment_status=1 or " +
                   " payment_status=1  or payment_status=3 or payment_status=4) and " +
                   "user_id=:userId and " +
                   "app_id=:appId and last_modified_date between :from and :to",
           nativeQuery = true)
    Optional<List<TransactionEntity>> getAllTransactionByApp(@Param("userId") Long userId,
                                                             @Param("appId") Long appId,
                                                             @Param("from") Date from,
                                                             @Param("to") Date to);

    @Query(value = "SELECT * FROM transaction where (payment_status=1 or " +
                   " payment_status=1  or payment_status=3 or payment_status=4) and " +
                   "user_id=:userId and " +
                   "last_modified_date between :from and :to",
           nativeQuery = true)
    Optional<List<TransactionEntity>> getAllTransactionFromTo(@Param("userId") Long userId,
                                                              @Param("from") Date from,
                                                              @Param("to") Date to);

}
