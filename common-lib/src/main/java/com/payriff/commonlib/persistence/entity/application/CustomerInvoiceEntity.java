package com.payriff.commonlib.persistence.entity.application;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.payriff.commonlib.enums.*;
import com.payriff.commonlib.persistence.entity.processing.ApplicationEntity;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

/** @author HasanovZK */
@Entity
@Getter
@Setter
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@Table(name = "customer_invoice")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CustomerInvoiceEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "invoice_generator")
  @SequenceGenerator(name = "invoice_generator", sequenceName = "invoice_seq", allocationSize = 1)
  private Long id;

  private String merchantId;
  private BigDecimal amount;
  private String fullName;
  private String email;
  private String phoneNumber;
  private String customMessage;
  private LocalDateTime expireDate;

  @Enumerated(EnumType.STRING)
  private CurrencyType currencyType;

  @Enumerated(EnumType.STRING)
  private LanguageType languageType;

  @Enumerated(EnumType.STRING)
  private PaymentType paymentType;

  @Enumerated(EnumType.STRING)
  private SubscriptionState subscriptionState;

  private LocalDate paymentDay = LocalDate.now();
  private LocalDate expireDay;
  private boolean active = true;
  private String description;
  private String approveURL;
  private String cancelURL;
  private String declineURL;
  private String uuid ;
  private String invoiceUuid;
  String baseUrl;
  private String invoiceCode;

  @Enumerated(EnumType.STRING)
  private InvoiceStatus invoiceStatus;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "user_id")
  @JsonIgnore
  private UserEntity userEntity;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "app_id")
  @JsonIgnore
  private ApplicationEntity applicationEntity;

  @Enumerated(EnumType.STRING)
  private InstallmentProductType installmentProductType;

  private Integer installmentPeriod;

  @Enumerated(EnumType.STRING)
  private InvoiceSource source;

  @Column private Boolean directPay;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "invoice_template_id")
  @JsonIgnore
  private InvoiceTemplateEntity invoiceTemplateEntity;

  @CreatedDate
  @Column(name = "created_date")
  private LocalDateTime createdDate;

  public CustomerInvoiceEntity(boolean nullSafe) {
    if (nullSafe) {
      this.fullName = "empty";
      this.invoiceUuid = "empty";
      this.directPay = false;
    }
  }
}
