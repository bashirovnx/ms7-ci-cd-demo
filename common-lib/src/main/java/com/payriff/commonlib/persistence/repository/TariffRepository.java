package com.payriff.commonlib.persistence.repository;

import com.payriff.commonlib.enums.CardType;
import com.payriff.commonlib.enums.GatewayType;
import com.payriff.commonlib.enums.InstallmentProductType;
import com.payriff.commonlib.enums.PaymentSource;
import com.payriff.commonlib.persistence.entity.MerchantCategoryEntity;
import com.payriff.commonlib.persistence.entity.application.TariffEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/** @author HasanovZK */
public interface TariffRepository extends JpaRepository<TariffEntity, Long> {
  Optional<TariffEntity> findByActiveTrueAndMerchant(String merchant);

  Optional<TariffEntity> findByActiveTrueAndMerchantAndCardType(String merchant, CardType cardType);

  Optional<TariffEntity>
      findByActiveTrueAndCardTypeAndGatewayTypeAndMerchantCategoryAndPaymentSource(
          CardType cardType,
          GatewayType gatewayType,
          MerchantCategoryEntity categoryEntity,
          PaymentSource paymentSource);


    Optional<List<TariffEntity>>
    findByActiveTrueAndGatewayTypeAndMerchantCategoryAndPaymentSource(
            GatewayType gatewayType,
            MerchantCategoryEntity categoryEntity,
            PaymentSource paymentSource);

  Optional<List<TariffEntity>> findByActiveTrueAndProductTypeAndInstallmentPeriod(
          InstallmentProductType productType, int period);
}
