package com.payriff.commonlib.persistence.repository;

import com.payriff.commonlib.persistence.entity.application.WalletEntity;
import com.payriff.commonlib.persistence.entity.application.WalletHistoryEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author HasanovZK
 */
public interface WalletHistoryRepository extends JpaRepository<WalletHistoryEntity, Long> {

    Page<WalletHistoryEntity> findAllByActiveTrueAndWalletEntityAndCreatedDateBetween(WalletEntity walletEntity,
                                                                                      Pageable pageable,
                                                                                      Date from, Date to);

    @Query( "SELECT sum(w.amount) " +
            "FROM WalletHistoryEntity AS w " +
            "WHERE w.createdDate BETWEEN :from AND :to  " +
            "AND w.walletEntity.id = :walletId AND w.operation=" +
            "com.digipay.pysys.model.enums.WalletOperation.IN"
    )
    BigDecimal getWalletHistoryInTransactions(
            Date from, Date to, Long walletId
    );

    @Query( "SELECT sum(w.amount) " +
            "FROM WalletHistoryEntity AS w " +
            "WHERE w.createdDate BETWEEN :from AND :to  " +
            "AND w.walletEntity.id = :walletId AND w.operation=" +
            "com.digipay.pysys.model.enums.WalletOperation.OUT"
    )
    BigDecimal getWalletHistoryOutTransactions(
            Date from, Date to, Long walletId
    );
}
