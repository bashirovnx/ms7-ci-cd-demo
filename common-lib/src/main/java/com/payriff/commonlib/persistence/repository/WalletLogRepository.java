package com.payriff.commonlib.persistence.repository;

import com.payriff.commonlib.persistence.entity.application.WalletLogEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WalletLogRepository extends JpaRepository<WalletLogEntity, Long> {}
