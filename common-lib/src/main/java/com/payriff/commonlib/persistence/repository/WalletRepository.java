package com.payriff.commonlib.persistence.repository;

import com.payriff.commonlib.persistence.entity.application.UserEntity;
import com.payriff.commonlib.persistence.entity.application.WalletEntity;
import com.payriff.commonlib.persistence.entity.processing.ApplicationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author HasanovZK
 */
public interface WalletRepository extends JpaRepository<WalletEntity, Long> {
    Optional<WalletEntity> findWalletEntityByActiveTrueAndApplicationEntity(ApplicationEntity applicationEntity);

    Optional<List<WalletEntity>> findAllByActiveTrueAndUserEntity(UserEntity userEntity);
}
