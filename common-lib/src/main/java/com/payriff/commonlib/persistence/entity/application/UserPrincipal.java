//package com.payriff.commonlib.persistence.entity.application;
//
///**
// * @author HasanovZK
// */
//
//import com.digipay.pysys.model.entity.security.Role;
//import lombok.AllArgsConstructor;
//import lombok.Builder;
//import lombok.NoArgsConstructor;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.security.core.userdetails.UserDetails;
//
//import java.util.Collection;
//import java.util.List;
//import java.util.Objects;
//import java.util.stream.Collectors;
//
//@AllArgsConstructor
//@NoArgsConstructor
//@Builder
//public class UserPrincipal implements UserDetails {
//
//    private Long id;
//    private String login;
//    private String password;
//    private boolean isLocked;
//    private String fullName;
//    private Collection<? extends GrantedAuthority> authorities;
//
//    public static UserPrincipal create(UserEntity userEntity) {
//        UserPrincipal userPrincipal = new UserPrincipal();
//        userPrincipal.setId(userEntity.getId());
//        userPrincipal.setLogin(userEntity.getEmail());
//        userPrincipal.setPassword(userEntity.getPassword());
//        userPrincipal.setLocked(false);
//
//        List<GrantedAuthority> authorities =
//                userEntity.getRoles().stream()
//                          .map(roleEntity -> new SimpleGrantedAuthority(roleEntity.getName()))
//                          .collect(Collectors.toList());
//
//        List<SimpleGrantedAuthority> privilegeGrantedAuthorities = userEntity.getRoles().stream()
//                .map(Role::getPrivileges)
//                .flatMap(Collection::stream)
//                .map(privilege -> new SimpleGrantedAuthority(privilege.getName()))
//                .collect(Collectors.toList());
//
//        List<? extends GrantedAuthority> pr = userEntity.getPrivileges()
//                                                        .stream()
//                                                        .map(privilege -> new SimpleGrantedAuthority(privilege.getName()))
//                                                        .collect(Collectors.toList());
//        authorities.addAll(pr);
//        authorities.addAll(privilegeGrantedAuthorities);
//
//        userPrincipal.setAuthorities(authorities);
//        return userPrincipal;
//    }
//
//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }
//
//    public String getLogin() {
//        return login;
//    }
//
//    public void setLogin(String login) {
//        this.login = login;
//    }
//
//    public void setPassword(String password) {
//        this.password = password;
//    }
//
//    public boolean isLocked() {
//        return isLocked;
//    }
//
//    public void setLocked(boolean locked) {
//        isLocked = locked;
//    }
//
//    public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
//        this.authorities = authorities;
//    }
//
//    public String getFullName() {
//        return fullName;
//    }
//
//    public void setFullName(String fullName) {
//        this.fullName = fullName;
//    }
//
//    @Override
//    public Collection<? extends GrantedAuthority> getAuthorities() {
//        return this.authorities;
//    }
//
//    @Override
//    public String getPassword() {
//        return this.password;
//    }
//
//    @Override
//    public String getUsername() {
//        return this.login;
//    }
//
//    @Override
//    public boolean isAccountNonExpired() {
//        return true;
//    }
//
//    @Override
//    public boolean isAccountNonLocked() {
//        return !this.isLocked;
//    }
//
//    @Override
//    public boolean isCredentialsNonExpired() {
//        return true;
//    }
//
//    @Override
//    public boolean isEnabled() {
//        return true;
//    }
//
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        UserPrincipal that = (UserPrincipal) o;
//        return Objects.equals(id, that.id);
//    }
//
//    @Override
//    public int hashCode() {
//        return Objects.hash(id);
//    }
//
//}
