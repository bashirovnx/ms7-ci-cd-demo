package com.payriff.commonlib.persistence.entity.application;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.payriff.commonlib.persistence.entity.processing.ApplicationEntity;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.Hibernate;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

/** @author HasanovZK */
@Entity
@Table(name = "wallet")
@Getter
@Setter
@RequiredArgsConstructor
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class WalletEntity extends Auditable<String> {
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "wallet_generator")
  @SequenceGenerator(name = "wallet_generator", sequenceName = "wallet_seq", allocationSize = 1)
  private Long id;

  private boolean active = true;
  private BigDecimal balance = BigDecimal.ZERO;

  @Version
  @ColumnDefault(value = "0")
  private int version;

  @OneToOne(mappedBy = "walletEntity", cascade = CascadeType.ALL)
  @JoinColumn(name = "app_id")
  private ApplicationEntity applicationEntity;

  @OneToMany(mappedBy = "walletEntity", cascade = CascadeType.ALL)
  @JsonIgnore
  private List<WalletHistoryEntity> walletHistoryEntities;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "user_id")
  @JsonIgnore
  private UserEntity userEntity;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
    WalletEntity that = (WalletEntity) o;
    return id != null && Objects.equals(id, that.id);
  }

  @Override
  public int hashCode() {
    return getClass().hashCode();
  }

  @Override
  public String toString() {
    return "WalletEntity{" + "id=" + id + ", active=" + active + ", balance=" + balance + '}';
  }
}
