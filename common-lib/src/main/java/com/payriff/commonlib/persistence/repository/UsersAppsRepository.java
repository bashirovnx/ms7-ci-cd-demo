package com.payriff.commonlib.persistence.repository;

import com.payriff.commonlib.enums.StatusType;
import com.payriff.commonlib.persistence.entity.security.UsersAppsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

/** @author HasanovZK */
@Repository
public interface UsersAppsRepository extends JpaRepository<UsersAppsEntity, Long> {
  Set<UsersAppsEntity> findBySubUserIdAndStatus(Long subUserId, StatusType type);

  Set<UsersAppsEntity> findByOwnerId(Long ownerId);

  Set<UsersAppsEntity> findAllByStatusAndOwnerId(StatusType status, Long ownerId);

  Set<UsersAppsEntity> findUsersAppsEntitiesByAppIdAndSubUserIdAndStatus(
      Long appId, Long userId, StatusType statusType);

  @Transactional
  @Modifying
  @Query(
      value = "UPDATE users_apps  SET status = 'INACTIVE' where app_id = :appId",
      nativeQuery = true)
  void deactiveApplicationsByAppID(Long appId);
}
