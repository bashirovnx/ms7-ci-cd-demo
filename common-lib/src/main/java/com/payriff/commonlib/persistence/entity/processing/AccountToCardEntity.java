package com.payriff.commonlib.persistence.entity.processing;

import com.payriff.commonlib.enums.PaymentStatus;
import com.payriff.commonlib.persistence.entity.application.Auditable;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "account_to_card")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AccountToCardEntity extends Auditable<String> {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  private Long id;

  private BigDecimal amount;
  private String orderId;
  private String sessionId;
  private String cardUuid;
  private Long transactionId;
  private Long appId;

  @Column(columnDefinition = "TEXT")
  private String gatewayResponse;

  @Column(columnDefinition = "TEXT")
  private String gatewayRequest;

  @Enumerated(EnumType.STRING)
  private PaymentStatus paymentStatus;
}
