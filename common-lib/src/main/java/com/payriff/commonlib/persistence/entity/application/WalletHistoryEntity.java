package com.payriff.commonlib.persistence.entity.application;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.payriff.commonlib.enums.WalletOperation;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * @author HasanovZK
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "wallet_history")
@Data
public class WalletHistoryEntity extends Auditable<String> {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "wallet_his_generator")
    @SequenceGenerator(name = "wallet_his_generator", sequenceName = "wallet_his_seq", allocationSize = 1)
    private Long id;
    private boolean active = true;
    private BigDecimal balance;
    private BigDecimal amount;
    @Enumerated(EnumType.STRING)
    private WalletOperation operation;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "wallet_id")
    @JsonIgnore
    private WalletEntity walletEntity;
}
