package com.payriff.commonlib.persistence.entity.processing;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.payriff.commonlib.enums.*;
import com.payriff.commonlib.persistence.entity.application.Auditable;
import com.payriff.commonlib.persistence.entity.application.UserEntity;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

/** @author HasanovZK */
@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "Transaction")
@ToString(onlyExplicitlyIncluded = true )
public class TransactionEntity extends Auditable<String> {
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "transaction_generator")
  @SequenceGenerator(
      name = "transaction_generator",
      sequenceName = "transaction_seq",
      allocationSize = 1)
  @ToString.Include
  private Long id;
  @ToString.Include
  private String merchantId;
  @JsonIgnore private String kbMerchantId;
  @ToString.Include
  private BigDecimal amount;
  @ToString.Include
  private String source;
  @ToString.Include
  private String orderType;
  private CurrencyType currencyType;
  private String description;
  @ToString.Include
  private String orderStatus;
  @ToString.Include
  private BigDecimal payriffAmount;
  private String installment_count;
  @ToString.Include
  private Long orderId;
  @ToString.Include
  private String sessionId;
  private LanguageType orderLanguage;
  @ToString.Include
  private String approveURL;
  private String cancelURL;
  private String declineURL;
  private String cardBrand;
  private String POS_eci;
  @ToString.Include
  private String rrn;
  private String cardHolderName;
  @ToString.Include
  private String pan;
  private String merchantTranId;
  private String clientUrl;

  @ColumnDefault("true")
  @ToString.Include
  private boolean directPay = true;

  @Transient private String fullName;

  @Transient private String bookingId;

  @ToString.Include
  private PaymentStatus paymentStatus;

  @Enumerated(EnumType.STRING)
  private GatewayType gatewayType;

  @ColumnDefault("0")
  private PaymentSource paymentSource;
  @ToString.Include
  private ApplicationStatus applicationStatus;
  private String date;
  private String version;
  private boolean active;

  @ColumnDefault("false")
  @ToString.Include
  private boolean invoice = false;
  @ToString.Include
  private BigDecimal commissionRate;
  @ToString.Include
  private BigDecimal paidAmount;

  @Enumerated(EnumType.STRING)
  private CardType cardType;

  @Enumerated(EnumType.STRING)
  private InstallmentProductType installmentProductType;

  @ColumnDefault("0")
  private Integer installmentPeriod;

  @JsonIgnore private String uuid;

  private Boolean emanat;
  @ToString.Include
  private String responseDescription;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "user_id")
  @JsonIgnore
  private UserEntity userEntity;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "app_id")
  @JsonIgnore
  private ApplicationEntity applicationEntity;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "transactionEntity")
  @JsonIgnore
  private List<RefundableLimitEntity> refundableLimitEntity;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
    TransactionEntity that = (TransactionEntity) o;
    return id != null && Objects.equals(id, that.id);
  }

  @Override
  public int hashCode() {
    return getClass().hashCode();
  }
}
