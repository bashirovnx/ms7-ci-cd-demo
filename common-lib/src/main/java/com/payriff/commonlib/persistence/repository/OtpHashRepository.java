package com.payriff.commonlib.persistence.repository;

import com.payriff.commonlib.persistence.entity.security.OtpHashEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author HasanovZK
 */
@Repository
public interface OtpHashRepository extends JpaRepository<OtpHashEntity, Long> {
    Optional<OtpHashEntity> findByHash(String hash);
}
