package com.payriff.commonlib.persistence.entity.application;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "contract_fee")
public class ContractFeeEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "contract_fee_generator")
    @SequenceGenerator(name = "contract_fee_generator", sequenceName = "contract_fee_seq", allocationSize = 1)
    private Long id;

    @Column(name = "kapitalbank_donation")
    private String kapitalbankDonation;

    @Column(name = "kapitalbank_food")
    private String kapitalbankFood;

    @Column(name = "kapitalbank_others")
    private String kapitalbankOthers;

    @Column(name = "domesticbank_donation")
    private String domesticbankDonation;

    @Column(name = "domesticbank_food")
    private String domesticbankFood;

    @Column(name = "domesticbank_others")
    private String domesticbankOthers;

}
