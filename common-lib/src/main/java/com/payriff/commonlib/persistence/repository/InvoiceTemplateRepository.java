package com.payriff.commonlib.persistence.repository;

import com.payriff.commonlib.persistence.entity.application.InvoiceTemplateEntity;
import com.payriff.commonlib.persistence.entity.application.UserEntity;
import com.payriff.commonlib.persistence.entity.processing.ApplicationEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/** @author HasanovZK */
@Repository
public interface InvoiceTemplateRepository
    extends JpaRepository<InvoiceTemplateEntity, Long>,
        JpaSpecificationExecutor<InvoiceTemplateEntity> {
  Optional<InvoiceTemplateEntity> findByIdAndUserEntityAndActive(
          Long id, UserEntity userEntity, boolean active);

  Optional<InvoiceTemplateEntity> findByIdAndUserEntityInAndApplicationEntityInAndActiveTrue(
      Long id, List<UserEntity> users, List<ApplicationEntity> applications);

  Optional<InvoiceTemplateEntity> findByUuidAndActiveAndUserEntity(
      String uuid, boolean active, UserEntity userEntity);

  Optional<InvoiceTemplateEntity> findByUuidAndActiveTrue(String uuid);

  Page<InvoiceTemplateEntity>
      findAllByUserEntityAndActiveAndCreatedDateBetweenOrderByCreatedDateDesc(
          UserEntity userEntity, boolean active, Pageable pageable, Date from, Date to);

  Page<InvoiceTemplateEntity>
      findAllByUserEntityIsInAndActiveAndCreatedDateBetweenOrderByCreatedDateDesc(
          Set<UserEntity> userEntity, boolean active, Pageable pageable, Date from, Date to);

  Page<InvoiceTemplateEntity> findAllByUserEntityAndApplicationEntityAndActiveAndCreatedDateBetween(
      UserEntity userEntity,
      ApplicationEntity applicationEntity,
      boolean active,
      Pageable pageable,
      Date from,
      Date to);

  Page<InvoiceTemplateEntity>
      findAllByUserEntityIsInAndApplicationEntityAndActiveAndCreatedDateBetween(
          Set<UserEntity> userEntity,
          ApplicationEntity applicationEntity,
          boolean active,
          Pageable pageable,
          Date from,
          Date to);

  @Query(
      value =
          "select *\n"
              + "from mail_order t\n"
              + "where t.subscription_state = 'ACTIVE'\n"
              + "and t.active='true' "
              + "and t.payment_type ='ONETIME'\n"
              + "  and t.active='true'\n"
              + "  and to_date(to_char(now(), 'dd.MM.yyyy'), 'dd.MM.yyyy') =\n"
              + "      to_date(to_char(t.payment_date, 'dd.MM.yyyy'), 'dd.MM.yyyy')\n"
              + "\n"
              + "  and to_date(to_char(now(), 'dd.MM.yyyy'), 'dd.MM.yyyy') <>\n"
              + "      to_date(to_char(t.last_sent_time, 'dd.MM.yyyy'), 'dd.MM.yyyy')\n"
              + "\n"
              + "  and to_date(to_char(now()- INTERVAL '30 min', 'HH:MM'), 'HH:MM') <\n"
              + "      to_date(to_char(t.payment_time, 'HH:MM'), 'HH:MM')\n"
              + "\n"
              + "  and to_date(to_char(now() + INTERVAL '30 min', 'HH:MM'), 'HH:MM') <\n"
              + "      to_date(to_char(t.payment_time, 'HH:MM'), 'HH:MM');",
      nativeQuery = true)
  List<InvoiceTemplateEntity> findAllOneTime();

  @Query(
      value =
          "select * "
              + "from mail_order t\n"
              + "where t.subscription_state = 'ACTIVE'\n"
              + "and t.active='true'\n"
              + "and t.payment_type ='DAILY'\n"
              + "  and to_date(to_char(now(), 'dd.MM.yyyy'), 'dd.MM.yyyy') <>\n"
              + "      to_date(to_char(t.last_sent_time, 'dd.MM.yyyy'), 'dd.MM.yyyy')\n"
              + "  and to_date(to_char(now() - INTERVAL '30 min', 'HH:MM'), 'HH:MM') <=\n"
              + "      to_date(to_char(t.payment_time, 'HH:MM'), 'HH:MM')\n"
              + "  and to_date(to_char(now()+ INTERVAL '30 min', 'HH:MM'), 'HH:MM') >=\n"
              + "      to_date(to_char(t.payment_time, 'HH:MM'), 'HH:MM')",
      nativeQuery = true)
  List<InvoiceTemplateEntity> findAllDaily();

  @Query(
      value =
          "select * "
              + "from mail_order t\n"
              + "where t.subscription_state = 'ACTIVE'\n"
              + "and t.active='true'\n"
              + "and t.payment_type ='WEEKLY'\n"
              + "  and t.week_day = to_char(extract(isodow from now())-1, '0')\n"
              + "  and to_date(to_char(now(), 'dd.MM.yyyy'), 'dd.MM.yyyy') <>\n"
              + "      to_date(to_char(t.last_sent_time, 'dd.MM.yyyy'), 'dd.MM.yyyy')\n",
      nativeQuery = true)
  List<InvoiceTemplateEntity> findAllWeekly();
}
// tr.created_date >= NOW() - INTERVAL '6 MONTH' \n"
