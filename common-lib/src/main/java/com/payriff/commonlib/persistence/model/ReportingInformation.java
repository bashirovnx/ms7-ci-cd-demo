package com.payriff.commonlib.persistence.model;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ReportingInformation {

    private Long applicationId;
    private Long userId;
    private String applicationName;
    private Long walletId;
}
