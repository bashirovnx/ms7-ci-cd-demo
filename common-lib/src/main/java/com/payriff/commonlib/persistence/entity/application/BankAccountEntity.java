package com.payriff.commonlib.persistence.entity.application;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;

import javax.persistence.*;

/**
 * @author HasanovZK
 */
@Entity
@Data
@Table(name = "bank_account")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class BankAccountEntity extends Auditable<String> {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "bank_acc_generator")
    @SequenceGenerator(name = "bank_acc_generator", sequenceName = "bank_acc_seq", allocationSize = 1)
    private Long id;
    private boolean active = true;

    private String serviceUser;
    private String address;
    private String postalCode;
    private String voen;
    private String bankName;
    private String accountNumber;
    private String iban;
    private String code;
    private String bankVoen;
    private String swift;
    private String phone;
    @MapsId
    @OneToOne(fetch = FetchType.EAGER, optional = false, cascade = CascadeType.MERGE)
    @JoinColumn(name = "company_id", nullable = false)
    @JsonIgnore
    private CompanyEntity companyEntity;
}
