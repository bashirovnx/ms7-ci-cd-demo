package com.payriff.commonlib.persistence.repository;

import com.payriff.commonlib.persistence.entity.CurrencyEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CurrencyRepository extends JpaRepository<CurrencyEntity, Long> {}
