package com.payriff.commonlib.persistence.entity.application;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "contract_hash")
public class ContractHashEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "contract_hash_generator")
    @SequenceGenerator(name = "contract_hash_generator", sequenceName = "contract_hash_seq", allocationSize = 1)
    private Long id;

    private String hash;

    @Column(name = "pin_code")
    private String pinCode;

    private String fullname;

    private String address;

    private String status; //should be Enumerated string

    @Column(name = "create_date")
    private LocalDateTime createDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private UserEntity userEntity;

}
