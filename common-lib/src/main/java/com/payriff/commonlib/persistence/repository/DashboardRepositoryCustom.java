package com.payriff.commonlib.persistence.repository;


import com.payriff.commonlib.dto.admin.TransferDto;
import com.payriff.commonlib.dto.application.WalletResponseDto;
import com.payriff.commonlib.enums.PaymentStatus;
import com.payriff.commonlib.enums.ReportDateType;
import com.payriff.commonlib.persistence.entity.dashboard.MonthlyReportEntity;
import com.payriff.commonlib.persistence.entity.dashboard.ReportTransactionEntity;
import com.payriff.commonlib.persistence.entity.dashboard.SalesRadarEntity;
import com.payriff.commonlib.persistence.entity.dashboard.SalesReportEntity;
import com.payriff.commonlib.persistence.entity.processing.TransactionEntity;
import com.payriff.commonlib.util.CommonUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Stream;

@Repository
@Slf4j
@RequiredArgsConstructor
public class DashboardRepositoryCustom {

  private final JdbcTemplate jdbcTemplate;

  public List<MonthlyReportEntity> getMonthlyReport(List<Integer> statuses, Long userID) {

    String sql =
        "SELECT sub.* \n"
            + "  FROM (SELECT count(*) AS tran_count,  \n"
            + "               to_char(tr.created_date, 'dd/MM/yyyy') AS tran_date, \n"
            + "               tr.payment_status             \n"
            + "          FROM transaction tr         \n"
            + "         WHERE tr.payment_status IN (?, ?)   \n"
            + "           AND tr.created_date >= NOW() - INTERVAL '6 MONTH' \n"
            + "           AND tr.user_id = ? \n"
            + "         GROUP BY tr.payment_status, tran_date) sub \n"
            + " ORDER BY to_date(sub.tran_date, 'dd/MM/yyyy')      \n";

    Object[] parameters = Stream.concat(statuses.stream(), Stream.of(userID)).toArray();

    return jdbcTemplate.query(
        sql,
        parameters,
        (rs, rowNum) ->
            MonthlyReportEntity.builder()
                .tranCount(rs.getLong("tran_count"))
                .paymentStatus(PaymentStatus.of(rs.getInt("payment_status")))
                .tranDate(CommonUtil.getLocalDate(rs.getString("tran_date")))
                .build());
  }

  public List<SalesRadarEntity> getSalesRadar(Long userID) {

    String sql =
        "SELECT count(tr.payment_status) AS tran_count, tr.payment_status, ap.name \n"
            + "       FROM transaction tr \n"
            + " INNER JOIN application ap ON ap.id = tr.app_id \n"
            + "      WHERE tr.application_status=2 and tr.user_id = ? \n"
            + "   GROUP BY tr.payment_status, ap.name";

    Object[] parameters = Stream.of(userID).toArray();

    return jdbcTemplate.query(
        sql,
        parameters,
        (rs, rowNum) ->
            SalesRadarEntity.builder()
                .tranCount(rs.getLong("tran_count"))
                .paymentStatus(PaymentStatus.of(rs.getInt("payment_status")))
                .name(rs.getString("name"))
                .build());
  }

  public List<SalesReportEntity> getReportSalesBySpecificDate(
      LocalDate todayDate, LocalDate weekDate, LocalDate monthDate, Long userId) {

    String sql =
        "SELECT 'today' as type, COUNT(*)     \n"
            + "          FROM transaction tr      \n"
            + "         WHERE tr.payment_status IN (1)   \n"
            + "           AND tr.created_date >= ':today_date' \n"
            + "           AND tr.user_id = :user_id \n"
            + "union all \n"
            + "SELECT 'week' as type, COUNT(*)  \n"
            + "    FROM transaction tr   \n"
            + "   WHERE tr.payment_status IN (1)        \n"
            + "     AND tr.created_date >= ':week_date' \n"
            + "        AND tr.user_id = :user_id \n"
            + " union all \n"
            + "SELECT 'month' as type, COUNT(*)  \n"
            + "    FROM transaction tr    \n"
            + "   WHERE tr.payment_status IN (1)        \n"
            + "     AND tr.created_date >= ':month_date' \n"
            + "     AND tr.user_id = :user_id";

    sql =
        sql.replaceAll(":user_id", String.valueOf(userId))
            .replaceAll(":today_date", todayDate.toString())
            .replaceAll(":week_date", weekDate.toString())
            .replaceAll(":month_date", monthDate.toString());

    return jdbcTemplate.query(
        sql,
        (rs, rowNum) ->
            SalesReportEntity.builder()
                .type(ReportDateType.of(rs.getString("type")))
                .count(rs.getBigDecimal("count"))
                .build());
  }

  public List<ReportTransactionEntity> getTransactionReport() {

    String sql =
        "select count(tr.id)  as tran_count, \n"
            + "                     coalesce (sum(tr.amount), 0) as sum_amount,         \n"
            + "                     to_char(tr.created_date, 'dd/MM/yyyy') as to_date   \n"
            + "                     from transaction tr\n"
            + "                     where tr.kb_merchant_id = 'ES1090026' \n"
            + "                     and tr.application_status = 2 \n"
            + "                     and tr.payment_status = 1     \n"
            + "                     and   \n"
            + "                     (case \n"
            + "                         when cast(date_trunc('month', current_date) as date) = current_date \n"
            + "                         then tr.created_date >= (select cast(date_trunc('day', current_date - interval '1 month') as date)) \n"
            + "                         and tr.created_date < current_date\n"
            + "                         else tr.created_date >= cast(date_trunc('month', current_date) as date) \n"
            + "                         and tr.created_date < current_date  \n"
            + "                     end)  \n"
            + "                     group by to_char(tr.created_date, 'dd/MM/yyyy')";

    return jdbcTemplate.query(
        sql,
        (rs, rowNum) ->
            ReportTransactionEntity.builder()
                .count(rs.getString("tran_count"))
                .totalAmount(rs.getString("sum_amount"))
                .date(
                    LocalDate.parse(
                        rs.getString("to_date"), DateTimeFormatter.ofPattern("dd/MM/yyyy")))
                .build());
  }

  public List<WalletResponseDto> getWallets(Long userId) {

    String sql =
        "select w.id          as id,\n"
            + "       cf.direct_pay as direct_pay,\n"
            + "       app.id        as app_id,\n"
            + "       app.name      as app_name,\n"
            + "       cf.id as config_id,\n"
            + "       w.balance\n"
            + "from wallet w,\n"
            + "     global_config cf,\n"
            + "     application app\n"
            + "where app.config_id = cf.id\n"
            + "  and app.wallet_id = w.id\n"
            + "and app.status = 2 "
            + "  and cf.active = true\n"
            + "  and w.active = true\n"
            + "  and app.active = true\n"
            + "  and w.user_id = :user_id";

    sql = sql.replaceAll(":user_id", String.valueOf(userId));

    return jdbcTemplate.query(
        sql,
        (rs, rowNum) ->
            WalletResponseDto.builder()
                .id(rs.getLong("id"))
                .appId(rs.getLong("app_id"))
                .name(rs.getString("app_name"))
                .directPay(rs.getBoolean("direct_pay"))
                .balance(rs.getBigDecimal("balance"))
                .configId(rs.getLong("config_id"))
                .build());
  }

  public List<TransferDto> getTransfers() {

    String sql =
        "select t.app_id, a.name, sum(t.amount) as totalAmount, sum(t.paid_amount) as totalPaymentAmount\n"
            + "from transaction t,\n"
            + "     application a,\n"
            + "     attached_cards ac\n"
            + "where application_status = 2\n"
            + "  and a.id = t.app_id\n"
            + "  and ac.app_id <> a.id\n"
            + "  and payment_status = 1\n"
            + "group by a.name, t.app_id;";

    return jdbcTemplate.query(
        sql,
        (rs, rowNum) ->
            TransferDto.builder()
                .appId(rs.getLong("app_id"))
                .appName(rs.getString("name"))
                .totalAmount(rs.getBigDecimal("totalamount"))
                .totalPaymentAmount(rs.getBigDecimal("totalpaymentamount"))
                .build());
  }

  public List<TransactionEntity> getTransactions() {

    String sql =
        "select *\n"
            + "from transaction t\n"
            + "where t.application_status = 2\n"
            + "  and t.payment_status = 0\n"
            + "  and t.payment_source != 2\n"
            + "  and t.created_date::time >= (NOW() - INTERVAL '120 minutes')::time\n"
            + "  and t.active = true;";

    return jdbcTemplate.query(
        sql,
        (rs, rowNum) ->
            TransactionEntity.builder()
                .sessionId(rs.getString("session_Id"))
                .orderId(rs.getLong("order_id"))
                .build());
  }
}
