package com.payriff.commonlib.persistence.entity.security;

import com.payriff.commonlib.persistence.entity.application.Auditable;
import com.payriff.commonlib.persistence.entity.application.UserEntity;
import com.payriff.commonlib.persistence.entity.util.OperationType;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * @author HasanovZK
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "otp_sms")
public class OtpSmsEntity extends Auditable<String> {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "otp_sms_generator")
    @SequenceGenerator(name = "otp_sms_generator", sequenceName = "otp_sms_seq", allocationSize = 1)
    private long id;
    private String code;
    private boolean active;
    private LocalDateTime expireDate;
    private OperationType operationType;
    private String ip;
    private String phone;
    private String data;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private UserEntity userEntity;
}
