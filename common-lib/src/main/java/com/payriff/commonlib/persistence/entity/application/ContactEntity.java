package com.payriff.commonlib.persistence.entity.application;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * @author HasanovZK
 */
@Entity
@Setter
@Getter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Table(name = "contact")
public class ContactEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "contact_generator")
    @SequenceGenerator(name = "contact_generator", sequenceName = "contact_seq", allocationSize = 1)
    @EqualsAndHashCode.Include
    private Long id;
    private String name;
    private String lastname;
    private String mail;
    private String phone;
    @Column(columnDefinition = "TEXT")
    private String message;
}
