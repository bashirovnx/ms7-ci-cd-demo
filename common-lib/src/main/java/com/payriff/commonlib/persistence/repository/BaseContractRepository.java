package com.payriff.commonlib.persistence.repository;

import com.payriff.commonlib.persistence.entity.application.BaseContractEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface BaseContractRepository extends JpaRepository<BaseContractEntity, Long> {

    Optional<BaseContractEntity> findByAccountType(String accountType);

}
