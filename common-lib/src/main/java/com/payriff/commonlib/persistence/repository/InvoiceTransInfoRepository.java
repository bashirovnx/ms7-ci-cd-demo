package com.payriff.commonlib.persistence.repository;

import com.payriff.commonlib.persistence.entity.application.CustomerInvoiceEntity;
import com.payriff.commonlib.persistence.entity.application.InvoiceTransInfoEntity;
import com.payriff.commonlib.persistence.entity.processing.TransactionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface InvoiceTransInfoRepository extends JpaRepository<InvoiceTransInfoEntity, Long> {

    List<InvoiceTransInfoEntity> findByTransactionEntity(TransactionEntity transactionEntity);

    Optional<InvoiceTransInfoEntity> findByInvoiceEntity(CustomerInvoiceEntity customerInvoiceEntity);

}
