package com.payriff.commonlib.persistence.entity.application;

import com.payriff.commonlib.enums.EmanatInvoiceReqType;
import com.payriff.commonlib.enums.EmanatTransStatus;
import com.payriff.commonlib.persistence.entity.processing.TransactionEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "emanat_trans_info")
public class EmanatTransInfoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "emanat_trans_info_generator")
    @SequenceGenerator(name = "emanat_trans_info_generator", sequenceName = "emanat_trans_info_seq", allocationSize = 1)
    private Long id;

    @Enumerated(EnumType.STRING)
    private EmanatInvoiceReqType type;

    @Column(name = "paid_amount")
    private BigDecimal paidAmount;

    @Column(name = "invoice_code")
    private String invoiceCode;

    @Enumerated(EnumType.STRING)
    private EmanatTransStatus status;

    @Column(name = "back_rrn")
    private String backRrn;

    @Column(name = "create_date")
    private LocalDateTime createDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "invoice_id", referencedColumnName = "id")
    private CustomerInvoiceEntity invoiceEntity;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "transaction_id", referencedColumnName = "id")
    private TransactionEntity transactionEntity;

}
