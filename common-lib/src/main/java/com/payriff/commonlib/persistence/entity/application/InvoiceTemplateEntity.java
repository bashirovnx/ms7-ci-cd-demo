package com.payriff.commonlib.persistence.entity.application;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.payriff.commonlib.enums.*;
import com.payriff.commonlib.persistence.entity.BookingEntity;
import com.payriff.commonlib.persistence.entity.processing.ApplicationEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

/** @author HasanovZK */
@Entity
@Getter
@Setter
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@Table(name = "mail_order")
public class InvoiceTemplateEntity extends Auditable<String> {
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mailOrder_generator")
  @SequenceGenerator(
      name = "mailOrder_generator",
      sequenceName = "mail_order_seq",
      allocationSize = 1)
  private Long id;

  private Long bookingId;

  private String merchantId;
  private BigDecimal amount;
  private String fullName;
  private String email;
  private String phoneNumber;
  private String customMessage;
  private LocalDateTime expireDate;
  @Transient private PaymentStatus paymentStatus;

  @Enumerated(EnumType.STRING)
  private CurrencyType currencyType;

  @Enumerated(EnumType.STRING)
  private LanguageType languageType;

  @Enumerated(EnumType.STRING)
  private PaymentType paymentType;

  @Enumerated(EnumType.STRING)
  private SubscriptionState subscriptionState;

  private LocalDate paymentDate = LocalDate.now();
  private LocalTime paymentTime = LocalTime.now();

  @Enumerated(EnumType.STRING)
  private WeekDay weekDay;

  @Column(name = "dayOfMonth")
  private Integer dayOfMonth;

  private LocalDate expireDay;
  private LocalDateTime lastSentTime;
  private boolean active = true;
  private String description;
  private String approveURL;
  private String cancelURL;
  private String declineURL;
  private String uuid;
  String baseUrl;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "user_id")
  @JsonIgnore
  private UserEntity userEntity;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "app_id")
  @JsonIgnore
  private ApplicationEntity applicationEntity;

  @Transient private String appName;

  @OneToOne(mappedBy = "invoice", fetch = FetchType.LAZY)
  @JsonIgnore
  private BookingEntity booking;

  private InstallmentProductType installmentProductType;
  private Integer installmentPeriod;

  @Column(name = "direct_pay", columnDefinition = "false")
  private Boolean directPay;

  private Boolean sendSms = true;

  public String getAppName() {
    return this.applicationEntity.getName();
  }
}
