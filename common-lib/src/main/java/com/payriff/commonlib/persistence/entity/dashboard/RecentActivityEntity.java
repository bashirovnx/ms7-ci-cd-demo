package com.payriff.commonlib.persistence.entity.dashboard;

import com.payriff.commonlib.persistence.entity.application.UserEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "recent_activity")
public class RecentActivityEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "recent_activity_generator")
    @SequenceGenerator(name = "recent_activity_generator", sequenceName = "recent_activity_seq", allocationSize = 1)
    private Long id;

    @Column(name = "action")
    private String action;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private UserEntity userEntity;

    @Column(name = "action_date")
    private LocalDateTime actionDate;

}
