package com.payriff.commonlib.persistence.repository;


import com.payriff.commonlib.persistence.entity.application.CompanyEntity;
import com.payriff.commonlib.persistence.entity.application.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author HasanovZK
 */
@Repository
public interface CompanyRepository extends JpaRepository<CompanyEntity, Long> {

    Optional<CompanyEntity> findByIdAndUserEntityAndActive(Long id, UserEntity loggedUserEntity, boolean active);

    Optional<List<CompanyEntity>> findAllByUserEntityAndActive(UserEntity loggedUserEntity, boolean active);

    List<CompanyEntity> getAllByActiveTrue();

}
