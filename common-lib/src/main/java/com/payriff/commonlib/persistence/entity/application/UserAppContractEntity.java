package com.payriff.commonlib.persistence.entity.application;


import com.payriff.commonlib.persistence.entity.processing.ApplicationEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "user_app_contract")
public class UserAppContractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_app_contract_generator")
    @SequenceGenerator(name = "user_app_contract_generator", sequenceName = "user_app_contract_seq", allocationSize = 1)
    private Long id;

    private String contract;

    @Column(columnDefinition = "TEXT")
    private String contractContent;

    @Column(name = "signed_file")
    private String signedFile;

    @Column(name = "unsigned_file")
    private String unsignedFile;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private UserEntity userEntity;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "app_id")
    private ApplicationEntity applicationEntity;

    @Column(name = "account_type")
    private String accountType;

    private String status;

}
