package com.payriff.commonlib.persistence.repository;

import com.payriff.commonlib.persistence.entity.application.UserEntity;
import com.payriff.commonlib.persistence.entity.application.UserNotpEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Kanan
 */
@Repository
public interface UserNotpRepository extends JpaRepository<UserNotpEntity, Long> {

    boolean existsByUserAndActive(UserEntity userEntity, boolean isActive);

    UserNotpEntity findByUserAndActive(UserEntity userEntity, boolean isActive);
}
