package com.payriff.commonlib.persistence.entity.application;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author HasanovZK
 */
@Data
@AllArgsConstructor
public class AppAndIncomeEntity extends Auditable<String> {
    private String name;
    private BigDecimal total;
}
