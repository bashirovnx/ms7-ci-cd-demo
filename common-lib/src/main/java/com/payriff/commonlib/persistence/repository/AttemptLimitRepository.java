package com.payriff.commonlib.persistence.repository;

/**
 * @author HasanovZK
 */

import com.payriff.commonlib.persistence.entity.security.AttemptLimitEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

@Repository
public interface AttemptLimitRepository extends JpaRepository<AttemptLimitEntity, Long> {

    @Query(value = "select count(*) from attempt_limit where " +
                   "user_id= :userId and " +
                   "active=true and " +
                   "operation_type=:operationType and " +
                   "created_date >= :time",
           nativeQuery = true)
    int countAllBy(@Param("userId") Long userId,
                   @Param("operationType") String operationType,
                   @Param("time") LocalDateTime time);

    @Query(value = "select count(*) from attempt_limit where " +
            "user_id= :userId and " +
            "ip=:ip and " +
            "operation_type=:operationType and " +
            "active=true and " +
            "phone_number=:phone_number and " +
            "user_agent=:user_agent and " +
            "created_date >= :time ",
            nativeQuery = true)
    int countAllByUserAndUserAgent(@Param("userId") Long userId,
                                   @Param("ip") String ip,
                                   @Param("time") LocalDateTime time,
                                   @Param("operationType") String operationType,
                                   @Param("user_agent") String userAgent,
                                   @Param("phone_number") String phoneNumber
    );

    @Query(value = "select count(*) from attempt_limit where " +
            "ip=:ip and " +
            "operation_type=:operationType and " +
            "active=true and " +
            "user_agent=:user_agent and " +
            "created_date >= :time ",
            nativeQuery = true)
    int countAllByUserAgent(@Param("ip") String ip,
                            @Param("time") LocalDateTime time,
                            @Param("operationType") String operationType,
                            @Param("user_agent") String userAgent
    );

}
