package com.payriff.commonlib.persistence.entity.security;

import com.payriff.commonlib.persistence.entity.processing.ApplicationEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * @author HasanovZK
 */
@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "otp_hash")
public class OtpHashEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "otp_hash_generator")
    @SequenceGenerator(name = "otp_hash_generator", sequenceName = "otp_hash_seq", allocationSize = 1)
    private long id;
    private String hash;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "app_id")
    private ApplicationEntity applicationEntity;
}
