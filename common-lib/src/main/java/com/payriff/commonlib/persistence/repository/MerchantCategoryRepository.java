package com.payriff.commonlib.persistence.repository;

import com.payriff.commonlib.persistence.entity.MerchantCategoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author HasanovZK
 */
@Repository
public interface MerchantCategoryRepository extends JpaRepository<MerchantCategoryEntity, Long> {
    Optional<List<MerchantCategoryEntity>> findAllByActive(boolean active);

}
