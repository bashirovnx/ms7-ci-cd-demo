package com.payriff.commonlib.persistence.entity.processing;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * @author HasanovZK
 */
@Entity
@Setter
@Getter
@EqualsAndHashCode(callSuper = false)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@NoArgsConstructor
@Table(name = "merchant")
public class MerchantEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "merchant_generator")
    @SequenceGenerator(name = "merchant_generator", sequenceName = "merchant_seq", allocationSize = 1,
                       initialValue = 1090099)
    private Long id;
    private String merchantId;
    private Long appId;
    private boolean active;

    public MerchantEntity(MerchantEntity entity) {
        this.id = entity.getId();
        this.active = true;
        this.merchantId = "ES" + entity.getId();
        this.appId = entity.getAppId();
    }

}
