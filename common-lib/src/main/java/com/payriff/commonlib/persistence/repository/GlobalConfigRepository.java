package com.payriff.commonlib.persistence.repository;

/** @author HasanovZK */

import com.payriff.commonlib.persistence.entity.application.GlobalConfigEntity;
import com.payriff.commonlib.persistence.entity.application.UserEntity;
import com.payriff.commonlib.persistence.entity.processing.ApplicationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/** @author HasanovZK */
@Repository
public interface GlobalConfigRepository extends JpaRepository<GlobalConfigEntity, Long> {
  GlobalConfigEntity findByActiveTrueAndApplicationEntityAndUserEntity(
          ApplicationEntity applicationEntity, UserEntity userEntity);

  List<GlobalConfigEntity> findAllByActiveTrueAndUserEntity(UserEntity userEntity);
}
