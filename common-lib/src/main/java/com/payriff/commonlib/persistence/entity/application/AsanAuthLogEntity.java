package com.payriff.commonlib.persistence.entity.application;

import com.payriff.commonlib.persistence.entity.processing.ApplicationEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "asan_auth_log")
public class AsanAuthLogEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "asan_auth_generator")
    @SequenceGenerator(name = "asan_auth_generator", sequenceName = "asan_auth_seq", allocationSize = 1)
    private Long id;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "asan_user_id")
    private String asanUserId;

    @Column(name = "pin_code")
    private String pinCode;

    @Column(name = "tax_id")
    private String taxId;

    @Column(name = "wrong_pin_code")
    private String wrongPinCode;

    @Column(name = "transaction_id")
    private String transactionId;

    @Column(name = "verification_code")
    private String verificationCode;

    @Column(name = "sign_verification_code")
    private String signVerificationCode;

    @Column(name = "certificate_id")
    private String certificateId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private UserEntity userEntity;

    private String firstname;

    private String surname;

    private String organization;

    @Column(name = "organization_name")
    private String organizationName;

    @Column(name = "organization_code")
    private String organizationCode;

    private String role;

    @Column(name = "organization_system_name")
    private String informationSystemName;

    @Column(name = "cert_type")
    private String certType;

    private String status;

    @Column(name = "action_date")
    private LocalDateTime actionDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "application_id")
    private ApplicationEntity applicationEntity;

}
