package com.payriff.commonlib.persistence.repository;

import com.payriff.commonlib.enums.AccountType;
import com.payriff.commonlib.persistence.entity.application.UserEntity;
import com.payriff.commonlib.persistence.entity.processing.ApplicationEntity;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/** @author HasanovZK */
@Repository
public interface ApplicationRepository extends JpaRepository<ApplicationEntity, Long> {
  List<ApplicationEntity> findAllByActive(boolean active);

  Set<ApplicationEntity> findAllByActiveAndUserEntity(boolean active, UserEntity entity);

  List<ApplicationEntity> findAllByActiveAndUserEntityAndAccountType(
          boolean active, UserEntity entity, AccountType accountType);

  Optional<ApplicationEntity> findByAppKeyAndActive(String appKey, boolean active);

  Optional<ApplicationEntity> findByMerchantIdAndUserEntityAndActive(
      String merchantCode, UserEntity userEntity, boolean active);

  Optional<ApplicationEntity> findByIdAndActiveAndUserEntity(
      long id, boolean active, UserEntity entity);

  Optional<ApplicationEntity> findByIdAndActiveAndUserEntityIn(
      long id, boolean active, List<UserEntity> entities);

  Optional<ApplicationEntity> findByIdAndActiveTrue(long id);

  Optional<List<ApplicationEntity>> findAllByMerchantIdAndActive(String merchantId, boolean active);

  @EntityGraph(ApplicationEntity.APP_USERS_GRAPH)
  Optional<ApplicationEntity> findByActiveTrueAndId(Long id);

  Optional<ApplicationEntity> findByMerchantId(String id);

}
