package com.payriff.commonlib.persistence.repository;

import com.payriff.commonlib.persistence.entity.application.SubInvoiceEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SubInvoiceRepository extends JpaRepository<SubInvoiceEntity, Long> {
}
