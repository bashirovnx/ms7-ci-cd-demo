package com.payriff.commonlib.persistence.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;


@Data
@AllArgsConstructor
public class SuccessfulOnUsTransactionReport {

    private BigDecimal onUsAmountSum;
    private BigDecimal onUsPaidAmountSum;
    private Long onUsTransactionCount;
}
