package com.payriff.commonlib.persistence.entity.application;

import com.payriff.commonlib.enums.CardType;
import com.payriff.commonlib.enums.GatewayType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Table(name = "bin")
@Getter
@Setter
@ToString
@Entity
public class BinEntity extends Auditable<String> {
  @Id private String bin;
  private boolean active;
  private String bankName;
  private String cardProduct;

  @Enumerated(EnumType.STRING)
  private CardType cardType = CardType.KAPITALBANK;

  @Enumerated(EnumType.STRING)
  private GatewayType gatewayType = GatewayType.KAPITAL_BANK;
}
