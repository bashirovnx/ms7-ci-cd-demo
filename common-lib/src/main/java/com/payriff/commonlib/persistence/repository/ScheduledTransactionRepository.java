package com.payriff.commonlib.persistence.repository;

import com.payriff.commonlib.enums.CommonStatus;
import com.payriff.commonlib.persistence.entity.application.ScheduledTransactionEntity;
import com.payriff.commonlib.persistence.entity.processing.TransactionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;


@Repository
public interface ScheduledTransactionRepository extends JpaRepository<ScheduledTransactionEntity, Long> {

    List<ScheduledTransactionEntity> findByStatusAndScheduledTimeBefore(CommonStatus status, LocalDateTime dateTime);

    Optional<ScheduledTransactionEntity> findByTransactionId(TransactionEntity transactionEntity);

}
