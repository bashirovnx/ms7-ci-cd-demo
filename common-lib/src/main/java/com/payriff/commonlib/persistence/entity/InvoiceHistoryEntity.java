package com.payriff.commonlib.persistence.entity;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder
@ToString
public class InvoiceHistoryEntity {

    private String templateId;
    private String templateCreatedDate;
    private String invoiceId;
    private String invoiceCode;
    private String invoiceCreatedDate;
    private String transactionId;
    private String amount;
    private String paymentStatus;
    private String transactionDate;

}
