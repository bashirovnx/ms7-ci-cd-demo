package com.payriff.terminal.service;

import com.payriff.terminal.client.ekolizing.EkolizingClient;
import com.payriff.terminal.client.kainat.ProviderClient;
import com.payriff.terminal.client.kainat.model.Contract;
import com.payriff.terminal.client.kainat.model.request.PaymentRequest;
import com.payriff.terminal.client.lifttaxi.LiftTaxiClient;
import com.payriff.terminal.client.lifttaxi.model.request.CheckDriverRqModel;
import com.payriff.terminal.client.lifttaxi.model.request.IncreaseBalanceAileRqModel;
import com.payriff.terminal.client.lifttaxi.model.request.IncreaseBalanceLiftRqModel;
import com.payriff.terminal.client.payriff.PayriffClient;
import com.payriff.terminal.client.payriff.error.PayriffClientException;
import com.payriff.terminal.client.payriff.model.Application;
import com.payriff.terminal.client.payriff.model.BindTransaction;
import com.payriff.terminal.client.payriff.model.CustomerInvoice;
import com.payriff.terminal.client.payriff.model.Transaction;
import com.payriff.terminal.client.payriff.model.enums.*;
import com.payriff.terminal.domain.PaymentLogEntity;
import com.payriff.terminal.domain.ProviderEntity;
import com.payriff.terminal.domain.SubInvoiceEntity;
import com.payriff.terminal.domain.TerminalTransInfoEntity;
import com.payriff.terminal.dto.*;
import com.payriff.terminal.dto.request.InvoiceDetailRequestDto;
import com.payriff.terminal.dto.request.TerminalPayRequestDto;
import com.payriff.terminal.dto.response.InvoiceResponseDto;
import com.payriff.terminal.dto.response.TerminalPayResponseDto;
import com.payriff.terminal.error.TerminalException;
import com.payriff.terminal.mapper.InvoiceMapper;
import com.payriff.terminal.mapper.TerminalMapper;
import com.payriff.terminal.repository.PaymentLogRepository;
import com.payriff.terminal.repository.ProviderRepository;
import com.payriff.terminal.repository.SubInvoiceRepository;
import com.payriff.terminal.repository.TerminalTransInfoRepository;
import com.payriff.terminal.service.provider.PayonixProvider;
import com.payriff.terminal.util.CommonUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static com.payriff.terminal.dto.TerminalTransStatus.*;
import static com.payriff.terminal.error.TerminalException.TerminalCode;

@Service
@RequiredArgsConstructor
@Slf4j
public class TerminalService {

    private final HttpServletRequest request;
    private final PayriffClient payriffClient;
    private final TerminalTransInfoRepository terminalTransInfoRepository;
    private final SubInvoiceRepository subInvoiceRepository;
    private final ProviderClient providerClient;
    private final LiftTaxiClient liftTaxiClient;
    private final PayonixProvider payonixProvider;

    private final EkolizingClient ekolizingClient;
    private final ProviderRepository providerRepository;
    private final PaymentLogRepository paymentLogRepository;
    private final TerminalInvoiceReqType INPUT_TYPE_PAYRIFF = TerminalInvoiceReqType.INVOICE_CODE;

    @Value("${service.kainat.security.authorization}")
    private String kainatAuthorization;

    @Value("${service.lift-taxi.security.authorization}")
    private String liftTaxiAuthorization;

    @Value("${service.payriff.security.authorization}")
    private String payriffAuthorization;

    public ResponseEntity<InvoiceResponseDto> getInvoiceDetails(InvoiceDetailRequestDto request) {

        Thread.currentThread().setName(CommonUtils.getThreadName("check"));

        log.info("Check request param: " + request);

        TerminalInvoiceReqType type = request.getType();
        String value = request.getValue();

        String rrn = getRrnByClient(request.getRrn());

        ProviderEntity provider = providerBySource(request.getSrc());

        log.info("Provider : " + provider);

        validateInputType(rrn, provider, type);

        TerminalTransInfoEntity infoEntity = TerminalTransInfoEntity.builder()
                .type(type)
                .backRrn(rrn)
                .source(provider.getProviderSource())
                .status(PENDING)
                .createDate(LocalDateTime.now())
                .client(getClient())
                .retryCount(BigDecimal.ZERO.intValue())
                .build();

        log.info("Provider is: {}", provider);

        if (TerminalInvoiceReqType.INVOICE_CODE.equals(type)) {
            return detailsByCode(provider, infoEntity, value);
        } else {
            switch (Adapter.fromString(provider.getAdapterName())) {
                case EKOLIZING:
                    return ekolizingCheck(request, provider, infoEntity);
                case KAINAT:
                    return detailsByPin(provider, infoEntity, value);
                case PAYONIX:
                    return payonixProvider.check(request, provider, infoEntity);
                case LIFT_TAXI:
                    return liftTaxiCheck(provider, infoEntity, request.getValue());
                case AILE_TAXI:
                    return aileTaxiCheck(provider, infoEntity, request.getValue());
                default:
                    throw new TerminalException(rrn, TerminalCode.ADAPTER_NAME_NOT_FOUND,
                            "Adapter name is not found , adapter :" + provider.getAdapterName());
            }
        }
    }

    private ResponseEntity<InvoiceResponseDto> ekolizingCheck(InvoiceDetailRequestDto request, ProviderEntity provider,
                                                              TerminalTransInfoEntity transInfo) {
        request.setRrn(transInfo.getBackRrn());
        InvoiceResponseDto invoiceResponseDto = ekolizingClient.check(request);

        log.info("Ekolizing response is: {}", invoiceResponseDto);

        var application = payriffClient.findApplicationById(payriffAuthorization, provider.getAppId());
        log.info("Ekolizing Application: {}", application);

        CustomerInvoice customerInvoice = CustomerInvoice.builder()
                .amount(invoiceResponseDto.getAmount())
                .currencyType(CurrencyType.AZN)
                .description(invoiceResponseDto.getDescription())
                .email(invoiceResponseDto.getEmail())
                .phoneNumber(invoiceResponseDto.getPhoneNumber())
                .invoiceStatus(InvoiceStatus.PENDING)
                .source(InvoiceSource.TERMINAL)
                .applicationEntity(application.getId())
                .userEntity(application.getUserId())
                .uuid(UUID.randomUUID().toString())
                .invoiceUuid(UUID.randomUUID().toString())
                .paymentDay(LocalDate.now())
                .build();

        invoiceResponseDto.setStatus(TerminalResult.TerminalStatus.SUCCESS);

        CustomerInvoice invoice = payriffClient.createInvoice(payriffAuthorization, customerInvoice);
        log.info("Ekolziing CutomerInvoice: {}", invoice);

        transInfo.setInvoiceCode(invoice.getInvoiceCode());
        transInfo.setInvoiceEntity(invoice.getId());
        transInfo.setKainatContractId(request.getValue());

        terminalTransInfoRepository.save(transInfo);

        log.info("Created pending invoice :" + invoice.getId());

        return ResponseEntity.ok(invoiceResponseDto);
    }

    private ResponseEntity<InvoiceResponseDto> detailsByCode(ProviderEntity provider,
                                                             TerminalTransInfoEntity infoEntity,
                                                             String code) {
        CustomerInvoice invoice;
        try {
            invoice = payriffClient.findInvoiceByCode(payriffAuthorization, code)
                    .orElseThrow(
                            () -> new TerminalException(
                                    infoEntity.getBackRrn(),
                                    TerminalCode.INVOICE_NOT_FOUND,
                                    "Invoice details not found!, code: ".concat(code)
                            )
                    );
        } catch (PayriffClientException ex) {
            throw new TerminalException(
                    infoEntity.getBackRrn(),
                    TerminalCode.INVOICE_NOT_FOUND,
                    ex.getMessage().concat(", code: " + code)
            );
        }

        if (!InvoiceStatus.PENDING.equals(invoice.getInvoiceStatus())) {
            throw new TerminalException(
                    infoEntity.getBackRrn(),
                    TerminalCode.INVOICE_IS_NOT_PENDING,
                    "Invoice status is not pending. Status : " + invoice.getInvoiceStatus()
            );
        }

        infoEntity.setInvoiceCode(code);
        infoEntity.setInvoiceEntity(invoice.getId()); //modify

        terminalTransInfoRepository.save(infoEntity);

        validateSource(infoEntity, provider, invoice);

        String applicationName = invoice.getSenderCompany();

        BigDecimal maxAmount = getMaxAmount(invoice.getAmount());

        InvoiceResponseDto response = InvoiceMapper.INSTANCE.toInvoiceDto(invoice,
                infoEntity.getBackRrn(),
                applicationName,
                null,
                maxAmount
        );
        response.setStatus(TerminalResult.TerminalStatus.SUCCESS);

        if (Objects.isNull(response.getExpiryDate())) {
            response.setExpiryDate(LocalDateTime.now().plusDays(3L));
        }

        if (Objects.isNull(response.getCheckinDate())) {
            response.setCheckinDate(LocalDateTime.now().minusDays(3L));
        }

        return ResponseEntity.ok(response);
    }

    private void validateInputType(String rrn, ProviderEntity provider, TerminalInvoiceReqType type) {
        if (!provider.getInputs().contains(type.name())) {
            throw new TerminalException(
                    rrn,
                    TerminalCode.INVOICE_NOT_FOUND,
                    "Input type: " + type + " is incorrect, available types: " + provider.getInputs());
        }
    }

    private void validateSource(TerminalTransInfoEntity transInfo, ProviderEntity provider, CustomerInvoice invoice) {

        if (INPUT_TYPE_PAYRIFF.equals(transInfo.getType())) {
            log.info("Type is PayRiff. No need check source");
            return;
        }

        Long invoiceApplication = invoice.getApplicationEntity();
        Long providerApplication = provider.getAppId();

        log.info("Invoice application: {} , provider application: {}", invoiceApplication, providerApplication);

        if (!invoiceApplication.equals(providerApplication)) {
            throw new TerminalException(
                    transInfo.getBackRrn(),
                    TerminalCode.INVOICE_NOT_FOUND,
                    "Source is incompatible, invoice owner: " +
                            invoiceApplication + ", provider owner: " + providerApplication);
        }

//        InvoiceSource userSource = InvoiceSource.valueOf(provider.getUserSource());
//        log.info("User source: " + userSource);
//        if (!userSource.equals(invoice.getSource())) {
//            throw new TerminalException(
//                    rrn,
//                    TerminalCode.INVOICE_NOT_FOUND,
//                    "Source is incompatible, user: " + userSource + ", source: " + invoice.getSource()
//            );
//        }
    }

    private ResponseEntity<InvoiceResponseDto> liftTaxiCheck(ProviderEntity provider,
                                                             TerminalTransInfoEntity infoEntity,
                                                             String tabelNumber) {
        log.info("Tabel number: {}", tabelNumber);
        var clientResponse = liftTaxiClient.checkDriverLift(
                liftTaxiAuthorization,
                CheckDriverRqModel.builder().tabel(tabelNumber).build());

        var response = InvoiceResponseDto.builder()
                .senderCompany("Lift taxi")
                .fullName(clientResponse.getFullName())
                .currencyType(CurrencyType.AZN)
                .description(String.format("Tabel name: %s, full name: %s", tabelNumber, clientResponse.getFullName()))
                .backRrn(infoEntity.getBackRrn())
                .build();

        response.setStatus(TerminalResult.TerminalStatus.SUCCESS);
        var invoice = createLiftTaxiInvoice(provider, response);

        infoEntity.setInvoiceCode(invoice.getInvoiceCode());
        infoEntity.setInvoiceEntity(invoice.getId()); //modify
        infoEntity.setKainatContractId(tabelNumber);

        terminalTransInfoRepository.save(infoEntity);

        return ResponseEntity.ok(response);

    }

    private ResponseEntity<InvoiceResponseDto> aileTaxiCheck(ProviderEntity provider,
                                                             TerminalTransInfoEntity infoEntity,
                                                             String tabelNumber) {
        log.info("Aile taxi tabel number: {}", tabelNumber);
        var clientResponse = liftTaxiClient.checkDriverAile(
                liftTaxiAuthorization,
                CheckDriverRqModel.builder().tabel(tabelNumber).build());

        var response = InvoiceResponseDto.builder()
                .senderCompany("Aile taxi")
                .fullName(clientResponse.getFullName())
                .currencyType(CurrencyType.AZN)
                .description(String.format("Tabel name: %s, full name: %s", tabelNumber, clientResponse.getFullName()))
                .backRrn(infoEntity.getBackRrn())
                .build();

        response.setStatus(TerminalResult.TerminalStatus.SUCCESS);
        var invoice = createLiftTaxiInvoice(provider, response);

        infoEntity.setInvoiceCode(invoice.getInvoiceCode());
        infoEntity.setInvoiceEntity(invoice.getId()); //modify
        infoEntity.setKainatContractId(tabelNumber);

        terminalTransInfoRepository.save(infoEntity);

        return ResponseEntity.ok(response);

    }

//    private ResponseEntity<InvoiceResponseDto> detailsByDriverCode(ProviderEntity provider,
//                                                                   TerminalTransInfoEntity infoEntity,
//                                                                   String driverCode) {
//        log.info("Driver code: {}", driverCode);
//        var clientResponse = liftTaxiClient.checkDriverAile(liftTaxiAuthorization, driverCode);
//        var fullName = String.format("%s %s %s",
//                clientResponse.getResponse().getFirstname(),
//                clientResponse.getResponse().getLastname(),
//                clientResponse.getResponse().getMiddlename());
//
//        var response = InvoiceResponseDto.builder()
//                .senderCompany("Ailə taksi")
//                .fullName(fullName)
//                .currencyType(CurrencyType.AZN)
//                .description(String.format("Driver code: %s, full name: %s", driverCode, fullName))
//                .backRrn(infoEntity.getBackRrn())
//                .build();
//
//        response.setStatus(TerminalResult.TerminalStatus.SUCCESS);
//        var invoice = createLiftTaxiInvoice(provider, response);
//
//        infoEntity.setInvoiceCode(invoice.getInvoiceCode());
//        infoEntity.setInvoiceEntity(invoice.getId()); //modify
//        infoEntity.setKainatContractId(driverCode);
//
//        terminalTransInfoRepository.save(infoEntity);
//
//        return ResponseEntity.ok(response);
//
//    }

    private ResponseEntity<InvoiceResponseDto> detailsByPin(
            ProviderEntity provider,
            TerminalTransInfoEntity infoEntity,
            String pin
    ) {
        log.info("Pin code : " + pin);

        Optional<Contract> info = providerClient.getContracts(kainatAuthorization, pin)
                .getData()
                .stream()
                .findFirst();

        if (info.isPresent()) {
            Contract contract = info.get();

            BigDecimal maxAmount = getMaxAmount(contract.getMaxAmount());
            String fullname = contract.getFirstName()
                    .concat(" ")
                    .concat(contract.getLastName())
                    .concat(" ")
                    .concat(contract.getMiddleName());

            String description = "No:"
                    .concat(contract.getNo())
                    .concat(", ")
                    .concat(fullname)
                    .concat(", ")
                    .concat(contract.getBranch().getName())
                    .concat(", ")
                    .concat(contract.getSubDepartment().getName());

            InvoiceResponseDto response = InvoiceResponseDto.builder()
                    .senderCompany("Kainat Tedris Merkezi")
                    .fullName(fullname)
                    .amount(getAmount(contract.getCurrentDebt()))
                    .currencyType(CurrencyType.AZN)
                    .description(description)
                    .phoneNumber("+99425559070")
                    .email("info@kainat.edu.az")
                    .backRrn(infoEntity.getBackRrn())
                    .maxAmount(getAmount(maxAmount))
                    .build();

            response.setStatus(TerminalResult.TerminalStatus.SUCCESS);

            CustomerInvoice invoice = createKainatInvoice(contract, response);

            infoEntity.setKainatContractId(String.valueOf(contract.getId()));
            infoEntity.setKainatUid(contract.getOperationUid());
            infoEntity.setInvoiceCode(invoice.getInvoiceCode());
            infoEntity.setInvoiceEntity(invoice.getId()); //modify

            terminalTransInfoRepository.save(infoEntity);

            return ResponseEntity.ok(response);
        } else {
            throw new TerminalException(
                    infoEntity.getBackRrn(),
                    TerminalCode.INVOICE_NOT_FOUND,
                    "Invoice details not found!"
            );
        }

    }

    private BigDecimal getMaxAmount(BigDecimal amount) {
        return BigDecimal.valueOf(Math.ceil(amount.doubleValue() / 10) * 10);
    }

    public ResponseEntity<TerminalPayResponseDto> pay(TerminalPayRequestDto request) {

        Thread.currentThread().setName(CommonUtils.getThreadName("pay"));

        log.info("Pay request param: " + request);

        Long paymentID = paymentLog(request);

        String backRrn = request.getBackRrn();

        TerminalTransInfoEntity terminalTransInfo = terminalTransInfoRepository.findByBackRrnAndStatus(backRrn, PENDING)
                .orElseThrow(
                        () -> new TerminalException(
                                backRrn,
                                TerminalCode.PENDING_TRANSACTION_NOT_FOUND,
                                "Pending transaction not found! By rrn :" + backRrn
                        ));

        changeStatusToAccapted(terminalTransInfo, paymentID);

        List<CustomerInvoice> invoices = payriffClient.findInvoiceByCode(payriffAuthorization, terminalTransInfo.getInvoiceCode())
                .stream()
                .filter(x -> InvoiceStatus.PENDING.equals(x.getInvoiceStatus()))
                .collect(Collectors.toList());

        boolean isPartial = invoices.stream().anyMatch(x -> InvoiceStatus.PARTIAL.equals(x.getInvoiceStatus()));

        log.info("Partial invoice : {}", isPartial);

        if (invoices.size() == 0) {
            throw new TerminalException(
                    backRrn,
                    TerminalCode.INVOICE_ALREDAY_COMPLETED,
                    "This invoice already completed!"
            );
        }

        CustomerInvoice invoice = payriffClient.findInvoiceById(payriffAuthorization, terminalTransInfo.getInvoiceEntity()); //modify

        if (!Objects.isNull(invoice.getExpireDay()) && invoice.getExpireDay().isAfter(LocalDate.now())) {
            throw new TerminalException(
                    backRrn,
                    TerminalCode.INVOICE_EXPIRED,
                    "Invoice expired!"
            );
        }

        setContext(invoice);

        BigDecimal paidAmount = request.getAmount();
        BigDecimal invoiceAmount = invoice.getAmount();

        if (Objects.isNull(invoiceAmount)) {
            log.info("Invoice amount is null, set amount");
            invoiceAmount = paidAmount;
        }

        String providerSource = terminalTransInfo.getSource();

        log.info("Find application by id: " + invoice.getApplicationEntity());

        Application application = payriffClient.findApplicationById(payriffAuthorization, invoice.getApplicationEntity()); //modify

        if (paidAmount.compareTo(invoiceAmount) > 0) {
            greaterAmountMakeTransaction(
                    application,
                    terminalTransInfo,
                    invoice,
                    paidAmount,
                    providerSource
            );
        } else if (paidAmount.compareTo(invoiceAmount) < 0) {
            terminalTransInfo = lessAmountMakeTransaction(
                    application,
                    terminalTransInfo,
                    invoice,
                    paidAmount,
                    providerSource
            );
        } else {
            equalAmountMakeTransaction(
                    application,
                    terminalTransInfo,
                    paidAmount,
                    invoice
            );
        }

        changePaymentLogStatus(paymentID, terminalTransInfo);

        ResponseEntity<TerminalPayResponseDto> response = getTransactionInfo(terminalTransInfo, paidAmount);

        InvoiceSource source = invoice.getSource();

        notifyBySource(
                terminalTransInfo,
                source,
                providerSource,
                response.getBody().getAmount(),
                response.getBody().getTransStatus()
        );

        return response;

    }


    public ResponseEntity<TerminalPayResponseDto> getTransactionInfo(@PathVariable("rrn") String rrn) {

        Thread.currentThread().setName(CommonUtils.getThreadName("status"));

        List<TerminalTransInfoEntity> list = terminalTransInfoRepository.findByBackRrn(rrn);


        TerminalTransInfoEntity transInfo;

        if (!Objects.isNull(list) && list.size() > 0) {

            BigDecimal totalAmount = list.stream()
                    .filter(x -> COMPLETE.equals(x.getStatus()))
                    .map(TerminalTransInfoEntity::getPaidAmount) // modify
                    .reduce(BigDecimal.ZERO, BigDecimal::add);

            transInfo = list.stream()
                    .max(Comparator.comparing(TerminalTransInfoEntity::getId))
                    .orElseThrow(
                            () -> new TerminalException(rrn, TerminalCode.TRANSACTION_NOT_FOUND,
                                    "Any transaction data not found! By rrn :" + rrn)
                    );

            return getTransactionInfo(transInfo, totalAmount);
        } else {
            throw new TerminalException(rrn, TerminalCode.TRANSACTION_NOT_FOUND,
                    "Transaction data not found! By rrn :" + rrn);
        }

    }

    public ResponseEntity<TerminalResult> notify(NotifyDto notifyDto) {

        Thread.currentThread().setName(CommonUtils.getThreadName("notify"));

        //Create Customer Invoice
        CustomerInvoice customerInvoice = this.createInvoice(notifyDto);

        // Getting invoice detail and prepare for call pay api.
        InvoiceDetailRequestDto invoiceDetailRequestDto = InvoiceDetailRequestDto.builder()
                .value(customerInvoice.getInvoiceCode())
                .type(TerminalInvoiceReqType.INVOICE_CODE)
                .src(notifyDto.getSrc())
                .build();

        ResponseEntity<InvoiceResponseDto> invoiceDetails = this.getInvoiceDetails(invoiceDetailRequestDto);

        // Call pay api.
        TerminalPayRequestDto terminalPayRequestDto = TerminalPayRequestDto.builder()
                .amount(notifyDto.getAmount())
                .src(notifyDto.getSrc())
                .backRrn(invoiceDetails.getBody().getBackRrn())
                .build();

        this.pay(terminalPayRequestDto);

        return ResponseEntity.ok(new TerminalResult(
                TerminalResult.TerminalStatus.SUCCESS,
                HttpStatus.OK.name(),
                "Successfully completed"
        ));
    }

    private CustomerInvoice createInvoice(NotifyDto notifyDto) {

        // Getting Applciation and USer by Provider source
        ProviderEntity providerEntity = this.providerBySource(notifyDto.getSrc());

        Application application = payriffClient.findApplicationById(payriffAuthorization, providerEntity.getAppId());

        CustomerInvoice customerInvoice = CustomerInvoice.builder()
                .amount(notifyDto.getAmount())
                .currencyType(CurrencyType.AZN)
                .description(notifyDto.getDescription())
                .phoneNumber(notifyDto.getPhoneNumber())
                .fullName(notifyDto.getName())
                .invoiceStatus(InvoiceStatus.PENDING)
                .source(InvoiceSource.TERMINAL)
                .uuid(UUID.randomUUID().toString())
                .invoiceUuid(UUID.randomUUID().toString())
                .paymentDay(LocalDate.now())
                .applicationEntity(application.getId())
                .userEntity(application.getUserId())
                .invoiceCodeExists(Boolean.TRUE)
                .build();

        return payriffClient.createInvoice(payriffAuthorization, customerInvoice);
    }

    private ResponseEntity<TerminalPayResponseDto> getTransactionInfo(TerminalTransInfoEntity transInfo,
                                                                      BigDecimal amount) {

        log.info("Return transaction info. Rrn : {}", transInfo.getBackRrn());

        TerminalPayResponseDto response;

        Transaction transaction = payriffClient.findTransactionById(payriffAuthorization, transInfo.getTransactionEntity());

        if (Objects.isNull(transaction)) {
            response = TerminalPayResponseDto.builder()
                    .invoiceCode(transInfo.getInvoiceCode())
                    .transStatus(transInfo.getStatus())

                    .build();
        } else {
            response = TerminalPayResponseDto.builder()
                    .invoiceCode(transInfo.getInvoiceCode())
                    .transStatus(transInfo.getStatus())
                    .transactionDate(transaction.getCreatedDate())
                    .partial(Boolean.FALSE)
                    .amount(amount)
                    .note("Thanks use PayRiff. Karabakh is Azerbaijan!")
                    .build();
        }

        response.setStatus(TerminalResult.TerminalStatus.SUCCESS);

        return ResponseEntity.ok(response);

    }

    private Transaction terminalCreditOperation(
            TerminalTransInfoEntity transInfo,
            String uuid,
            Application application,
            BigDecimal amount,
            String source
    ) {

        log.info("Terminal credit operation. App : {}, Application Id : {}, Amount : {}",
                application.getName(),
                application.getId(),
                amount.doubleValue()
        );

        String rrn = transInfo.getBackRrn();

        Optional<Transaction> transactionEntity = Optional.empty();

        if (application.getStatus() == ApplicationStatus.PROD) {

            CalculateAmount calculateAmount = calculateAmount(transInfo, amount, application, source);

            Transaction transaction = Transaction
                    .builder()
                    .applicationId(application.getId())
                    .amount(amount)
                    .paidAmount(calculateAmount.getPaidAmount())
                    .commissionRate(calculateAmount.getPercentage())
                    .payriffAmount(calculateAmount.getCommission())
                    .cardBrand(getCardBrand())
                    .currencyType(CurrencyType.AZN)
                    .description("Payment from terminal: " + getClient())
                    .paymentStatus(PaymentStatus.CREATED)
                    .paymentSource(PaymentSource.NORMAL)
                    .uuid(uuid)
                    .rrn(rrn)
                    .orderId(transInfo.getPaymentId())
                    .sessionId(Thread.currentThread().getName())
                    .responseDescription("Accepted payment from terminal")
                    .orderLanguage(LanguageType.EN)
                    .build();

            transaction = payriffClient.createTransaction(payriffAuthorization, transaction);

            log.info("Create transaction for deposit terminal wallet, Transaction ID: {}", transaction.getId());

            PaymentStatus paymentStatus = deposit(transaction, source);

            transaction.setPaymentStatus(paymentStatus);

            transaction = payriffClient.updateTransaction(payriffAuthorization, transaction);

            log.info("Updated transaction payment status: {}", paymentStatus);

            transactionEntity = Optional.of(transaction);

        }

        return transactionEntity.orElseThrow(
                () -> new TerminalException(
                        rrn,
                        TerminalCode.TRANSACTION_NOT_CREATED,
                        "Transaction not created!"
                )
        );

    }

    private CalculateAmount calculateAmount(TerminalTransInfoEntity transInfo,
                                            BigDecimal amount,
                                            Application application,
                                            String source
    ) {
        if (TerminalInvoiceReqType.INVOICE_CODE.equals(transInfo.getType())) {
            try {
                return calculateAmountByApplication(amount, application);
            } catch (Exception ex) {
                log.warn("Custom comission not found. Return default");
            }
        }

        return calculateAmountBySource(amount, source);

    }

    private PaymentStatus deposit(Transaction transaction, String source) {

        boolean deposit = this.providerBySource(source).isDeposit();

        PaymentStatus status;
        if (deposit) {
            status = payriffClient.deposit(payriffAuthorization, transaction);
            log.info("Deposit is true. Deposit wallet balance, status: " + status);
        } else {
            status = PaymentStatus.APPROVED;
            log.info("Deposit is false. Only informative transaction");
        }

        return status;
    }

    private CalculateAmount calculateAmountBySource(BigDecimal totalAmount, String src) {

        log.info("Calculate comission by source: " + src);

        BigDecimal percentage = providerBySource(src).getCommission();

        BigDecimal commissionAmount = totalAmount
                .multiply(percentage)
                .divide(new BigDecimal(100), RoundingMode.HALF_UP);

        log.info("Commission percentage: {} , amount: {}", percentage, commissionAmount);

        BigDecimal paidAmount = totalAmount.subtract(commissionAmount)
                .setScale(2, RoundingMode.HALF_UP);

        log.info("Paid amount : " + paidAmount);

        return CalculateAmount.builder()
                .percentage(percentage)
                .commission(commissionAmount)
                .paidAmount(paidAmount)
                .build();
    }

    private CalculateAmount calculateAmountByApplication(BigDecimal totalAmount, Application application) {

        log.info("Calculate comission by app id: " + application.getId());

        BigDecimal percentage = providerByApplication(application).getCommission();

        BigDecimal commissionAmount = totalAmount
                .multiply(percentage)
                .divide(new BigDecimal(100), RoundingMode.HALF_UP);

        log.info("Commission percentage: {} , amount: {}", percentage, commissionAmount);

        BigDecimal paidAmount = totalAmount.subtract(commissionAmount)
                .setScale(2, RoundingMode.HALF_UP);

        log.info("Paid amount : " + paidAmount);

        return CalculateAmount.builder()
                .percentage(percentage)
                .commission(commissionAmount)
                .paidAmount(paidAmount)
                .build();
    }

    private void bindTransaction(Transaction transactionEntity,
                                 TerminalTransInfoEntity terminalTransInfoEntity
    ) {
        log.info("Binding transaction to terminal trans info. Transaction ID :{} , Terminal Trans ID :{}",
                transactionEntity.getId(),
                terminalTransInfoEntity.getId()
        );

        terminalTransInfoEntity.setPaidAmount(transactionEntity.getAmount());
        terminalTransInfoEntity.setStatus(COMPLETE);
        terminalTransInfoEntity.setTransactionEntity(transactionEntity.getId());

        terminalTransInfoRepository.save(terminalTransInfoEntity);

        BindTransaction link = BindTransaction.builder()
                .transactionId(transactionEntity.getId())
                .invoiceId(terminalTransInfoEntity.getInvoiceEntity())
                .build();

        try {
            Boolean isLinked = payriffClient.bindTransaction(payriffAuthorization, link);

            log.info("Linked : " + isLinked);
        } catch (Exception ex) {
            log.error("Error on bind transaction", ex);
        }

        try {
            Boolean isNotify = payriffClient.notify(payriffAuthorization, transactionEntity);

            log.info("Notify : " + isNotify);
        } catch (Exception ex) {
            log.error("Error on notify transaction", ex);
        }

    }

    private void greaterAmountMakeTransaction(Application application,
                                              TerminalTransInfoEntity transInfo,
                                              CustomerInvoice invoice,
                                              BigDecimal paidAmount,
                                              String src
    ) {

        log.info("Request amount : {} greater than invoice amount : {}", paidAmount, invoice.getAmount());

        completeParentInvoice(application, invoice, transInfo, src);

        completeSubInvoice(application, paidAmount, invoice, transInfo, src);

    }

    private TerminalTransInfoEntity partialSubInvoice(Application application,
                                                      BigDecimal paidAmount,
                                                      CustomerInvoice parentInvoice,
                                                      TerminalTransInfoEntity parentTerminalTransInfo,
                                                      String src
    ) {

        CustomerInvoice subInvoice = InvoiceMapper.INSTANCE.cloneInvoiceEntity(parentInvoice);

        subInvoice.setAmount(paidAmount);
        subInvoice.setPaymentDay(LocalDate.now());

        subInvoice.setInvoiceUuid(parentInvoice.getInvoiceUuid());
        subInvoice.setUuid(parentTerminalTransInfo.getBackRrn());

        subInvoice.setInvoiceStatus(InvoiceStatus.PENDING);
        subInvoice.setInvoiceCodeExists(Boolean.TRUE);

        subInvoice = payriffClient.createInvoice(payriffAuthorization, subInvoice);

        log.info("1. Debug save invoice ID: " + subInvoice.getId());

        SubInvoiceEntity link = SubInvoiceEntity.builder()
                .parentId(parentInvoice.getId()) //modify
                .subId(subInvoice.getId()) //modify
                .createDate(LocalDateTime.now())
                .build();

        subInvoiceRepository.save(link);

        log.info("Created sub invoice : {} by paid amount : {} and link to parent invoice. Link id :{}",
                subInvoice.getId(), paidAmount, link.getId());

        String description = "Invoice code : " + parentTerminalTransInfo.getInvoiceCode() + " partial paid!";

        Transaction subTransaction = terminalCreditOperation(
                parentTerminalTransInfo,
                parentInvoice.getInvoiceUuid(),
                application,
                paidAmount,
                src
        );

        subInvoice.setInvoiceStatus(InvoiceStatus.COMPLETE);
        subInvoice.setInvoiceUuid(parentInvoice.getInvoiceUuid());
        subInvoice.setUuid(parentTerminalTransInfo.getBackRrn());
        subInvoice.setInvoiceCodeExists(Boolean.TRUE);

        Long invoiceID = payriffClient.updateInvoice(payriffAuthorization, subInvoice).getId();

        log.info("2. Debug save invoice ID: " + invoiceID);

        TerminalTransInfoEntity subTerminalTransInfo = TerminalMapper.INSTANCE.cloneTerminalTransInfoEntity(parentTerminalTransInfo);
        subTerminalTransInfo.setCreateDate(LocalDateTime.now());
        subTerminalTransInfo.setTransactionEntity(subTransaction.getId()); //modify
        subTerminalTransInfo.setInvoiceEntity(subInvoice.getId()); //modify
        subTerminalTransInfo.setPaidAmount(paidAmount);

        bindTransaction(subTransaction, subTerminalTransInfo);

        log.info("Created and complete sub invoice id : {} , and bindig to sub trans info id :{}", subInvoice.getId(),
                subTerminalTransInfo.getId());

        return subTerminalTransInfo;
    }

    private void partialParentInvoice(CustomerInvoice parentInvoice,
                                      BigDecimal paidAmount,
                                      TerminalTransInfoEntity transInfo) {
        BigDecimal remainingAmount = parentInvoice.getAmount().subtract(paidAmount);

        parentInvoice.setPaymentStatus(PaymentStatus.PARTIAL);
        parentInvoice.setInvoiceStatus(InvoiceStatus.PARTIAL);

        Long invoiceID = payriffClient.updateInvoice(payriffAuthorization, parentInvoice).getId();

        log.info("3. Debug save invoice ID: " + invoiceID);

        transInfo.setPaidAmount(remainingAmount);

        transInfo.setStatus(TerminalTransStatus.PARTIAL);

        terminalTransInfoRepository.save(transInfo);

        log.info("Change parent invoice and trans info status to PARTIAL");

        CustomerInvoice partialInvoice = InvoiceMapper.INSTANCE.cloneInvoiceEntity(parentInvoice);

        partialInvoice.setAmount(remainingAmount);
        partialInvoice.setPaymentDay(LocalDate.now());
        partialInvoice.setInvoiceStatus(InvoiceStatus.PENDING);
        partialInvoice.setInvoiceUuid(parentInvoice.getInvoiceUuid());
        partialInvoice.setUuid(transInfo.getBackRrn());
        partialInvoice.setInvoiceCodeExists(Boolean.TRUE);

        partialInvoice = payriffClient.createInvoice(payriffAuthorization, partialInvoice);

        log.info("4. Debug save invoice ID: " + partialInvoice.getId());

        log.info("Create partial invoice for next pay. Invoice id : {}", partialInvoice.getId());

    }

    private void completeParentInvoice(Application application,
                                       CustomerInvoice parentInvoice,
                                       TerminalTransInfoEntity parentTerminalTransInfo,
                                       String src
    ) {

        String description = "Invoice code : " + parentTerminalTransInfo.getInvoiceCode() + " greater paid !. Part 1";

        Transaction parentTransaction = terminalCreditOperation(
                parentTerminalTransInfo,
                parentInvoice.getUuid(),
                application,
                parentInvoice.getAmount(),
                src
        );

        bindTransaction(parentTransaction, parentTerminalTransInfo);

        parentInvoice.setInvoiceStatus(InvoiceStatus.COMPLETE);
        parentInvoice.setInvoiceUuid(parentInvoice.getInvoiceUuid());
        parentInvoice.setUuid(parentTerminalTransInfo.getBackRrn());
        parentInvoice.setInvoiceCodeExists(Boolean.TRUE);

        Long invoiceID = payriffClient.createInvoice(payriffAuthorization, parentInvoice).getId();

        log.info("5. Debug save invoice ID: " + invoiceID);
        log.info("Paid and binding, change status to Complete parent invoice id : {}", parentInvoice.getId());
    }

    private void completeSubInvoice(Application application,
                                    BigDecimal paidAmount,
                                    CustomerInvoice parentInvoice,
                                    TerminalTransInfoEntity parentTerminalTransInfo,
                                    String src
    ) {

        BigDecimal remainingAmount = paidAmount.subtract(parentInvoice.getAmount());

        CustomerInvoice subInvoice = InvoiceMapper.INSTANCE.cloneInvoiceEntity(parentInvoice);

        subInvoice.setAmount(remainingAmount);
        subInvoice.setPaymentDay(LocalDate.now());
        subInvoice.setUuid(parentTerminalTransInfo.getBackRrn());
        subInvoice.setInvoiceUuid(parentInvoice.getInvoiceUuid());

        subInvoice.setInvoiceStatus(InvoiceStatus.PENDING);
        subInvoice.setInvoiceCodeExists(Boolean.TRUE);

        subInvoice = payriffClient.createInvoice(payriffAuthorization, subInvoice);

        log.info("6. Debug save invoice ID: " + subInvoice.getId());

        SubInvoiceEntity link = SubInvoiceEntity.builder()
                .parentId(parentInvoice.getId()) //modify
                .subId(subInvoice.getId())  //modify
                .build();

        subInvoiceRepository.save(link);

        log.info("Created new invoice : {} by remaining amount : {} and link to parent invoice. Link id :{}",
                subInvoice.getId(), remainingAmount, link.getId());

        String description = "Invoice code : " + parentTerminalTransInfo.getInvoiceCode() +
                " greater paid !. Remaining amount Part 2";

        Transaction subTransaction = terminalCreditOperation(
                parentTerminalTransInfo,
                parentInvoice.getUuid(),
                application,
                subInvoice.getAmount(),
                src
        );

        TerminalTransInfoEntity subTerminalTransInfo = TerminalMapper.INSTANCE.cloneTerminalTransInfoEntity(parentTerminalTransInfo);
        subTerminalTransInfo.setCreateDate(LocalDateTime.now());
        subTerminalTransInfo.setTransactionEntity(subTransaction.getId());  //modify
        subTerminalTransInfo.setInvoiceEntity(subInvoice.getId());  //modify
        subTerminalTransInfo.setPaidAmount(subTransaction.getAmount());

        bindTransaction(subTransaction, subTerminalTransInfo);

        subInvoice.setInvoiceStatus(InvoiceStatus.COMPLETE);
        subInvoice.setInvoiceCodeExists(Boolean.TRUE);

        Long invoiceID = payriffClient.createInvoice(payriffAuthorization, subInvoice).getId();

        log.info("7. Debug save invoice ID: " + invoiceID);

        log.info("Created and complete sub invoice id : {} , and bindig to sub trans info id :{}", subInvoice.getId(),
                subTerminalTransInfo.getId());
    }

    private TerminalTransInfoEntity lessAmountMakeTransaction(Application application,
                                                              TerminalTransInfoEntity transInfo,
                                                              CustomerInvoice invoice,
                                                              BigDecimal paidAmount,
                                                              String src
    ) {

        log.info("Request amount : {} less than invoice amount : {}", paidAmount, invoice.getAmount());

        partialParentInvoice(invoice, paidAmount, transInfo);

        return partialSubInvoice(application, paidAmount, invoice, transInfo, src);
    }


    private void setContext(CustomerInvoice invoiceEntity) {
        /*requestContextProvider.get().setLoggedUserEntity(invoiceEntity.getUserEntity()); modify
        log.info("Set logged user  : {} from base invoice.", invoiceEntity.getUserEntity().getId());*/
    }

    private void equalAmountMakeTransaction(Application application,
                                            TerminalTransInfoEntity terminalTransInfo,
                                            BigDecimal paidAmount,
                                            CustomerInvoice invoice) {

        log.info("Request amount : {} equal invoice amount : {}", paidAmount, paidAmount);

        String description = "Invoice code : " + terminalTransInfo.getInvoiceCode() + " fully paid!";

        Transaction transaction = terminalCreditOperation(
                terminalTransInfo,
                invoice.getUuid(),
                application,
                paidAmount,
                terminalTransInfo.getSource()
        );

        bindTransaction(transaction, terminalTransInfo);

        invoice.setPaymentStatus(PaymentStatus.APPROVED);
        invoice.setAmount(paidAmount);
        invoice.setInvoiceStatus(InvoiceStatus.COMPLETE);

        Long invoiceID = payriffClient.updateInvoice(payriffAuthorization, invoice).getId();

        log.info("8. Debug save invoice ID: " + invoiceID);

        terminalTransInfo.setPaidAmount(paidAmount);

        terminalTransInfoRepository.save(terminalTransInfo);

    }

    private CustomerInvoice createLiftTaxiInvoice(ProviderEntity provider, InvoiceResponseDto invoiceResponse) {
        var application = payriffClient.findApplicationById(payriffAuthorization, provider.getAppId());
        log.info("Payriff application: {} by id: {} :", application, provider.getAppId());
        CustomerInvoice customerInvoice = CustomerInvoice.builder()
                .currencyType(CurrencyType.AZN)
                .description(invoiceResponse.getDescription())
                .invoiceStatus(InvoiceStatus.PENDING)
                .source(InvoiceSource.TERMINAL)
                .applicationEntity(application.getId())
                .userEntity(application.getUserId())
                .uuid(UUID.randomUUID().toString())
                .invoiceUuid(UUID.randomUUID().toString())
                .paymentDay(LocalDate.now())
                .build();

        CustomerInvoice response = payriffClient.createInvoice(payriffAuthorization, customerInvoice);
        log.info("Pending invoice for lift taxi: {} created ", response.getId());
        return response;
    }

    private CustomerInvoice createKainatInvoice(Contract contract, InvoiceResponseDto invoiceResponseDto) {

        String merchantId = contract.getBranch().getPrMerchant();

        log.info("Find payriff merchant by merchant id :" + merchantId);

        Application application = payriffClient.findApplicationByMerchantId(payriffAuthorization, merchantId);

        CustomerInvoice customerInvoice = CustomerInvoice.builder()
                .amount(invoiceResponseDto.getAmount())
                .currencyType(CurrencyType.AZN)
                .description(invoiceResponseDto.getDescription())
                .email(invoiceResponseDto.getEmail())
                .phoneNumber(invoiceResponseDto.getPhoneNumber())
                .invoiceStatus(InvoiceStatus.PENDING)
                .source(InvoiceSource.TERMINAL)
                .applicationEntity(application.getId())
                .userEntity(application.getUserId())
                .uuid(UUID.randomUUID().toString())
                .invoiceUuid(UUID.randomUUID().toString())
                .paymentDay(LocalDate.now())
                .build();

        CustomerInvoice response = payriffClient.createInvoice(payriffAuthorization, customerInvoice);

        log.info("Created pending invoice :" + response.getId());

        return response;

    }

    private BigDecimal getAmount(BigDecimal amount) {
        return amount.divide(new BigDecimal(100), 2, RoundingMode.HALF_UP);
    }

    private boolean notifyBySource(TerminalTransInfoEntity info,
                                   InvoiceSource invoiceSource,
                                   String providerSource,
                                   BigDecimal amount,
                                   TerminalTransStatus status) {

        log.info("Invoice source : " + invoiceSource);

        ProviderEntity provider = providerBySource(providerSource);

        if (Objects.isNull(provider.getAdapterName())) {
            log.info("Provider is not notifable");
            return Boolean.FALSE;
        }

        boolean isSuccess;

        try {
            log.info("Provider is: {}", provider);
            switch (Adapter.fromString(provider.getAdapterName())) {
                case EKOLIZING:
                    ekolizingPay(info, invoiceSource, amount, status);
                    break;
                case PAYONIX:
                    payonixProvider.pay(info, invoiceSource, amount, status);
                    break;
                case KAINAT:
                    makeKainatPayment(info, invoiceSource, amount, status);
                    break;
                case LIFT_TAXI:
                    makeLiftTaxiPayment(info, invoiceSource, amount, status);
                    break;
                case AILE_TAXI:
                    makeAileTaxiPayment(info, invoiceSource, amount, status);
                    break;
                default:
                    log.info("Provider is not notifable");
                    break;
            }

            isSuccess = Boolean.TRUE;

        } catch (Exception ex) {

            log.error("Error on notify client", ex);
            statusToRetry(info);
            isSuccess = Boolean.FALSE;
        }
        log.info("Notify status: {}", isSuccess);

        return isSuccess;
    }

    private void ekolizingPay(
            TerminalTransInfoEntity info,
            InvoiceSource invoiceSource,
            BigDecimal amount,
            TerminalTransStatus status
    ) {

        log.info("Notify pay for ekolizing");

        if (InvoiceSource.TERMINAL.equals(invoiceSource) &&
                TerminalTransStatus.COMPLETE.equals(status)) {

            TerminalPayRequestDto requestDto = TerminalPayRequestDto.builder()
                    .amount(amount)
                    .backRrn(info.getBackRrn())
                    .value(info.getKainatContractId())
                    .build();

            TerminalPayResponseDto responseDto = ekolizingClient.pay(requestDto);

            log.info("Ekolizing Pay API: {}", responseDto);
        }
    }

    private void statusToRetry(TerminalTransInfoEntity infoEntity) {
        TerminalTransInfoEntity info = terminalTransInfoRepository.findById(infoEntity.getId())
                .orElseThrow(
                        () -> new IllegalArgumentException("Info not found by id: " + infoEntity.getId())
                );

        info.setStatus(RETRY);
        info.setRetryCount(info.getRetryCount() + 1);

        info = terminalTransInfoRepository.save(info);

        log.info("Status to RETRY. ID: " + info.getId());

    }

    private void makeLiftTaxiPayment(TerminalTransInfoEntity info,
                                     InvoiceSource invoiceSource,
                                     BigDecimal amount,
                                     TerminalTransStatus status) {

        //TODO verify provider to be notifiable

        log.info("Notify pay for lift taxi");
        if (InvoiceSource.TERMINAL.equals(invoiceSource) &&
                TerminalTransStatus.COMPLETE.equals(status)) {

            var paymentRequest = IncreaseBalanceLiftRqModel.builder()
                    .tabel(info.getKainatContractId())
                    .amount(amount)
                    .paymentId(info.getBackRrn())
                    .build();

            liftTaxiClient.increaseBalanceLift(liftTaxiAuthorization, paymentRequest);
            log.info("Lift taxi balance increased for driver with tabel number: {}", info.getKainatContractId());
        }
    }

    private void makeAileTaxiPayment(TerminalTransInfoEntity info,
                                     InvoiceSource invoiceSource,
                                     BigDecimal amount,
                                     TerminalTransStatus status) {

        //TODO verify provider to be notifiable

        log.info("Notify pay for aile taxi");
        if (InvoiceSource.TERMINAL.equals(invoiceSource) &&
                TerminalTransStatus.COMPLETE.equals(status)) {

            var paymentRequest = IncreaseBalanceLiftRqModel.builder()
                    .tabel(info.getKainatContractId())
                    .amount(amount)
                    .paymentId(info.getBackRrn())
                    .build();

            liftTaxiClient.increaseBalanceAile(liftTaxiAuthorization, paymentRequest);
            log.info("Aile taxi balance increased for driver with tabel number: {}", info.getKainatContractId());
        }
    }

//    private void makeAileTaxiPayment(TerminalTransInfoEntity info,
//                                     InvoiceSource invoiceSource,
//                                     BigDecimal amount,
//                                     TerminalTransStatus status) {
//
//        //TODO verify provider to be notifiable
//
//        log.info("Notify pay for aile taxi");
//        if (InvoiceSource.TERMINAL.equals(invoiceSource) &&
//                TerminalTransStatus.COMPLETE.equals(status)) {
//
//            String txnId = info.getBackRrn().substring(0, 30);
//
//            var paymentRequest = IncreaseBalanceAileRqModel.builder()
//                    .driver_code(info.getKainatContractId())
//                    .amount(amount.toString())
//                    .txn_id(txnId)
//                    .build();
//
//            liftTaxiClient.increaseBalanceAile(liftTaxiAuthorization, paymentRequest);
//            log.info("Balance increased for driver with tabel number: {}", info.getKainatContractId());
//        }
//    }

    private void makeKainatPayment(TerminalTransInfoEntity info,
                                   InvoiceSource invoiceSource,
                                   BigDecimal amount,
                                   TerminalTransStatus status
    ) {

        log.info("Notify pay for kainat");

        if (Objects.isNull(info.getKainatUid())) {
            log.info("Provider is not notifiable");
            return;
        }

        if (InvoiceSource.TERMINAL.equals(invoiceSource) &&

                TerminalTransStatus.COMPLETE.equals(status)) {

            PaymentRequest paymentRequest = PaymentRequest.builder()
                    .amount(amount.multiply(new BigDecimal(100)))
                    .contractId(Long.valueOf(info.getKainatContractId()))
                    .operationUid(info.getKainatUid())
                    .build();

            providerClient.makePayment(kainatAuthorization, paymentRequest);

        }
    }

    private ProviderEntity providerBySource(String source) {

        return providerRepository.findByProviderSource(source)
                .orElseThrow(() -> {
                    log.error("Provider with {} name cannot be found", source);
                    throw new IllegalArgumentException("Provider with " + source + "name cannot be found");
                });
    }

    private ProviderEntity providerByApplication(Application application) {

        return providerRepository.findByAppId(application.getId())
                .orElseThrow(() -> {
                    log.error("Provider with {} id cannot be found", application.getId());
                    throw new IllegalArgumentException("Provider with " + application.getId() + " id cannot be found");
                });
    }

    private String getCardBrand() {
        return getClient().toUpperCase();
    }

    private String getClient() {
        String client = request.getHeader("X-Client");

        log.info("Client name: " + client);

        return client;
    }

    private String getRrnByClient(String clientRrn) {

        String client = getClient();

        final String CLIENT_KOMTEC = "komtec";

        String rrn;

        if (CLIENT_KOMTEC.equals(client)) {

            rrn = clientRrn;

            List<TerminalTransInfoEntity> checkRrn = terminalTransInfoRepository.findByBackRrn(rrn);

            if (!checkRrn.isEmpty()) {
                throw new TerminalException(rrn, TerminalException.TerminalCode.RRN_MUST_BE_UNIQUE, "Duplicate rrn: " + rrn);
            }

        } else {
            rrn = UUID.randomUUID().toString();
        }

        return rrn;
    }

    private Long paymentLog(TerminalPayRequestDto request) {

        try {
            PaymentLogEntity payment = PaymentLogEntity.builder()
                    .amount(request.getAmount())
                    .backRrn(request.getBackRrn())
                    .client(getClient())
                    .createDate(LocalDateTime.now())
                    .status(PaymentLogStatus.ACCEPTED)
                    .build();

            payment = paymentLogRepository.save(payment);

            log.info("Logging payment id: " + payment.getId());

            return payment.getId();
        } catch (Exception ex) {
            log.info("Error on payment data logging", ex);
        }

        return 0L;
    }

    private void changePaymentLogStatus(Long paymentID, TerminalTransInfoEntity info) {

        try {
            log.info(String.format("Change payment log status. Info ID: %s, status: %s",
                    info.getId(),
                    info.getStatus().name())
            );

            Optional<PaymentLogEntity> paymentLog = paymentLogRepository.findById(paymentID);

            if (paymentLog.isPresent()) {

                PaymentLogEntity entity = paymentLog.get();

                entity.setStatus(
                        PaymentLogStatus.getStatus(info.getStatus().name())
                );

                paymentLogRepository.save(entity);
            }

        } catch (Exception ex) {
            log.error("Error on change payment status", ex);
        }
    }

    private void changeStatusToAccapted(TerminalTransInfoEntity terminalTransInfoEntity, Long paymentID) {

        log.info(String.format("Info ID: %s, payment ID: %s", terminalTransInfoEntity.getId(), paymentID));

        try {
            terminalTransInfoEntity.setStatus(ACCEPTED);
            terminalTransInfoEntity.setPaymentId(paymentID);

            terminalTransInfoRepository.save(terminalTransInfoEntity);
        } catch (Exception ex) {
            log.error("Error on change status to accapted", ex);
        }
    }

    public boolean retryNotify(TerminalTransInfoEntity info) {

        log.info("Retry notify : " + info);

        return notifyBySource(
                info,
                InvoiceSource.TERMINAL,
                info.getSource(),
                info.getPaidAmount(),
                COMPLETE
        );
    }

}
