package com.payriff.terminal.service.provider;

import com.payriff.terminal.client.kainat.ProviderClient;
import com.payriff.terminal.client.model.request.ProviderCheckRequest;
import com.payriff.terminal.client.model.request.ProviderPayRequest;
import com.payriff.terminal.client.model.response.ProviderCheckResponse;
import com.payriff.terminal.client.model.response.ProviderPayResponse;
import com.payriff.terminal.client.payriff.PayriffClient;
import com.payriff.terminal.client.payriff.model.Application;
import com.payriff.terminal.client.payriff.model.CustomerInvoice;
import com.payriff.terminal.client.payriff.model.enums.InvoiceSource;
import com.payriff.terminal.client.payriff.model.enums.InvoiceStatus;
import com.payriff.terminal.domain.ProviderEntity;
import com.payriff.terminal.domain.TerminalTransInfoEntity;
import com.payriff.terminal.dto.CurrencyType;
import com.payriff.terminal.dto.TerminalTransStatus;
import com.payriff.terminal.dto.request.InvoiceDetailRequestDto;
import com.payriff.terminal.dto.response.InvoiceResponseDto;
import com.payriff.terminal.mapper.InvoiceMapper;
import com.payriff.terminal.repository.TerminalTransInfoRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@Service
@Slf4j
@RequiredArgsConstructor
public class PayonixProvider {

    private final PayriffClient payriffClient;
    private final ProviderClient providerClient;
    private final TerminalTransInfoRepository terminalTransInfoRepository;

    @Value("${service.kainat.security.authorization}")
    private String kainatAuthorization;

    @Value("${service.payriff.security.authorization}")
    private String payriffAuthorization;

    public ResponseEntity<InvoiceResponseDto> check(
            InvoiceDetailRequestDto request,
            ProviderEntity provider,
            TerminalTransInfoEntity transInfo
    ) {

        ProviderCheckRequest checkRequest = ProviderCheckRequest.builder()
                .value(request.getValue())
                .build();

        log.info("Payonix request is: {}", checkRequest);

        ProviderCheckResponse checkResponse = providerClient.payonixCheck(kainatAuthorization, checkRequest);

        InvoiceResponseDto invoiceResponseDto = InvoiceMapper.INSTANCE.toInvoiceDto(checkResponse);

        //set static values
        invoiceResponseDto.setSenderCompany("Payonix");
        invoiceResponseDto.setDescription("Payonix payment for: " + request.getValue());
        invoiceResponseDto.setPhoneNumber(request.getValue());
        invoiceResponseDto.setMaxAmount(new BigDecimal("10000"));
        invoiceResponseDto.setEmail("info@payriff.com");
        invoiceResponseDto.setExpiryDate(LocalDateTime.now().plusDays(1L));
        invoiceResponseDto.setBackRrn(transInfo.getBackRrn());

        log.info("Payonix response is: {}", invoiceResponseDto);

        Application application = payriffClient.findApplicationById(payriffAuthorization, provider.getAppId());

        log.info("Payyonix Application: {}", application);

        CustomerInvoice customerInvoice = CustomerInvoice.builder()
                .amount(invoiceResponseDto.getAmount())
                .currencyType(CurrencyType.AZN)
                .description(invoiceResponseDto.getDescription())
                .email(invoiceResponseDto.getEmail())
                .phoneNumber(invoiceResponseDto.getPhoneNumber())
                .invoiceStatus(InvoiceStatus.PENDING)
                .source(InvoiceSource.TERMINAL)
                .applicationEntity(application.getId())
                .userEntity(application.getUserId())
                .uuid(UUID.randomUUID().toString())
                .invoiceUuid(UUID.randomUUID().toString())
                .paymentDay(LocalDate.now())
                .build();

        CustomerInvoice invoice = payriffClient.createInvoice(payriffAuthorization, customerInvoice);

        log.info("Payonix customer invoice: {}", invoice);

        transInfo.setInvoiceCode(invoice.getInvoiceCode());
        transInfo.setInvoiceEntity(invoice.getId());
        transInfo.setValue(request.getValue());

        terminalTransInfoRepository.save(transInfo);

        log.info("Created pending invoice :" + invoice.getId());

        return ResponseEntity.ok(invoiceResponseDto);
    }

    public void pay(
            TerminalTransInfoEntity info,
            InvoiceSource invoiceSource,
            BigDecimal amount,
            TerminalTransStatus status
    ) {

        log.info("Notify pay for payonix");

        if (InvoiceSource.TERMINAL.equals(invoiceSource) &&
                TerminalTransStatus.COMPLETE.equals(status)) {

            ProviderPayRequest payRequest = ProviderPayRequest.builder()
                    .amount(amount)
                    .value(info.getValue())
                    .rrn(info.getBackRrn())
                    .build();

            ProviderPayResponse payResponse = providerClient.payonixPay(kainatAuthorization, payRequest);

            log.info("Payonix Pay API: {}", payResponse);
        }
    }

}
