package com.payriff.terminal;

import com.payriff.terminal.config.TerminalConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients(basePackages = "com.payriff.terminal")
@EnableConfigurationProperties({
        TerminalConfig.class
})
public class MsTerminalApplication {

    public static void main(String[] args) {
        SpringApplication.run(MsTerminalApplication.class, args);
    }
}
