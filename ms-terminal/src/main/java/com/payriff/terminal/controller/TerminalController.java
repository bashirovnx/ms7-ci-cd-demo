package com.payriff.terminal.controller;

import com.payriff.terminal.dto.TerminalResult;
import com.payriff.terminal.dto.NotifyDto;
import com.payriff.terminal.dto.request.TerminalPayRequestDto;
import com.payriff.terminal.dto.request.InvoiceDetailRequestDto;
import com.payriff.terminal.dto.response.TerminalPayResponseDto;
import com.payriff.terminal.dto.response.InvoiceResponseDto;
import com.payriff.terminal.service.TerminalService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("api/v1/terminal")
@RequiredArgsConstructor
public class TerminalController {

    private final TerminalService terminalService;

    @PostMapping("/invoice-details")
    public ResponseEntity<InvoiceResponseDto> getInvoiceDetails(@RequestBody InvoiceDetailRequestDto request) {
        return terminalService.getInvoiceDetails(request);
    }

    @PostMapping("/pay")
    public ResponseEntity<TerminalPayResponseDto> pay(@RequestBody @Valid TerminalPayRequestDto request) {
        return terminalService.pay(request);
    }

    @GetMapping("/transaction-info/{rrn}")
    public ResponseEntity<TerminalPayResponseDto> getTransactionInfo(@PathVariable("rrn") String rrn) {
        return terminalService.getTransactionInfo(rrn);
    }

    @PostMapping("/notify")
    public ResponseEntity<TerminalResult> notify(@RequestBody @Valid NotifyDto request) {
        return terminalService.notify(request);
    }
}
