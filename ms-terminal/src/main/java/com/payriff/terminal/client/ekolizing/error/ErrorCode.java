package com.payriff.terminal.client.ekolizing.error;

public enum ErrorCode {
    INTERNAL_ERROR,
    AUTHORIZATION_ERROR
}
