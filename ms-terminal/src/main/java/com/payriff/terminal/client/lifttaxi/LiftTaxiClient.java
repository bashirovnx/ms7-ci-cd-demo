package com.payriff.terminal.client.lifttaxi;

import com.payriff.terminal.client.lifttaxi.model.request.CheckDriverRqModel;
import com.payriff.terminal.client.lifttaxi.model.request.IncreaseBalanceAileRqModel;
import com.payriff.terminal.client.lifttaxi.model.request.IncreaseBalanceLiftRqModel;
import com.payriff.terminal.client.lifttaxi.model.response.CheckAileDriverRsModel;
import com.payriff.terminal.client.lifttaxi.model.response.CheckLiftDriverRsModel;
import com.payriff.terminal.client.lifttaxi.model.response.DriverRsModel;
import com.payriff.terminal.client.lifttaxi.model.response.IncreaseBalanceAileDriverRsModel;
import com.payriff.terminal.client.lifttaxi.model.response.IncreaseBalanceLiftRsModel;
import com.payriff.terminal.config.LiftTaxiFeignClientConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "adp-lift-taxi", contextId = "adp-lift-taxi", configuration = LiftTaxiFeignClientConfiguration.class)
public interface LiftTaxiClient {

    @PostMapping(path = "/api/v1/lift-taxi/check")
    CheckLiftDriverRsModel checkDriverLift(
            @RequestHeader("Authorization") String authorization,
            @RequestBody CheckDriverRqModel requestBody);

    @PostMapping(path = "/api/v1/lift-taxi/increase-balance")
    IncreaseBalanceLiftRsModel increaseBalanceLift(
            @RequestHeader("Authorization") String authorization,
            @RequestBody IncreaseBalanceLiftRqModel requestBody);

    @PostMapping(path = "/api/v1/aile-taxi/check")
    CheckLiftDriverRsModel checkDriverAile(
            @RequestHeader("Authorization") String authorization,
            @RequestBody CheckDriverRqModel requestBody);

    @PostMapping(path = "/api/v1/aile-taxi/increase-balance")
    IncreaseBalanceLiftRsModel increaseBalanceAile(
            @RequestHeader("Authorization") String authorization,
            @RequestBody IncreaseBalanceLiftRqModel requestBody);

//
//    @GetMapping(path = "/api/v1/aile-taxi/check")
//    DriverRsModel<CheckAileDriverRsModel> checkDriverAile(@RequestHeader("Authorization") String authorization,
//                                                          @RequestParam(name = "driver_code") String driverCode);
//
//    @PostMapping(path = "/api/v1/aile-taxi/increase-balance")
//    DriverRsModel<IncreaseBalanceAileDriverRsModel> increaseBalanceAile(
//            @RequestHeader("Authorization") String authorization,
//            @RequestBody IncreaseBalanceAileRqModel requestBody);
}
