package com.payriff.terminal.client.model.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProviderPayRequest {

    private String rrn;
    private String value;
    private BigDecimal amount;

}
