package com.payriff.terminal.client.payriff.model;

import com.payriff.terminal.client.payriff.model.enums.ApplicationStatus;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Application {

    private Long id;
    private String name;
    private ApplicationStatus status;
    private Long userId;

}
