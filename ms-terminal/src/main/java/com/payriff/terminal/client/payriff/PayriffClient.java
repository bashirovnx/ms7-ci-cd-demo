package com.payriff.terminal.client.payriff;

import com.payriff.terminal.client.payriff.model.Application;
import com.payriff.terminal.client.payriff.model.BindTransaction;
import com.payriff.terminal.client.payriff.model.CustomerInvoice;
import com.payriff.terminal.client.payriff.model.Transaction;
import com.payriff.terminal.client.payriff.model.enums.PaymentStatus;
import com.payriff.terminal.config.PayriffFeignClientConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@FeignClient(
        url = "${service.payriff.url}",
        name = "ms-terminal",
        contextId = "ms-terminal",
        configuration = PayriffFeignClientConfiguration.class
)
public interface PayriffClient {

    @GetMapping(path = "/v1/terminal/invoice/{code}/code")
    Optional<CustomerInvoice> findInvoiceByCode(@RequestHeader("Authorization") String authorization,
                                                @PathVariable("code") String code
    );

    @PostMapping(path = "/v1/terminal/transaction/{orderId}")
    Transaction findTansactionByOrderId(@RequestHeader("Authorization") String authorization,
                                        @PathVariable("orderId") Long orderId);

    @GetMapping(path = "/v1/terminal/application/{id}")
    Application findApplicationById(@RequestHeader("Authorization") String authorization,
                                    @PathVariable("id") Long id);

    @GetMapping(path = "/v1/terminal/application/{id}/merchant")
    Application findApplicationByMerchantId(@RequestHeader("Authorization") String authorization,
                                            @PathVariable("id") String id);

    @GetMapping(path = "/v1/terminal/invoice/{id}/id")
    CustomerInvoice findInvoiceById(@RequestHeader("Authorization") String authorization,
                                    @PathVariable("id") Long id);

    @GetMapping(path = "/v1/terminal/transaction/{id}")
    Transaction findTransactionById(@RequestHeader("Authorization") String authorization,
                                    @PathVariable("id") Long id);

    @PostMapping(path = "/v1/terminal/transaction/create")
    Transaction createTransaction(@RequestHeader("Authorization") String authorization,
                                  Transaction transaction);

    @PutMapping(path = "/v1/terminal/transaction/update")
    Transaction updateTransaction(@RequestHeader("Authorization") String authorization,
                                  Transaction transaction);

    @PostMapping(path = "/v1/terminal/invoice")
    CustomerInvoice createInvoice(@RequestHeader("Authorization") String authorization,
                                  CustomerInvoice customerInvoiceEntity);

    @PutMapping(path = "/v1/terminal/invoice")
    CustomerInvoice updateInvoice(@RequestHeader("Authorization") String authorization,
                                  CustomerInvoice customerInvoiceEntity);

    @PostMapping(path = "/v1/terminal/wallet/deposit")
    PaymentStatus deposit(@RequestHeader("Authorization") String authorization,
                          @RequestBody Transaction transaction);

    @PostMapping(path = "/v1/terminal/invoice/bind-transaction")
    Boolean bindTransaction(@RequestHeader("Authorization") String authorization,
                            BindTransaction bindTransaction);

    @PostMapping(path = "/v1/terminal/notify")
    Boolean notify(@RequestHeader("Authorization") String authorization,
                   @RequestBody Transaction transaction);

}
