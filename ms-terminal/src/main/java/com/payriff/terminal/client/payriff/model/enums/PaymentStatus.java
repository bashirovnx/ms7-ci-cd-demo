package com.payriff.terminal.client.payriff.model.enums;

import java.util.Arrays;
import java.util.LinkedHashSet;

/**
 * @author HasanovZK
 */
public enum PaymentStatus {

    CREATED(0),
    APPROVED(1),
    CANCELED(2),
    DECLINED(3),
    REFUNDED(4),
    PREAUTH_APPROVED(5),
    EXPIRED(6),
    REVERSE(7),
    PARTIAL_REFUND(8),
    PARTIAL(9);

    private int id;

    PaymentStatus(int id) {
        this.id = id;
    }

    public static PaymentStatus of(int id) {
        return Arrays.stream(PaymentStatus.values())
                .filter(status -> status.id == id)
                .findFirst()
                .orElse(null);
    }

    public static PaymentStatus of(String name) {
        return Arrays.stream(PaymentStatus.values())
                .filter(status -> status.name().equalsIgnoreCase(name))
                .findFirst()
                .orElse(null);
    }

    public static LinkedHashSet<PaymentStatus> getSalesRadarOrder() {

        return new LinkedHashSet<PaymentStatus>() {
            {
                add(PaymentStatus.APPROVED);
                add(PaymentStatus.REFUNDED);
                add(PaymentStatus.DECLINED);
                add(PaymentStatus.CREATED);
                add(PaymentStatus.CANCELED);
            }
        };
    }

    public int getId() {
        return id;
    }
}