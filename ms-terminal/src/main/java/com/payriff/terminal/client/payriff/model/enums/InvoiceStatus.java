package com.payriff.terminal.client.payriff.model.enums;

public enum InvoiceStatus {

    PENDING,
    ERROR,
    EXPIRED,
    PARTIAL,
    COMPLETE

}