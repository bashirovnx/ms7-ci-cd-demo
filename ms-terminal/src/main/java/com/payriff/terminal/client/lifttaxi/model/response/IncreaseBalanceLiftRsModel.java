package com.payriff.terminal.client.lifttaxi.model.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Builder
@Data
@NoArgsConstructor
public class IncreaseBalanceLiftRsModel {
    private String transactionId;
}
