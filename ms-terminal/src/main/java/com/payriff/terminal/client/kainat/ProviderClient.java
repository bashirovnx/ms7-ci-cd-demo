package com.payriff.terminal.client.kainat;

import com.payriff.terminal.client.kainat.model.Contract;
import com.payriff.terminal.client.kainat.model.request.PaymentRequest;
import com.payriff.terminal.client.kainat.model.response.GeneralResponse;
import com.payriff.terminal.client.kainat.model.response.PaymentResponse;
import com.payriff.terminal.client.model.request.ProviderCheckRequest;
import com.payriff.terminal.client.model.request.ProviderPayRequest;
import com.payriff.terminal.client.model.response.ProviderCheckResponse;
import com.payriff.terminal.client.model.response.ProviderPayResponse;
import com.payriff.terminal.config.ProviderFeignClientConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(
        name = "adp-provider",
        contextId = "adp-provider",
        configuration = ProviderFeignClientConfiguration.class
)
public interface ProviderClient {

    @GetMapping(path = "/api/v1/kainat/contracts/{pin}")
    GeneralResponse<List<Contract>> getContracts(@RequestHeader("Authorization") String authorization,
                                                 @PathVariable("pin") String pin);

    @PostMapping(path = "/api/v1/kainat/make-payment")
    GeneralResponse<PaymentResponse> makePayment(@RequestHeader("Authorization") String authorization,
                                                 @RequestBody PaymentRequest request);

    @PostMapping(path = "/api/v1/payonix/check")
    ProviderCheckResponse payonixCheck(@RequestHeader("Authorization") String authorization,
                                       @RequestBody ProviderCheckRequest request);

    @PostMapping(path = "/api/v1/payonix/pay")
    ProviderPayResponse payonixPay(@RequestHeader("Authorization") String authorization,
                                   @RequestBody ProviderPayRequest request);

}
