package com.payriff.terminal.client.payriff.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class BindTransaction {

    private Long invoiceId;
    private Long transactionId;

}
