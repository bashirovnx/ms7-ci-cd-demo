package com.payriff.terminal.client.ekolizing;

import com.payriff.terminal.config.EkolizingFeignClientConfiguration;
import com.payriff.terminal.dto.request.InvoiceDetailRequestDto;
import com.payriff.terminal.dto.request.TerminalPayRequestDto;
import com.payriff.terminal.dto.response.InvoiceResponseDto;
import com.payriff.terminal.dto.response.TerminalPayResponseDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

@FeignClient(name = "adp-ekolizing", configuration = EkolizingFeignClientConfiguration.class, contextId = "adp-ekolizing")
public interface EkolizingClient {

    @PostMapping(path = "/api/v1/ekolizing/check")
    InvoiceResponseDto check(@RequestBody @Valid InvoiceDetailRequestDto requestDto);

    @PostMapping(path = "/api/v1/ekolizing/pay")
    TerminalPayResponseDto pay(@RequestBody @Valid TerminalPayRequestDto requestDto);
}
