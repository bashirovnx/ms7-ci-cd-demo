package com.payriff.terminal.client.lifttaxi.model.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Builder
@Data
@NoArgsConstructor
public class IncreaseBalanceAileRqModel {

    private String api_key;
    private String amount;
    private String driver_code;
    private String txn_id;
}
