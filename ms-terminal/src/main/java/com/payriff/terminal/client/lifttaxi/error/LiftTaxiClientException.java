package com.payriff.terminal.client.lifttaxi.error;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class LiftTaxiClientException extends RuntimeException {

    private String code;
    private String message;

}
