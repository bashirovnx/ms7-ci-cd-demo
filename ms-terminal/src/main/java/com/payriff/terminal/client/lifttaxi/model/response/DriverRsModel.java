package com.payriff.terminal.client.lifttaxi.model.response;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.payriff.terminal.client.lifttaxi.model.ErrorRsModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Builder
@Data
@NoArgsConstructor
public class DriverRsModel<T> {
    private String status;

    @JsonInclude(NON_NULL)
    private T response;

    @JsonInclude(NON_NULL)
    private ErrorRsModel error;
}
