package com.payriff.terminal.client.lifttaxi.model.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@AllArgsConstructor
@Builder
@Data
@NoArgsConstructor
public class IncreaseBalanceLiftRqModel {
    private String tabel;
    private BigDecimal amount;
    private String paymentId;
}
