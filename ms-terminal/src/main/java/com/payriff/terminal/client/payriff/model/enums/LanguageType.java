package com.payriff.terminal.client.payriff.model.enums;

public enum LanguageType {
    AZ,
    EN,
    RU
}