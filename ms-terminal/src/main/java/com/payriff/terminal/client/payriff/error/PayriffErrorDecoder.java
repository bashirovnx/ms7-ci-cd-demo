package com.payriff.terminal.client.payriff.error;

import feign.Response;
import feign.codec.Decoder;
import feign.codec.ErrorDecoder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
@RequiredArgsConstructor
public class PayriffErrorDecoder implements ErrorDecoder {

    private final Decoder decoder;

    private static void httpStatusDecoder(int status) {

        switch (status) {
            case 401:
            case 403:
                throw new PayriffClientException("AUTHORIZATION_ERROR", "Authorization error");
            case 404:
                throw new PayriffClientException("NOT_FOUND", "Not found!");
        }

    }

    public Exception decode(String methodKey, Response response) {

        httpStatusDecoder(response.status());

        BaseResponse error;
        try {
            error = (BaseResponse) decoder.decode(response, BaseResponse.class);
        } catch (Exception e) {
            return new PayriffClientException(
                    "INTERNAL_ERROR", e.getMessage()
            );
        }

        return new PayriffClientException(error.getResponseCode(), error.getMessage());

    }

    @Data
    public static class BaseResponse {

        private String responseCode;
        private String message;

    }

}
