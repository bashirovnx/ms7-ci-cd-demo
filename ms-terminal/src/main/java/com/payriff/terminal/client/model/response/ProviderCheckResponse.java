package com.payriff.terminal.client.model.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProviderCheckResponse {

    private String fullName;
    private BigDecimal amount;
    private String description;
    private String email;
    private String phoneNumber;

}
