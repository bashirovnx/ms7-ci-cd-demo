package com.payriff.terminal.client.lifttaxi.error;

import com.payriff.terminal.client.lifttaxi.model.ErrorRsModel;
import feign.Response;
import feign.codec.Decoder;
import feign.codec.ErrorDecoder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
@RequiredArgsConstructor
public class LiftTaxiErrorDecoder implements ErrorDecoder {

    private final Decoder decoder;

    private static void httpStatusDecoder(int status) {
        switch (status) {
            case 404:
                throw new LiftTaxiClientException("NOT_FOUND_ERROR", "Driver is not found error with given tabel number");
            case 401:
                throw new LiftTaxiClientException("AUTHORIZATION_ERROR", "Authorization error");
            default:
                throw new LiftTaxiClientException("INTERNAL_ERROR", "Internal error");
        }

    }

    public Exception decode(String methodKey, Response response) {

        httpStatusDecoder(response.status());

        ErrorRsModel error;
        try {
            error = (ErrorRsModel) decoder.decode(response, ErrorRsModel.class);
        } catch (Exception e) {
            return new LiftTaxiClientException(
                    "INTERNAL_ERROR", e.getMessage()
            );
        }

        return new LiftTaxiClientException(error.getCode(), error.getMessage());

    }

    @Data
    public static class BaseResponse {

        private String responseCode;
        private String message;

    }

}
