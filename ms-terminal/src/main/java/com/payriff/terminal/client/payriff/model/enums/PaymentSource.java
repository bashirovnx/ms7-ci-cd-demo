package com.payriff.terminal.client.payriff.model.enums;

public enum PaymentSource {

    NORMAL,
    INVOICE,
    TRANSFER,
    INSTALLMENT,
    QR

}
