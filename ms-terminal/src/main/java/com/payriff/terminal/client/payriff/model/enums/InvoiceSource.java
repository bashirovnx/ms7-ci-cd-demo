package com.payriff.terminal.client.payriff.model.enums;

public enum InvoiceSource {

    B2B,
    DASHBOARD,
    PMD,
    TERMINAL

}
