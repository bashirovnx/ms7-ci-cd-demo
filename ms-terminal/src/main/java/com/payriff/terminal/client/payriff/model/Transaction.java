package com.payriff.terminal.client.payriff.model;

import com.payriff.terminal.client.payriff.model.enums.LanguageType;
import com.payriff.terminal.client.payriff.model.enums.PaymentSource;
import com.payriff.terminal.client.payriff.model.enums.PaymentStatus;
import com.payriff.terminal.dto.CurrencyType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Transaction {

    private Long id;
    private Date createdDate;
    private String cardBrand;
    private PaymentStatus paymentStatus;
    private BigDecimal amount;
    private BigDecimal paidAmount;
    private BigDecimal commissionRate;
    private BigDecimal payriffAmount;
    private Long applicationId;
    private CurrencyType currencyType;
    private String description;
    private PaymentSource paymentSource;
    private String uuid;
    private String rrn;
    private Long orderId;
    private String sessionId;
    private String responseDescription;
    private LanguageType orderLanguage;

}