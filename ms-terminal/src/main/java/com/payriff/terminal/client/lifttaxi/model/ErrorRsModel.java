package com.payriff.terminal.client.lifttaxi.model;

import com.payriff.terminal.util.ResultStatus;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ErrorRsModel {
    private ResultStatus status;
    private String code;
    private String message;
}
