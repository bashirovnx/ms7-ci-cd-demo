package com.payriff.terminal.client.kainat.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Contract {

    private Long id;
    private String no;
    private String contractDate;
    private String nextPaymentDate;
    private BigDecimal maxAmount;
    private BigDecimal currentDebt;
    private Long personId;
    private String firstName;
    private String middleName;
    private String lastName;
    private String operationUid;
    private Branch branch;
    private Department department;
    private SubDepartment subDepartment;

}
