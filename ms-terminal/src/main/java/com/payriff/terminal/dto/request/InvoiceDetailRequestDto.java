package com.payriff.terminal.dto.request;

import com.payriff.terminal.dto.TerminalInvoiceReqType;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class InvoiceDetailRequestDto {

    private String rrn;
    private String src;
    private TerminalInvoiceReqType type;
    private String value;

}
