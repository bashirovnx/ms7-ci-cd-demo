package com.payriff.terminal.dto;

public enum TerminalTransStatus {

    PENDING,
    ACCEPTED,
    ERROR,
    RETRY,
    RETRY_IN_PROGRESS,
    EXPIRED,
    PARTIAL,
    COMPLETE

}
