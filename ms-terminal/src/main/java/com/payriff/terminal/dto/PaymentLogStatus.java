package com.payriff.terminal.dto;

public enum PaymentLogStatus {

    ACCEPTED,
    ERROR,
    COMPLETE,
    UNKNOWN;

    public static PaymentLogStatus getStatus(String status) {
        switch (status) {
            case "ACCAPTED":
                return PaymentLogStatus.ACCEPTED;
            case "ERROR":
                return PaymentLogStatus.ERROR;
            case "COMPLETE":
                return PaymentLogStatus.COMPLETE;
            default:
                return PaymentLogStatus.UNKNOWN;
        }
    }

}
