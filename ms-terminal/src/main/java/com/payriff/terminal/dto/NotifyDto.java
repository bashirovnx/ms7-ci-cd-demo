package com.payriff.terminal.dto;

import com.payriff.terminal.dto.request.TerminalPayRequestDto;
import lombok.*;

@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class NotifyDto extends TerminalPayRequestDto {

    private String name;
    private String description;
    private String phoneNumber;
    private String transactionId;

}
