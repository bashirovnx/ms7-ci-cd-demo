package com.payriff.terminal.dto;

public enum TerminalInvoiceReqType {

    INVOICE_CODE,
    PIN_CODE,
    CUSTOMER_CODE,
    PHONE_NUMBER

}
