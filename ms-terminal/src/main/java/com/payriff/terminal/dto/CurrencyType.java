package com.payriff.terminal.dto;

public enum CurrencyType {

    AZN(944),
    USD(840),
    EUR(978);

    private int code;

    CurrencyType(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public String getCodeAsString() {
        return String.valueOf(code);
    }
}
