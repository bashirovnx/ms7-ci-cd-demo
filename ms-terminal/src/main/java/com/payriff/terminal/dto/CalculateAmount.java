package com.payriff.terminal.dto;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class CalculateAmount {

    private BigDecimal percentage;
    private BigDecimal commission;
    private BigDecimal paidAmount;

}
