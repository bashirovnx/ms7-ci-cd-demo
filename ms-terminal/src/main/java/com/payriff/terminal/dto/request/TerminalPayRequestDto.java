package com.payriff.terminal.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TerminalPayRequestDto {

    private String value;
    private BigDecimal amount;
    private String backRrn;
    private boolean partial;
    private String src;

}
