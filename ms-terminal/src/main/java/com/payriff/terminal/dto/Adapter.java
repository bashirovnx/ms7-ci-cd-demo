package com.payriff.terminal.dto;

public enum Adapter {

    EKOLIZING("adp-ekolizing"),
    LIFT_TAXI("adp-lift-taxi"),
    AILE_TAXI("aile-taxi"),
    KAINAT("adp-kainat"),
    PAYONIX("adp-payonix");

    private String name;

    Adapter(String name) {
        this.name = name;
    }

    public static Adapter fromString(String stringValue) {
        for (Adapter a : Adapter.values()) {
            if (a.name.equals(stringValue))
                return a;
        }
        throw new IllegalArgumentException("No Adapter found with " + stringValue + " value");
    }
}
