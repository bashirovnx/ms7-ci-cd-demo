package com.payriff.terminal.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.payriff.terminal.dto.TerminalResult;
import com.payriff.terminal.dto.TerminalTransStatus;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@EqualsAndHashCode(callSuper = false)
public class TerminalPayResponseDto extends TerminalResult {

    private String invoiceCode;
    private BigDecimal amount;
    private TerminalTransStatus transStatus;
    private Date transactionDate;
    private String note;
    private boolean partial;

}
