package com.payriff.terminal.schedule;

import com.payriff.terminal.dto.TerminalTransStatus;
import com.payriff.terminal.repository.TerminalTransInfoRepository;
import com.payriff.terminal.service.TerminalService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.time.LocalDateTime;

import static com.payriff.terminal.dto.TerminalTransStatus.COMPLETE;

@Configuration
@EnableScheduling
@RequiredArgsConstructor
@Slf4j
public class RetryScheduler {

    private final TerminalService terminalService;
    private final TerminalTransInfoRepository terminalTransInfoRepository;

    @Bean
    public RetryNotify retryNotify() {
        return new RetryNotify();
    }

    public class RetryNotify {

        @SneakyThrows
        @Scheduled(fixedDelayString = "PT5M") //every 5 minute
        public void retry() {

            log.info("Retry notify job started. Schedule time: " + LocalDateTime.now());

            terminalTransInfoRepository.byStatusAndRetryCountLessThan(TerminalTransStatus.RETRY, 10)
                    .forEach(transInfo -> {

                        log.info("TerminalTransInfoEntity: {} is retrying to notify", transInfo);
                        transInfo.setStatus(TerminalTransStatus.RETRY_IN_PROGRESS);
                        terminalTransInfoRepository.save(transInfo);

                        boolean isSuccess = terminalService.retryNotify(transInfo);

                        log.info("Retry notify status: " + isSuccess);

                        if (isSuccess) {
                            transInfo.setStatus(COMPLETE);

                            terminalTransInfoRepository.save(transInfo);

                            log.info("Trans info status to COMPLETE");
                        }

                        log.info("TerminalTransInfoEntity: {} retried to notify.", transInfo);
                    });

        }
    }

}
