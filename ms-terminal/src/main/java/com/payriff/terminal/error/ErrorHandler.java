package com.payriff.terminal.error;

import com.payriff.terminal.client.lifttaxi.error.LiftTaxiClientException;
import com.payriff.terminal.repository.ErrorLogRepository;
import com.payriff.terminal.util.AsyncLogging;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static com.payriff.terminal.util.CommonUtils.getFilteredStackTrace;

@RestControllerAdvice
@Slf4j
@RequiredArgsConstructor
public class ErrorHandler extends ResponseEntityExceptionHandler {

    private final HttpServletRequest request;
    private final ErrorLogRepository logRepository;

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({TerminalException.class})
    public ResponseEntity<TerminalResult> handleServiceException(TerminalException ex) {

        log.error("Error handler print: ", getFilteredStackTrace(ex));

        asyncLogging(ex);

        return ResponseEntity.badRequest()
                .body(TerminalResult.builder()
                        .status(TerminalResult.TerminalStatus.ERROR)
                        .code(ex.getCode().getCode())
                        .message(ex.getMessage())
                        .build());
    }

    @ExceptionHandler({LiftTaxiClientException.class})
    public ResponseEntity<TerminalResult> handleLiftTaxiException(LiftTaxiClientException ex) {

        log.error("Error handler print: ", getFilteredStackTrace(ex));

        return ResponseEntity.badRequest()
                .body(TerminalResult.builder()
                        .status(TerminalResult.TerminalStatus.ERROR)
                        .code(ex.getCode())
                        .message(ex.getMessage())
                        .build());
    }


    private void asyncLogging(TerminalException ex) {

        String thread = Thread.currentThread().getName();
        String client = getClient();

        log.info("Async logging, thread name: " + thread);

        final ExecutorService executorService = Executors.newFixedThreadPool(2);
        final Future<Boolean> notifyPayerTask = executorService.submit(
                new AsyncLogging(thread, client, ex, logRepository)
        );

        Boolean result;
        try {
            result = notifyPayerTask.get();
        } catch (InterruptedException e) {
            result = Boolean.FALSE;
            log.error("Error on InterruptedException", getFilteredStackTrace(e));
        } catch (ExecutionException e) {
            result = Boolean.FALSE;
            log.error("Error on ExecutionException", getFilteredStackTrace(e));
        }
        log.info("Result async logging : " + result);
    }

    private String getClient() {
        return request.getHeader("X-Client");
    }

}