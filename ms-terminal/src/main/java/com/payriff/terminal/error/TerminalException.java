package com.payriff.terminal.error;


import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class TerminalException extends RuntimeException {

    private String rrn;
    private TerminalCode code;

    public TerminalException(String rrn, TerminalCode code, String message) {
        super(message);
        this.rrn = rrn;
        this.code = code;
    }

    public enum TerminalCode {

        INVOICE_NOT_FOUND("01001"),
        INVOICE_ALREDAY_COMPLETED("01002"),
        INVOICE_EXPIRED("01003"),

        INVOICE_TYPE_INCORRECT("02001"),
        INVOICE_IS_NOT_PENDING("02002"),
        TRANSACTION_NOT_FOUND("02003"),
        PENDING_TRANSACTION_NOT_FOUND("02004"),
        CARD_NOT_FOUND_FOR_TRANSFER("02005"),
        TRANSACTION_NOT_CREATED("02006"),
        RRN_MUST_BE_UNIQUE("02007"),

        ADAPTER_NAME_NOT_FOUND("02008");

        String code;

        TerminalCode(String code) {
            this.code = code;
        }

        public String getCode() {
            return code;
        }
    }

}
