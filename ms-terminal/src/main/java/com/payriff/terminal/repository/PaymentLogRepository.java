package com.payriff.terminal.repository;

import com.payriff.terminal.domain.PaymentLogEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PaymentLogRepository extends JpaRepository<PaymentLogEntity, Long> {

    @Query(
            value = "update payment_log set status = :status where back_rrn = :rrn",
            nativeQuery = true
    )
    void update(@Param("status") String status, @Param("rrn") String rrn);

}
