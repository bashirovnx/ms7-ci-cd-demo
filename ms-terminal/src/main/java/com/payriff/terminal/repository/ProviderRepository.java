package com.payriff.terminal.repository;

import com.payriff.terminal.domain.ProviderEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProviderRepository extends JpaRepository<ProviderEntity, Long> {

    Optional<ProviderEntity> findByProviderSource(String source);

    Optional<ProviderEntity> findByAppId(Long applicationId);

}
