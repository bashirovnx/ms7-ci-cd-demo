package com.payriff.terminal.repository;

import com.payriff.terminal.domain.SubInvoiceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SubInvoiceRepository extends JpaRepository<SubInvoiceEntity, Long> {
}
