package com.payriff.terminal.repository;

import com.payriff.terminal.domain.TerminalTransInfoEntity;
import com.payriff.terminal.dto.TerminalTransStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TerminalTransInfoRepository extends JpaRepository<TerminalTransInfoEntity, Long> {

    default List<TerminalTransInfoEntity> byStatusAndRetryCountLessThan(TerminalTransStatus status, Integer count) {
        return findByStatusAndRetryCountLessThan(status, count);
    }

    Optional<TerminalTransInfoEntity> findByBackRrnAndStatus(String rrn, TerminalTransStatus status);

    List<TerminalTransInfoEntity> findByBackRrn(String rrn);

    Optional<TerminalTransInfoEntity> findByInvoiceCodeAndStatus(String invoiceCode, TerminalTransStatus status);

    List<TerminalTransInfoEntity> findByStatusAndRetryCountLessThan(
            TerminalTransStatus status,
            Integer retryCount);

}
