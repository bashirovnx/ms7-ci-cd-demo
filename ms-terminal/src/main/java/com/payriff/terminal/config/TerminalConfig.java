package com.payriff.terminal.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties("service.terminal.security")
public class TerminalConfig {

    private String username;
    private String password;

}