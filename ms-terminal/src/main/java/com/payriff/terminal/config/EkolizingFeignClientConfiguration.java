package com.payriff.terminal.config;

import com.payriff.terminal.client.ekolizing.error.EkolizingErrorDecoder;
import feign.Logger;
import feign.codec.Decoder;
import feign.codec.ErrorDecoder;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration(value = "EkolizingConfiguration")
@RequiredArgsConstructor
public class EkolizingFeignClientConfiguration {

    @Bean
    Logger.Level ekolizingLoggerLevel() {
        return Logger.Level.FULL;
    }

    @Bean
    public ErrorDecoder ekolizingErrorDecoder() {
        return new EkolizingErrorDecoder(new Decoder.Default());
    }
}