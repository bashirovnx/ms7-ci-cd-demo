package com.payriff.terminal.config;

import feign.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ProviderFeignClientConfiguration {

    private static final String AUTHORIZATION_HEADER = "Authorization";
    @Value("${service.kainat.security.authorization}")
    private String authorization;

    @Bean(name = "kainatFeignLoggerLevel")
    Logger.Level feignLoggerLevel() {
        return Logger.Level.FULL;
    }

    /*@Bean
    public RequestInterceptor kainatRequestInterceptor() {
        return (RequestTemplate template) -> template.header(AUTHORIZATION_HEADER, authorization);
    }*/

}