package com.payriff.terminal.config.interceptor;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;

@Slf4j
public class KainatRequestInterceptor implements RequestInterceptor {

    private static final String AUTHORIZATION_HEADER = "Authorization";
    @Value("${service.kainat.security.authorization}")
    private String authorization;

    @Override
    public void apply(RequestTemplate template) {
        template.header(AUTHORIZATION_HEADER, authorization);
        log.info("KainatRequestInterceptor applied");
    }
}