package com.payriff.terminal.config;

import com.payriff.terminal.client.lifttaxi.error.LiftTaxiErrorDecoder;
import feign.Logger;
import feign.codec.Decoder;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@AllArgsConstructor
public class LiftTaxiFeignClientConfiguration {

    @Bean(name = "liftTaxiFeignLoggerLevel")
    Logger.Level feignLoggerLevel() {
        return Logger.Level.FULL;
    }

    @Bean
    public LiftTaxiErrorDecoder clientErrorDecoder() {
        return new LiftTaxiErrorDecoder(new Decoder.Default());
    }


}