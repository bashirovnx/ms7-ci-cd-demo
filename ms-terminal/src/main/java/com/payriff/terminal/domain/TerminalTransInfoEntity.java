package com.payriff.terminal.domain;

import com.payriff.terminal.dto.TerminalInvoiceReqType;
import com.payriff.terminal.dto.TerminalTransStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import org.hibernate.annotations.ColumnDefault;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "terminal_trans_info")
public class TerminalTransInfoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    private TerminalInvoiceReqType type;

    @Column(name = "paid_amount")
    private BigDecimal paidAmount;

    @Column(name = "invoice_code")
    private String invoiceCode;

    @Enumerated(EnumType.STRING)
    private TerminalTransStatus status;

    @Column(name = "back_rrn")
    private String backRrn;

    @Column(name = "source")
    private String source;

    @Column(name = "client")
    private String client;

    @Column(name = "create_date")
    private LocalDateTime createDate;

    private Long invoiceEntity;

    private Long transactionEntity;

    @Column(name = "kainat_contract_id")
    private String kainatContractId;

    private String value;

    @Column(name = "kainat_uid")
    private String kainatUid;

    @Column(name = "payment_id")
    private Long paymentId;

    @Column(name = "retry_count")
    @ColumnDefault("0")
    private Integer retryCount;
}
