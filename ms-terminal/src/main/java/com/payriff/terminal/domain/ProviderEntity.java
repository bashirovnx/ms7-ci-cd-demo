package com.payriff.terminal.domain;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;

import static com.payriff.terminal.domain.ProviderEntity.TABLE_NAME;

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Table(name = TABLE_NAME)
public class ProviderEntity {

    public static final String TABLE_NAME = "providers";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "app_id")
    private Long appId;

    @Column(name = "name")
    private String name;

    @Column(name = "adapter_name")
    private String adapterName;

    @Column(name = "provider_source")
    private String providerSource;

    @Column(name = "user_source")
    private String userSource;

    @Column(name = "commission")
    private BigDecimal commission;

    @Column(name = "deposit")
    private boolean deposit;

    @Column(name = "active")
    private boolean isActive;

    @Column(name = "inputs")
    private String inputs;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        ProviderEntity that = (ProviderEntity) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
