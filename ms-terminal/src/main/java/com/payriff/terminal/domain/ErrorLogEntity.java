package com.payriff.terminal.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "error_log")
public class ErrorLogEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "back_rrn")
    private String backRrn;

    @Column(name = "code")
    private String code;

    @Column(name = "message")
    private String message;

    @Column(name = "client")
    private String client;

    @Column(name = "create_date")
    private LocalDateTime createDate;

}
