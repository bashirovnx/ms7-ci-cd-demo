package com.payriff.terminal.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@Table(name = "sub_invoice")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SubInvoiceEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sub_invoice_generator")
    @SequenceGenerator(name = "sub_invoice_generator", sequenceName = "sub_invoice_seq", allocationSize = 1)
    private Long id;

    private Long parentId;

    private Long subId;

    @Column(name = "create_date")
    private LocalDateTime createDate;

}