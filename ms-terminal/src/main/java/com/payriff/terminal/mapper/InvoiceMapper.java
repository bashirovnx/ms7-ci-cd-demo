package com.payriff.terminal.mapper;

import com.payriff.terminal.client.model.response.ProviderCheckResponse;
import com.payriff.terminal.dto.response.InvoiceResponseDto;
import com.payriff.terminal.client.payriff.model.CustomerInvoice;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface InvoiceMapper {

    InvoiceMapper INSTANCE = Mappers.getMapper(InvoiceMapper.class);

    @Mappings({
            @Mapping(source = "rrn", target = "backRrn"),
            @Mapping(source = "senderCompany", target = "senderCompany"),
            @Mapping(source = "checkinDate", target = "checkinDate"),
            @Mapping(source = "maxAmount", target = "maxAmount")})
        //@Mapping(source = "invoiceEntity.expireDate", target = "expiryDate")
    InvoiceResponseDto toInvoiceDto(CustomerInvoice invoiceEntity,
                                    String rrn,
                                    String senderCompany,
                                    LocalDateTime checkinDate,
                                    BigDecimal maxAmount);

    @Mapping(ignore = true, target = "id")
    CustomerInvoice cloneInvoiceEntity(CustomerInvoice invoiceEntity);

    InvoiceResponseDto toInvoiceDto(ProviderCheckResponse providerCheckResponse);


}