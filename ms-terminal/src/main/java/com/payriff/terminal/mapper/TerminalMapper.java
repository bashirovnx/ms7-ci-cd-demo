package com.payriff.terminal.mapper;

import com.payriff.terminal.domain.TerminalTransInfoEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface TerminalMapper {

    TerminalMapper INSTANCE = Mappers.getMapper(TerminalMapper.class);

    @Mapping(ignore = true, target = "id")
    TerminalTransInfoEntity cloneTerminalTransInfoEntity(TerminalTransInfoEntity terminalTransInfoEntity);

}
