package com.payriff.terminal.util;

import com.payriff.terminal.domain.ErrorLogEntity;
import com.payriff.terminal.error.TerminalException;
import com.payriff.terminal.repository.ErrorLogRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;
import java.util.concurrent.Callable;

import static com.payriff.terminal.util.CommonUtils.getFilteredStackTrace;

@Slf4j
@RequiredArgsConstructor
public class AsyncLogging implements Callable<Boolean> {

    private final String thread;
    private final String client;
    private final TerminalException terminalException;
    private final ErrorLogRepository errorLogRepository;

    @Override
    public Boolean call() throws Exception {
        return logging(terminalException);
    }

    private boolean logging(TerminalException terminalException) {

        Thread.currentThread().setName(thread.concat("_error"));

        log.info("Save ansync error log");
        try {
            ErrorLogEntity entity = ErrorLogEntity.builder()
                    .backRrn(terminalException.getRrn())
                    .code(terminalException.getCode().getCode())
                    .message(terminalException.getMessage())
                    .client(client)
                    .createDate(LocalDateTime.now())
                    .build();

            entity = errorLogRepository.save(entity);

            log.info("Saved error log id: " + entity.getId());
            return Boolean.TRUE;
        } catch (Exception ex) {
            log.error("Error on error logging", getFilteredStackTrace(ex));
            return Boolean.FALSE;
        }
    }

}
