package com.payriff.terminal.util;

import java.util.Arrays;
import java.util.UUID;

public class CommonUtils {

    public static String getThreadName(String method) {
        return UUID.randomUUID().toString()
                .replace("-", "")
                .concat("_")
                .concat(method);
    }

    public static Exception getFilteredStackTrace(Exception ex) {
        ex.setStackTrace(
                Arrays.stream(ex.getStackTrace())
                        .filter(se -> se.getClassName().contains("com.payriff"))
                        .toArray(StackTraceElement[]::new));

        return ex;
    }

}
