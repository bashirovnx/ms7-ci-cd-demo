package com.payriff.gateway.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties("service")
public class GatewayConfig {

    private String[] filterable;
    private String[] allowedSpecialCharacters;

}
