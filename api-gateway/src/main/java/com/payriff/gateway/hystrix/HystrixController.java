package com.payriff.gateway.hystrix;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/fallback")
public class HystrixController {

    @GetMapping("/emanat")
    public String emanatFallback() {
        return "Emanat Service is not available.";
    }

    @GetMapping("/kassam")
    public String kassamFallback() {
        return "Kassam Service is not available.";
    }

    @GetMapping("/dictionary")
    public String dictionaryFallback() {
        return "Dictionary Service is not available.";
    }

    @GetMapping("/kainat")
    public String kainatFallback() {
        return "Kainat Service is not available.";
    }

}